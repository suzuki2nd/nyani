
Shader "BackGround"
{
	Properties
	{
		_Color("Tint", Color) = (1,1,1,1)
		_Alpha("Alpha", Range(0,1)) = 1.0
		_MainTex("Texture", 2D) = "white" {}

		_ScrollX("ScrollX", float) = 0
		_ScrollY("ScrollY", float) = 0
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent + 1"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

				Pass
				{

					Blend SrcAlpha OneMinusSrcAlpha

					CGPROGRAM   // プログラムを書き始めるという宣言

					// 関数宣言
					#pragma vertex vert    // "vert" 関数を頂点シェーダー使用する宣言
					#pragma fragment frag  // "frag" 関数をフラグメントシェーダーと使用する宣言

				struct appdata
				{
			//頂点座標
			//POSITION セマンティクスをつけているので頂点座標の情報が入る
			float4 vertex : POSITION;

			// UV座標
			//TEXCOORD セマンティクスをつけているのでuv座標の情報が入る
			float2 uv0 : TEXCOORD0;

		};

		//出力情報
		struct v2f
		{
			float4  pos : SV_POSITION;
			// 変換前の頂点座標
			float2 uv0 : TEXCOORD0;
		};


		sampler2D _MainTex;
		fixed _ScrollX, _ScrollY;

		//頂点シェーダー
		v2f vert(appdata a)
		{
			v2f v;

			v.pos = UnityObjectToClipPos(a.vertex);
			v.uv0 = a.uv0 + fixed2(_ScrollX , _ScrollY);

			return v;
		}

		// フラグメントシェーダー
		//ピクセルを何色に塗るか決める
		float4 frag(v2f v) : SV_Target
		{

			float4 col = tex2D(_MainTex, v.uv0);

			return col;
		}

			ENDCG   // プログラムを書き終わるという宣言
		}
		}
}