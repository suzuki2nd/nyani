
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile DUMMY PIXELSNAP_ON
#include "UnityCG.cginc"

struct appdata
{
	float4 vertex   : POSITION;
	float4 color    : COLOR;
	float2 texcoord : TEXCOORD0;
};

struct v2f
{
	float2 texcoord  : TEXCOORD0;
	float4 vertex   : SV_POSITION;
	float4 color : COLOR;
};

sampler2D _MainTex;
float _OutLineSpread;
fixed4 _Color;
fixed4 _ColorX = float4(1, 0, 1, 1);
fixed _ScrollX, _ScrollY;


v2f vert(appdata IN)
{
	v2f OUT;
	OUT.vertex = UnityObjectToClipPos(IN.vertex);
	OUT.texcoord = IN.texcoord;
	OUT.color = IN.color;
	return OUT;
}

float4 frag(v2f i) : SV_Target
{

	fixed4 mainColor = (tex2D(_MainTex, i.texcoord + float2(-_OutLineSpread,_OutLineSpread))
	+ tex2D(_MainTex, i.texcoord + float2(_OutLineSpread,-_OutLineSpread))
	+ tex2D(_MainTex, i.texcoord + float2(_OutLineSpread,_OutLineSpread))
	+ tex2D(_MainTex, i.texcoord - float2(_OutLineSpread,_OutLineSpread)));

	mainColor.rgb = _ColorX.rgb;

	fixed4 addcolor = tex2D(_MainTex, i.texcoord) * i.color;

	if (mainColor.a > 0.40) { mainColor = _ColorX; }
	if (addcolor.a > 0.40) { mainColor = addcolor; mainColor.a = addcolor.a; }

	return mainColor * i.color.a;
}