﻿
// 頂点シェーダへの入力構造体
struct vertInput
{
    float4 vertex : POSITION;
    float4 normal : NORMAL;
    float2 texcoord : TEXCOORD0; //uv座標1
    float2 texcoord2 : TEXCOORD1; //uv座標2
};

// フラグメントシェーダへの入力構造体
struct vert2frag
{
    float4 position : POSITION;
    float2 uv : TEXCOORD0;      //uv座標1
    float2 uv2 : TEXCOORD1;    //uv座標2
};

// テクスチャ
uniform sampler2D _MainTex;
uniform sampler2D _SubTex;

vert2frag vert(vertInput v)
{
    //毛を法線方向に伸ばす間隔
    const float spacing = 0.2;
	
    vert2frag o;
	
    float3 forceDirection = float3(0.0, 0.0, 0.0);
    float4 position = v.vertex;
	
    //風に吹かれている感をだす
    //風の方向を作ればその方向になびかせられる
    forceDirection.x = sin(_Time.y + position.x * 0.05) * 0.2;
    forceDirection.y = cos(_Time.y * 0.7 + position.y * 0.04) * 0.2;
    forceDirection.z = sin(_Time.y * 0.7 + position.y * 0.04) * 0.2;
	
    //重力をかける
    float3 displacement = forceDirection + float3(0.0, -0.75, 0.0).xyz;
	
    float displacementFactor = pow(FUR_OFFSET, 3.0);
    float4 aNormal = v.normal;
    aNormal.xyz += displacement * displacementFactor;
	
    //法線方向にモデルを伸ばす
    float4 n = normalize(aNormal) * FUR_OFFSET * spacing;
    
    //本来の頂点位置に足しこむ
    //w の値を1.0固定にしておく
    float4 wpos = float4(v.vertex.xyz + n.xyz, 1.0);
    //頂点座標設定
    o.position = UnityObjectToClipPos(wpos);
    
    //メインテクスチャそのまま
    o.uv = v.texcoord;
    //サブテクスチャを拡大
    //拡大すると、毛並みの間隔が広くなる
    o.uv2 = v.texcoord2 * 1.0f;

    return o;
}

//フラグメントシェーダ
float4 frag(vert2frag i) : COLOR
{
    //白黒画像からそのピクセルの色を取得
    float4 map = tex2D(_SubTex, i.uv2);
    
    //アルファ0 以下 または、
    //青の成分が オフセット以下の場合
    //レンダリングしない（破棄する）
    if (map.a <= 0.0 || map.b < FUR_OFFSET)
    {
        discard;
    }

    //レンダリングする場合は
    ///そこのピクセルを描画
    float4 color = tex2D(_MainTex, i.uv);
    
    //徐々に薄くすることで、毛並みっぽくなる
    color.a = 1.1 - FUR_OFFSET;
    return color;
}