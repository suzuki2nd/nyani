
Shader "Custom/SpriteOutline"
{
	Properties
	{
		_OutLineSpread("Outline Spread", Range(0,0.01)) = 0.007
		_Color("Tint", Color) = (1,1,1,1)
		_ColorX("Outline", Color) = (1,1,1,1)
		_Alpha("Alpha", Range(0,1)) = 1.0
		_MainTex("Texture", 2D) = "white" {}
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Pass
			{

				ZWrite Off
				Blend SrcAlpha OneMinusSrcAlpha
				Cull Off

				CGPROGRAM

				#include "OutlineSprite.cginc"

				ENDCG
			}

				Pass
				{

					Blend SrcAlpha OneMinusSrcAlpha

					CGPROGRAM   // プログラムを書き始めるという宣言

					// 関数宣言
					#pragma vertex vert    // "vert" 関数を頂点シェーダー使用する宣言
					#pragma fragment frag  // "frag" 関数をフラグメントシェーダーと使用する宣言

				struct appdata
				{
			//頂点座標
			//POSITION セマンティクスをつけているので頂点座標の情報が入る
			float4 vertex : POSITION;

			// UV座標
			//TEXCOORD セマンティクスをつけているのでuv座標の情報が入る
			float2 uv : TEXCOORD0;
		};

		//出力情報
		struct v2f
		{
			float4  pos : SV_POSITION;

			// 変換前の頂点座標
			float2 uv : TEXCOORD0;
		};


		sampler2D _MainTex;
		float4 _addColor;

		//頂点シェーダー
		v2f vert(appdata a)
		{
			v2f v;

			v.pos = UnityObjectToClipPos(a.vertex);

			v.uv = a.uv;

			return v;
		}

		// フラグメントシェーダー
		//ピクセルを何色に塗るか決める
		float4 frag(v2f v) : SV_Target
		{
			float4 col = tex2D(_MainTex, v.uv);

			if (col.a > 0)
			{
				col += _addColor;
			}

			return  col;
		}

			ENDCG   // プログラムを書き終わるという宣言
		}
		}
}