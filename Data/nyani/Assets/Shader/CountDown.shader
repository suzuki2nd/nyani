
Shader "CountDown"
{
	Properties
	{
		_OutLineSpread("Outline Spread", Range(0,0.02)) = 0.007
		_ColorX("Outline", Color) = (1,1,1,1)
		_Color("Tint", Color) = (1,1,1,1)

		_OutLineSpread2("Outline Spread2", Range(0,0.02)) = 0.007
		_ColorX2("Outline2", Color) = (1,1,1,1)

		_Alpha("Alpha", Range(0,1)) = 1.0
		_MainTex("Texture", 2D) = "white" {}
		_SubTex("Texture", 2D) = "white" {}

		_ScrollX("ScrollX", float) = 0
		_ScrollY("ScrollY", float) = 0
	}

		SubShader
		{
			Tags	
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Pass
			{

				ZWrite Off 
				Blend SrcAlpha OneMinusSrcAlpha
				Cull Off

				CGPROGRAM
			
				#include "OutlineSprite.cginc"

				ENDCG
			}


			Pass
			{

				ZWrite Off
				Blend SrcAlpha OneMinusSrcAlpha
				Cull Off

				CGPROGRAM


#pragma vertex vert
#pragma fragment frag
#pragma multi_compile DUMMY PIXELSNAP_ON
#include "UnityCG.cginc"

struct appdata
{
	float4 vertex   : POSITION;
	float4 color    : COLOR;
	float2 texcoord : TEXCOORD0;
};

struct v2f
{
	float2 texcoord  : TEXCOORD0;
	float4 vertex   : SV_POSITION;
	float4 color : COLOR;
};

sampler2D _MainTex;
float _OutLineSpread2;
fixed4 _Color;
fixed4 _ColorX2 = float4(1, 0, 1, 1);

v2f vert(appdata IN)
{
	v2f OUT;
	OUT.vertex = UnityObjectToClipPos(IN.vertex);
	OUT.texcoord = IN.texcoord;
	OUT.color = IN.color;
	return OUT;
}

float4 frag(v2f i) : SV_Target
{

	fixed4 mainColor = (tex2D(_MainTex, i.texcoord + float2(-_OutLineSpread2,_OutLineSpread2))
	+ tex2D(_MainTex, i.texcoord + float2(_OutLineSpread2,-_OutLineSpread2))
	+ tex2D(_MainTex, i.texcoord + float2(_OutLineSpread2,_OutLineSpread2))
	+ tex2D(_MainTex, i.texcoord - float2(_OutLineSpread2,_OutLineSpread2)));

	mainColor.rgb = _ColorX2.rgb;

	fixed4 addcolor = tex2D(_MainTex, i.texcoord) * i.color;

	if (mainColor.a > 0.40) { mainColor = _ColorX2; }
	if (addcolor.a > 0.40) { mainColor = addcolor; mainColor.a = addcolor.a; }

	return mainColor * i.color.a;
}

				ENDCG
			}


				Pass
				{
	
					Blend SrcAlpha OneMinusSrcAlpha

					CGPROGRAM   // プログラムを書き始めるという宣言
					
					// 関数宣言
					#pragma vertex vert    // "vert" 関数を頂点シェーダー使用する宣言
					#pragma fragment frag  // "frag" 関数をフラグメントシェーダーと使用する宣言

				struct appdata
				{
					//頂点座標
					//POSITION セマンティクスをつけているので頂点座標の情報が入る
					float4 vertex : POSITION;

					// UV座標
					//TEXCOORD セマンティクスをつけているのでuv座標の情報が入る
					float2 uv0 : TEXCOORD0;

					float2 uv1 : TEXCOORD1;
				};

				//出力情報
				struct v2f
				{
					float4  pos : SV_POSITION;
					float al : TEXCOORD2;

					// 変換前の頂点座標
					float2 uv0 : TEXCOORD0;

					float2 uv1 : TEXCOORD1;
				};


				sampler2D _MainTex;
				sampler2D _SubTex;
				fixed _ScrollX, _ScrollY;

				//頂点シェーダー
				v2f vert(appdata a)
				{
					v2f v;

					v.pos = UnityObjectToClipPos(a.vertex);
					
					v.uv0 = a.uv0;
					v.uv1 = a.uv1 + fixed2(_ScrollX , _ScrollY);

					return v;
				}

				// フラグメントシェーダー
				//ピクセルを何色に塗るか決める
				float4 frag(v2f v) : SV_Target
				{
					if (tex2D(_MainTex, v.uv0).a <= 0)
					{
						discard;
					}

					float4 col = tex2D(_SubTex, v.uv1);
					//col.a = 0.8f;

					float val = 0.4f;
					//return col + float4(val, val, val, 0.f);
					return col;
				}

					ENDCG   // プログラムを書き終わるという宣言
				}
		}
}