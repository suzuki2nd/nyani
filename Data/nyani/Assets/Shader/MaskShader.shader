//https://www.sejuku.net/blog/65248
//https://blog.applibot.co.jp/2017/09/01/tutorial-for-unity-3d-4/

//べた塗シェーダー

// シェーダーの情報
Shader "Unlit/MaskShader"
{
    // Unity上でやり取りをするプロパティ情報
    // マテリアルのInspectorウィンドウ上に表示され、スクリプト上からも設定できる
    Properties
    {
        _Color("Main Color", Color) = (1,1,1,1) // Color プロパティー (デフォルトは白) 
    }
        // サブシェーダー
        // シェーダーの主な処理はこの中に記述する
        // サブシェーダーは複数書くことも可能が、基本は一つ
        //複数定義することでPCとスマートフォンなど、プラットフォームごとに処理を切り替えることができるもの
        SubShader
    {
        // パス
        // 1つのオブジェクトの1度の描画で行う処理をここに書く
        // これも基本一つだが、複雑な描画をするときは複数書くことも可能
        //オブジェクトのレンダリング処理
        Pass
        {
            CGPROGRAM   // プログラムを書き始めるという宣言

            // 関数宣言
            #pragma vertex vert    // "vert" 関数を頂点シェーダー使用する宣言
            #pragma fragment frag  // "frag" 関数をフラグメントシェーダーと使用する宣言

            // 変数宣言
            fixed4 _Color; // マテリアルからのカラー

		struct appdata
		{
			//頂点座標
            //POSITION セマンティクスをつけているので頂点座標の情報が入る
            half4 vertex : POSITION;

            // UV座標
            //TEXCOORD セマンティクスをつけているのでuv座標の情報が入る
            half2 uv : TEXCOORD;
		};

		//出力情報
		struct v2f
		{
            half4  pos : SV_POSITION;

            // 変換前の頂点座標
            half4 pos2 : TEXCOORD0;
		};

		//頂点シェーダー
		v2f vert(appdata a)
		{
			v2f v;

            //// 擬似乱数を生成
            //half  rand = frac(sin(dot(a.uv, fixed2(12.9898, 78.233))) * 43758.5453);

            //// 頂点を押し出す
            //v.pos = a.vertex * (1 + rand * 0.3);
			
			//UnityObjectToClipPos：3次元情の頂点を、ディスプレイのどこに描画するか
			v.pos = UnityObjectToClipPos(a.vertex);
			
			return v;
		}
            
	    // フラグメントシェーダー
		//ピクセルを何色に塗るか決める
		fixed4 frag(v2f flagV) : SV_Target
	    {
			//今回はプロパティの色
		    return _Color;
	    }

        	ENDCG   // プログラムを書き終わるという宣言
		}
    }
}