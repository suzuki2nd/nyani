
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

struct appdata
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
};

struct v2f
{
    half4 pos : SV_POSITION;
};

half _OutlineWidth;

//頂点シェーダー
v2f vert(appdata v)
{
    v2f o = (v2f) 0;
    o.pos = UnityObjectToClipPos(v.vertex + v.normal * _OutlineWidth);
    return o;
}

uniform float4 _OutlineColor;

//フラッグシェーダー
//ピクセルシェーダーのようなもの
fixed4 frag(v2f i) : SV_Target
{
    fixed4 col = _OutlineColor;
    return col;
}