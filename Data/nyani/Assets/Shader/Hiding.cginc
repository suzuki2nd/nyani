
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

struct appdata
{
    half4 vertex : POSITION;
};

struct v2f
{
    half4 pos : SV_POSITION;
};

float4 _silhouette;

v2f vert(appdata v)
{
    v2f o = (v2f) 0;
    o.pos = UnityObjectToClipPos(v.vertex);

    return o;
}

float4 frag(v2f i) : SV_Target
{            
    return _silhouette;
}