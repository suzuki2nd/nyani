
Shader "CatParticle"
{
    // Unity上でやり取りをするプロパティ情報
    // マテリアルのInspectorウィンドウ上に表示され、スクリプト上からも設定できる
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }

        SubShader
    {
          Tags
        {
            "RenderType" = "Opaque"
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
        }

        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {

            CGPROGRAM   // プログラムを書き始めるという宣言

            // 関数宣言
            #pragma vertex vert    // "vert" 関数を頂点シェーダー使用する宣言
            #pragma fragment frag  // "frag" 関数をフラグメントシェーダーと使用する宣言

        struct appdata
        {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
        // UV座標
//TEXCOORD セマンティクスをつけているのでuv座標の情報が入る
        float2 uv0 : TEXCOORD0;
    };

    //出力情報
    struct v2f
    {
        float4  pos : SV_POSITION;

        // 変換前の頂点座標
        float2 uv0 : TEXCOORD0;
    };

    sampler2D _MainTex;

    //頂点シェーダー
    v2f vert(appdata a)
    {
        v2f v;

        v.pos = UnityObjectToClipPos(a.vertex + a.normal * 0.01f);
        v.uv0 = a.uv0;

        return v;
    }

    // フラグメントシェーダー
    //ピクセルを何色に塗るか決める
    float4 frag(v2f v) : SV_Target
    {
        float4 col = tex2D(_MainTex, v.uv0);

        if (col.a <= 0)
            discard;

        return float4(0,0,0,1);
    }

        ENDCG   // プログラムを書き終わるという宣言
    }


          Pass
        {
            ZTest Always    // 深度に関わらず描画
            CGPROGRAM   // プログラムを書き始めるという宣言

            // 関数宣言
            #pragma vertex vert    // "vert" 関数を頂点シェーダー使用する宣言
            #pragma fragment frag  // "frag" 関数をフラグメントシェーダーと使用する宣言

        struct appdata
        {
        //頂点座標
        //POSITION セマンティクスをつけているので頂点座標の情報が入る
        float4 vertex : POSITION;

        // UV座標
        //TEXCOORD セマンティクスをつけているのでuv座標の情報が入る
        float2 uv0 : TEXCOORD0;
    };

    //出力情報
    struct v2f
    {
        float4  pos : SV_POSITION;


        // 変換前の頂点座標
        float2 uv0 : TEXCOORD0;
    };

    sampler2D _MainTex;

    //頂点シェーダー
    v2f vert(appdata a)
    {
        v2f v;

        v.pos = UnityObjectToClipPos(a.vertex);
        v.uv0 = a.uv0;

        return v;
    }

    // フラグメントシェーダー
    //ピクセルを何色に塗るか決める
    float4 frag(v2f v) : SV_Target
    {
        float4 col = tex2D(_MainTex, v.uv0);
       
        if (col.a <= 0)
            discard;

        //col.xyz = col.xyz * _SinTime;

        return col;
    }

        ENDCG   // プログラムを書き終わるという宣言
    }
  
    }
}