//ライティングモデル
//#pragma surface [Surface Shader関数名] [ライティングの方法]
#pragma surface surf ToonRamp

//コンパイルターゲット
#pragma target 3.0

sampler2D _MainTex;
sampler2D _RampTex;
float val;

// surf関数の入力に使う構造体
// 使用しなくても定義する必要があるのでダミーの変数を定義しておく
struct Input
{
    // Input構造体にuv_[テクスチャ名]と宣言すればUVが取得できる
    float2 uv_MainTex;
    float3 worldPos;
};

fixed4 _Color;

// カスタムのライティング関数
 // Lightingから始まる名前を付ける必要がある
fixed4 LightingToonRamp(SurfaceOutput s, fixed3 lightDir, fixed atten)
{
    half d = dot(s.Normal, lightDir) * 0.5 + 0.5;
    fixed3 ramp = tex2D(_RampTex, fixed2(d, 0.5)).rgb;
    fixed4 c;
    c.rgb = s.Albedo * _LightColor0.rgb * ramp;
    c.a = 0;

    return c;
}

                         // この関数の中で表面の材質に関するパラメータを設定する
void surf(Input IN, inout SurfaceOutput o)
{
                             //ローカル座標計算
    float3 localPos = IN.worldPos - mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;

                             //サンプリング
    float4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

    if (localPos.y < val)
    {
        o.Albedo = c.rgb;
    }

    o.Alpha = c.a;
}