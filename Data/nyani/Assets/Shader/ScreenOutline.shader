Shader "Custom/ScreenOutliners"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Amp("Amp", Float) = 0.1
        _T("T", Float) = 0.25
    }
        SubShader
        {
            // No culling or depth
            Cull Off ZWrite Off ZTest Always

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = v.uv;
                    return o;
                }

                sampler2D _MainTex;
                float _val;
                int _isEffect;

                fixed4 frag(v2f i) : SV_Target
                {
                    float2 uv = 2 * i.uv - 1;

                    float r = length(uv);
                    
                    float offset = 1.2f;
                    r = r - offset;                   
                    float n = smoothstep(0.15,0.15, saturate(r));

                    fixed4 col = tex2D(_MainTex, i.uv);

                    return (1-n) * col;
                }

                ENDCG
            }
        }
}
