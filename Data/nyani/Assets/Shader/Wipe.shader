Shader "Custom/Wipe"
{
    Properties
    {
        _Radius("Radius", Range(0,2)) = 2
        _MainTex("Texture", 2D) = "white" {}
        _Color("Color" , Color) = (0,0,0,1)
    }

        SubShader
    {
        Pass
        {
            CGPROGRAM

            #include "UnityCG.cginc"

            #pragma vertex vert_img
            #pragma fragment frag

            float _Radius;
            fixed4 _Color;          //�J���[
            sampler2D _MainTex;


            fixed4 frag(v2f_img i) : COLOR
            {
                fixed2 u = i.uv;

                i.uv -= fixed2(0.5, 0.5);
                i.uv.x *= 16.0 / 9.0;


                if (distance(i.uv, fixed2(0,0)) < _Radius)
                {
                    discard;
                }

                return tex2D(_MainTex, u);
            }
                ENDCG
            }
    }
}