//隠れている部分描画
//https://zenn.dev/nanbokku/articles/unity-stencil-silhouette


Shader "Custom/Cat"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _silhouette("Silhouette", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB)", 2D) = "white"{}

        _RampTex("Ramp", 2D) = "white"{}
        _OutlineColor("Outline Color", Color) = (0.5,0.5,0.5,1)
        _OutlineWidth("Outline Width", float) = 0.1
    }

        SubShader
        {
            Tags
            {
                "RenderType" = "Opaque"
                "Queue" = "Transparent+1"
            }

            //隠れている部分あぶり出し
            Pass
            {
                //マップの壁が 1 で塗られているので
                //そのピクセルと等しい場所のリファレンス値を 2 にする
                Stencil
                {
                    Ref 1
                    Comp Equal      // 1と一致するものに対して処理
                    Pass IncrSat    // インクリメントする(1 + 1 = 2)
                }

                ColorMask 0     // ステンシルのみ書き込み
                ZTest Always    // 深度に左右されずに書き込む
                ZWrite Off      // デプスバッファに書き込まない
            }

            //アウトライン描画
            //隠れていない部分
            Pass
            {
                Stencil
                {
                    Ref 3
                    Comp Always     // ZTest Alwaysではないので隠れている部分は対象外
                    Pass Replace    // ステンシルバッファを3に書き換え
                }

                Cull Front

                CGPROGRAM

                #include "Outline.cginc"

                ENDCG
            }

            //トゥーン描画
            //隠れていない部分
            Stencil
            {
                Ref 3
                Comp Always     // ZTest Alwaysではないので隠れている部分は対象外
                Pass Replace    // ステンシルバッファを3に書き換え
            }

            CGPROGRAM

            //ライティングモデル
            //#pragma surface [Surface Shader関数名] [ライティングの方法]
            #pragma surface surf ToonRamp

            //コンパイルターゲット
            #pragma target 3.0

            sampler2D _MainTex;
            sampler2D _RampTex;
            float val;

            // surf関数の入力に使う構造体
            // 使用しなくても定義する必要があるのでダミーの変数を定義しておく
            struct Input
            {
                // Input構造体にuv_[テクスチャ名]と宣言すればUVが取得できる
                float2 uv_MainTex;
                float3 worldPos;
            };

            fixed4 _Color;

            // カスタムのライティング関数
            // Lightingから始まる名前を付ける必要がある
            fixed4 LightingToonRamp(SurfaceOutput s, fixed3 lightDir, fixed atten)
            {
                half d = dot(s.Normal, lightDir) * 0.5 + 0.5;
                fixed3 ramp = tex2D(_RampTex, fixed2(d, 0.5)).rgb;
                fixed4 c;
                c.rgb = s.Albedo * _LightColor0.rgb * ramp;
                c.a = 0;

                return c;
            }

            // この関数の中で表面の材質に関するパラメータを設定する
            void surf(Input IN, inout SurfaceOutput o)
            {
                //ローカル座標計算
                float3 localPos = IN.worldPos - mul(unity_ObjectToWorld, float4(0, 0, 0, 1)).xyz;

                //サンプリング
                float4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

                o.Albedo = c.rgb;

                o.Alpha = c.a;
            }

            ENDCG


                //隠れている部分を描画
                //2 が書き込まれている部分
                Pass
                {
                    ZTest Always

                    Stencil
                    {
                        Ref 2
                        Comp Equal
                    }

                    CGPROGRAM

                    #include "Hiding.cginc"

                    ENDCG
                }
        }
}