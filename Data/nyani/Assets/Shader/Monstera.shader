Shader "Custom/Monstera"
{
	Properties{
	_Color("Color", Color) = (1, 1, 1, 1)
	_MainTex("Albedo (RGB)", 2D) = "white" {}
	_SubTex("Albedo (RGB)", 2D) = "white" {}
	_Glossiness("Smoothness"  , Range(0, 1)) = 0.5
	_Metallic("Metallic"    , Range(0, 1)) = 0.0
	_Cutoff("Cutoff"      , Range(0, 1)) = 0.5

		_OutlineColor("Outline Color", Color) = (0.5,0.5,0.5,1)
		_OutlineWidth("Outline Width", float) = 0.1
	}

		SubShader
	{

		//アウトライン描画
		//隠れていない部分
		Pass
		{
			Stencil
			{
				Ref 1
				Comp always
				Pass replace
			}

			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off

		Tags
		{
			"Queue" = "AlphaTest"
			"RenderType" = "TransparentCutout"
			"LightMode" = "ForwardBase"
		}


			   CGPROGRAM

#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : NORMAL;
		};

		struct v2f
		{
			half4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
		};

		half _OutlineWidth;
		sampler2D _SubTex;

		//頂点シェーダー
		v2f vert(appdata v)
		{
			v2f o = (v2f)0;
			o.uv = v.uv;
			o.pos = UnityObjectToClipPos(v.vertex + v.normal * _OutlineWidth);
			return o;
		}

		uniform float4 _OutlineColor;

		//フラッグシェーダー
		//ピクセルシェーダーのようなもの
		fixed4 frag(v2f i) : SV_Target
		{
			if (tex2D(_SubTex, i.uv).a <= 0)
			{
				discard;
			}

			fixed4 col = _OutlineColor;

			return col;
		}
			   ENDCG
		}


		 Stencil
		{
			Ref 1
			Comp always
			Pass replace
		}

		Tags
		{
			"Queue" = "AlphaTest"
			"RenderType" = "TransparentCutout"
			"LightMode" = "ForwardBase"
		}
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
				#pragma target 3.0
				#pragma surface surf Standard fullforwardshadows alphatest:_Cutoff ToonRamp

				fixed4 _Color;
				sampler2D _MainTex;
				sampler2D _RampTex;

				struct Input
				{
					float2 uv_MainTex;
				};

				// カスタムのライティング関数
				// Lightingから始まる名前を付ける必要がある
				fixed4 LightingToonRamp(SurfaceOutput s, fixed3 lightDir, fixed atten)
				{
					half d = dot(s.Normal, lightDir) * 0.5 + 0.5;
					fixed3 ramp = tex2D(_RampTex, fixed2(d, 0.5)).rgb;
					fixed4 c;
					c.rgb = s.Albedo * _LightColor0.rgb * ramp;
					c.a = 0;

					return c;
				}

				void surf(Input IN, inout SurfaceOutputStandard o)
				{
					fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

					o.Albedo = c.rgb;
					o.Alpha = c.a;
				}

			ENDCG
	}

		FallBack "Transparent/Cutout/Diffuse"
}