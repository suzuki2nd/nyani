Shader "Custom/Cursor"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Amp("Amp", Float) = 0.1
        _T("T", Float) = 0.25
    }
        SubShader
        {
            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Cull Off 
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha


            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"


                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = v.uv;
                    return o;
                }

                sampler2D _MainTex;
                float4 nikukyu;

                fixed4 frag(v2f i) : SV_Target
                {
                    fixed4 col = tex2D(_MainTex, i.uv);

                if (col.a <= 0.0f) return col;

                if (col.a < 1.0f)
                {
                    col = nikukyu;
                }
                    return col;
                }

                ENDCG
            }
        }
}
