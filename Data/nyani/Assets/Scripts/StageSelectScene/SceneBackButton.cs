using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 指定のシーンに戻るボタン
/// </summary>
public class SceneBackButton : ImageButtonBase
{
    private string sceneName = "";

    private new void Start()
    {
        base.Start();
        seName = "BackButtondown";
    }

    /// <summary>
    /// 戻りたいシーンを設定
    /// </summary>
    /// <param name="name"> 戻りたいシーン名 </param>
    public void SetSceneName(string name)
    {
        sceneName = name;
    }

    public override void OnGamePadButtonDown()
    {
        base.OnGamePadButtonDown();

        BackScene();
    }

    /// <summary>
    /// ゲームパッドのボタンが押されたときに呼ばれる関数
    /// </summary>
    public override void OnGamePadButtonDown(int num)
    {
        base.OnGamePadButtonDown();
        BackScene();
    }

    private void BackScene()
    {
        //シーン名が設定されているとき
        if (sceneName != "")
            SceneManager.LoadScene(sceneName);
    }

    public void OnclickButton()
    {
    }
}
