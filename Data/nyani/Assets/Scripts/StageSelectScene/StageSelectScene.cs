//Fling to the Finish
//Fling to the Finish
//https://www.youtube.com/watch?v=NKcAwiUr4NA
//fall guys
//https://www.youtube.com/watch?v=E5Xdu1pUhJY
//スーパーバニーマン
//https://www.youtube.com/watch?v=vpv561c0Cho
//
//https://www.youtube.com/watch?v=e8GJ9o2EwiU

//ハンコ
//http://hanko.ginneko-do.com/hpsozai/hanko/hanko_sozai2.html


using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ステージセレクトシーンクラス
/// </summary>
public class StageSelectScene : SceneBase
{
    [SerializeField] private GameObject stageImageObj;
    [SerializeField] private GameObject backSceneObj;
    [SerializeField] private GameObject previewCameraObj;
    [SerializeField] private GameObject catStamp;

    [SerializeField] private GameObject[] lookPos;

    [SerializeField] private RenderTexture[] renderTexObjs;

    [SerializeField] private Sprite[] sprites;
    private GameObject countDownObj;
    private GameObject[] previewCameras;


    new void Start()
    {
        base.Start();
        
        FadeManagerTest.Instance.InScene(1.0f);
        StageCollector.Instance.Start();

        GameObject cursor = GameObject.FindGameObjectWithTag("Cursor");
        //ステージ画像生成
        GameObject stageImage = Instantiate(stageImageObj, transform);
        stageImage.transform.SetParent(cursor.transform, false);

        //戻るボタン生成
        GameObject back = Instantiate(backSceneObj, transform);
        back.transform.SetParent(cursor.transform, false);
        //戻るシーン先の設定
        back.GetComponent<SceneBackButton>().SetSceneName("CharacterSelectScene");

        //カーソルの生成
        //操作を行うのは 1P のみなので 0
        base.CursorGeneration<StageSelectCursor>(0, Vector2.one * 50.0f , cursor);

        countDownObj = GameObject.FindGameObjectWithTag("CountDown");
        countDownObj.SetActive(false);
        canCountDown = false;

        previewCameras = new GameObject[(int)StageType.MAX];

        for(int index = 0;index < (int)StageType.MAX; ++index)
        {
            previewCameras[index] = Instantiate(previewCameraObj);
            previewCameras[index].GetComponent<Camera>().targetTexture = renderTexObjs[index];
            previewCameras[index].GetComponent<StagePreviewCamera>().SetLookPos(lookPos[index].transform.position);
            previewCameras[index].transform.position = lookPos[index].transform.position + Vector3.up * 10.0f;
        }

        SoundManager sound = SoundManager.Instance;
        sound.SetBgm("SelectBgm", "SelectBgm");
        sound.SetSE("StageSelect", "StageSelect");

        //  this.GetComponent<Image>().color = Color.white * 0.0f;
    }

    new void Update()
    {
        SoundManager.Instance.PlayBGM("SelectBgm");

        //仮の方法
        if (StageCollector.Instance.GetStageType() != StageType.NONE && canCountDown == false)
        {
            canCountDown = true;
            StartCoroutine(CatStamp());
            StartCoroutine(CountDown(3.0f));
        }
    }

    public void EnablePreview(int num)
    {
        previewCameras[num].GetComponent<StagePreviewCamera>().EnableRotate();
    }

    public void DisablePreview(int num)
    {
        previewCameras[num].GetComponent<StagePreviewCamera>().DisableRotate();
    }


    private IEnumerator CatStamp()
    {
        float time = 0;
        int turn = 0;
        int interval = 24;
        GameObject stamp = Instantiate(catStamp);
        stamp.transform.SetParent(GameObject.FindGameObjectWithTag("Cursor").transform, false);

        Vector2[] stampPos = new Vector2[(int)StageType.MAX]
        {
            new Vector2(-225.0f,0.0f),
            new Vector2(0.0f,0.0f),
            new Vector2(225.0f,0.0f),
        };
        
        
        SoundManager.Instance.PlaySE("StageSelect");
        StageCollector collector = StageCollector.Instance;

        RectTransform rect = stamp.GetComponent<RectTransform>();
        rect.localPosition = stampPos[(int)collector.GetStageType()];

        while (time <= 1)
        {
            if (turn < 360 + interval)
            {
                rect.localRotation = Quaternion.Euler(0, 0, turn);
                turn += interval;
            }

            rect.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, time * 5.0f);


            time += Time.deltaTime;

            yield return 0;
        }
    }

    /// <summary>
    /// カウントダウンコルーチン
    /// </summary>
    /// <param name="countTime"> カウントダウンの秒数 </param>
    /// <returns></returns>
    private IEnumerator CountDown(float countTime)
    {
        float time = 0;
        float turn = 0;
        float interval = Time.deltaTime * 5.0f;
        countDownObj.SetActive(true);

        //Vector3[] catPos = new Vector3[(int)StageType.MAX]
        //{
        //    new Vector3(-0.3f, 0.65f, 1.0f),
        //    new Vector3(0.3f, 0.65f, 1.0f),
        //    new Vector3(0.9f, 0.65f, 1.0f),
        //};

    //    Vector3[] catPos = new Vector3[(int)StageType.MAX]
    //{
    //        new Vector3(-1.4f, 0.0f, 5.0f),
    //        new Vector3(1.4f, 0.0f, 5.0f),
    //        new Vector3(4.4f, 0.0f, 5.0f),
    //};

    //    Transform catTransform = this.transform;
    //    StageCollector collector = StageCollector.Instance;
    //    catTransform.position = catPos[(int)collector.GetStageType()];

    //    GameObject cat = Instantiate(previewCat, catTransform);
    //    cat.GetComponent<Animator>().SetBool("emoteGenbaneko", true);
    //    GameObject poly = cat.transform.Find("polySurface30").gameObject;
    //    Material material = poly.GetComponent<Renderer>().material;
    //    material.SetFloat("val", 1);

    //    cat.transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);

        while (time <= 1)
        {
            turn = CountDownAnime(time, turn, interval);
            time += Time.deltaTime;
            yield return 0;
        }

        turn = 0;

        while (time <= 2)
        {
            turn = CountDownAnime(time, turn, interval);
            time += Time.deltaTime;
            yield return 0;
        }

        turn = 0;

        while (time <= 3)
        {
            turn = CountDownAnime(time, turn, interval);
            time += Time.deltaTime;
            yield return 0;
        }

        FadeManagerTest.Instance.LoadScene("PlayScene", 1.0f , true);
    }

    private float CountDownAnime(float time, float val, float interval)
    {
        countDownObj.GetComponent<Image>().sprite = sprites[(int)time];

        RectTransform rect = countDownObj.GetComponent<RectTransform>();

        if (val < 0.8f)
        {
            rect.localScale = Vector3.one * val;
            val += interval;
        }

        return val;
    }
}
