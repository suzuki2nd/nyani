using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ステージの画像ボタンクラス
/// </summary>
public class StageImageButton : ImageButtonBase
{
    private Vector2 defaultSizeDelta;

    private new void Start()
    {
        base.Start();

        defaultSizeDelta = new Vector2(128.0f, 256.0f);

        //Color col = this.GetComponent<Image>().color;
        //col.a = 0.5f;
    }

    /// <summary>
    /// ゲームパッドのボタンが押された際に呼び出される処理
    /// </summary>
    public override void OnGamePadButtonDown()
    {
        base.OnGamePadButtonDown();

        StageCollector collector = StageCollector.Instance;
        StageHelper helper = StageHelper.Instance;

        //選択されたステージを設定
        collector.SelectStage(helper.GetType(this.gameObject.tag));

        //アウトライン表示
        transform.parent.GetComponent<Outline>().effectDistance = new Vector2(10.0f, 10.0f);

        cursor.GetComponent<CursorBase>().DisebleMove();
    }

    new private void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta =new Vector2(150.0f, 270.0f);

        RectTransform parent = transform.parent.GetComponent<RectTransform>();
        parent.sizeDelta = new Vector2(150.0f, 270.0f);

        RectTransform child = this.transform.Find("Preview").GetComponent<RectTransform>();
        child.sizeDelta = new Vector2(child.sizeDelta.x, 270.0f);
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    new private void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = defaultSizeDelta;

        RectTransform parent = transform.parent.GetComponent<RectTransform>();
        parent.sizeDelta = defaultSizeDelta;

        RectTransform child = this.transform.Find("Preview").GetComponent<RectTransform>();
        child.sizeDelta = new Vector2(256.0f, 256.0f);
    }
}
