using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ステージを選択するカーソルクラス
/// </summary>
public class StageSelectCursor : CursorBase
{
    private bool isSelect;
    private GameObject target;

    new void Start()
    {
        base.Start();

        isSelect = false;
        target = null;

        RectTransform rect = this.GetComponent<RectTransform>();
        float offset = 2.0f;

        //画面サイズによって位置が変わってしまうので、対処必要
        rightSideLimit = new Vector2(Screen.currentResolution.width / 4 - rect.sizeDelta.x * offset, -(Screen.currentResolution.height / 4 - rect.sizeDelta.y * 1.5f));

        //画面サイズによって位置が変わってしまうので、対処必要
        leftSideLimit = new Vector2(-(Screen.currentResolution.width / 4 - rect.sizeDelta.x * offset) , Screen.currentResolution.height / 4 - rect.sizeDelta.y * 1.5f);
    }

    new void Update()
    {
        base.CursorMove();

        GamePadButtonDown();
    }

    /// <summary>
    /// ゲームパッドのボタンが押された際の処理
    /// </summary>
    protected override void GamePadButtonDown()
    {
        //ゲームパッドBボタンを押したとき
        if (MyInput.GetAButtonDown(gamePadNum))
        {
            base.GamePadButtonDown();

            //カーソルがアイコンの上にある場合
            if (isSelect == true)
            {
                target.GetComponent<ImageButtonBase>().OnGamePadButtonDown();
            }
        }
    }

    /// <summary>
    /// 当たり続けている場合に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        isSelect = true;
        target = collision.gameObject;

        if (collision.gameObject.tag.Equals("Untagged")) return;

        StageHelper helper = StageHelper.Instance;
        sceneAdmin.GetComponent<StageSelectScene>().EnablePreview((int)helper.GetType(collision.gameObject.tag));
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        isSelect = false;

        if (collision.gameObject.tag.Equals("Untagged")) return;

        StageHelper helper = StageHelper.Instance;
        sceneAdmin.GetComponent<StageSelectScene>().DisablePreview((int)helper.GetType(collision.gameObject.tag));
    }
}

 
