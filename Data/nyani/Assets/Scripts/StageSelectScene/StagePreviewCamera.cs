using System;
using UnityEngine;

public class StagePreviewCamera : MonoBehaviour
{
    private bool isRotate;
    private Vector3 lookPos;

    void Start()
    {
        isRotate = false;
    }

    void Update()
    {
        transform.RotateAround(lookPos, Vector3.up, Convert.ToInt32(isRotate));
    }

    public void EnableRotate()
    {
        isRotate = true;
    }

    public void DisableRotate()
    {
        isRotate = false;
    }

    public void SetLookPos(Vector3 look)
    {
        lookPos = look;
    }
}
