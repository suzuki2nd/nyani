using UnityEngine;

public class EntrySceneCursor : CursorBase
{
    private bool isSelect;

    private GameObject target = null;

    new void Start()
    {
        base.Start();

        //初期化
        isSelect = false;

        RectTransform rect = this.GetComponent<RectTransform>();
        float offset = 2.0f;

        //画面サイズによって位置が変わってしまうので、対処必要
        rightSideLimit = new Vector2(Screen.currentResolution.width / 4 - rect.sizeDelta.x * offset , -(Screen.currentResolution.height / 4 - rect.sizeDelta.y * 1.5f));

        //画面サイズによって位置が変わってしまうので、対処必要
        leftSideLimit = new Vector2(-(Screen.currentResolution.width / 4 - rect.sizeDelta.x * offset) , Screen.currentResolution.height / 4 - rect.sizeDelta.y * 1.5f);
    }

    new void Update()
    {
        base.CursorMove();
        GamePadButtonDown();
    }

    /// <summary>
    /// ゲームパッドのボタンが押された際の処理
    /// </summary>
    protected override void GamePadButtonDown()
    {
        //ゲームパッドAボタンを押したとき
        if (MyInput.GetAButtonDown(0))
        {
            base.GamePadButtonDown();

            //カーソルがアイコンの上にある場合
            //かつ、シーン遷移を行わない場合
            if (isSelect == true)
            {
                //最後に触れたボタンが押された処理を行う
                target.GetComponent<ImageButtonBase>().OnGamePadButtonDown();
            }
        }
    }

    /// <summary>
    /// 当たり続けている場合に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        //セレクトしている状態
        isSelect = true;

        //OnGamePadButtonDown を実行するために保持しておく
        target = collision.gameObject;
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        isSelect = false;
    }
}
