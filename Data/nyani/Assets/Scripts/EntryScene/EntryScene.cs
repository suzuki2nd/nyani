using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryScene : SceneBase
{
    [SerializeField] private GameObject backSceneObj;

    private new void Start()
    {
        FadeManagerTest.Instance.InScene(1.0f);
        base.Start();

        GameObject canvas = GameObject.FindGameObjectWithTag("Cursor");


        //戻るボタン生成
        GameObject back = Instantiate(backSceneObj, transform);
        back.transform.SetParent(canvas.transform, false);

        RectTransform backRect = back.GetComponent<RectTransform>();
        backRect.localPosition = new Vector2(-320, 170);

        //戻るシーン先の設定
        back.GetComponent<SceneBackButton>().SetSceneName("TitleScene");

        //カーソルの生成
        //操作を行うのは 1P のみなので 0
        base.CursorGeneration<EntrySceneCursor>(0, Vector2.one * 50.0f, canvas);


        SoundManager.Instance.SetBgm("SelectBgm", "SelectBgm");
    }

    private new void Update()
    {
        base.Update();

        SoundManager.Instance.PlayBGM("SelectBgm");
    }
}
