using UnityEngine;

public class PlayerEntryButton : ImageButtonBase
{
    [SerializeField] short entryNum;
    private float defaultSizeDelta;
    private float selectSizeDelta;

    private new void Start()
    {
        base.Start();

        defaultSizeDelta = 150.0f;
        selectSizeDelta = 140.0f;
    }
        

    /// <summary>
    /// ゲームパッドのボタンが押された際に呼び出される処理
    /// </summary>
    public override void OnGamePadButtonDown()
    {
        base.OnGamePadButtonDown();

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = Vector2.one * selectSizeDelta;

        PlayHelper.Instance.SetEntryNum(entryNum);
        FadeManagerTest.Instance.LoadScene("CharacterSelectScene", 1.0f);

        cursor.GetComponent<CursorBase>().DisebleMove();
    }


    new private void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(160.0f, 160.0f);
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
     new private void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = Vector2.one * defaultSizeDelta;
    }
}
