using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.ResourceManagement;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public sealed class AddressablesManager : Singleton<AddressablesManager>
{
    private static readonly Regex REGEX = new Regex("Key=(.*), Type=(.*)");
    private AsyncOperationHandle<GameObject> handle;
    private AsyncOperationHandle<Texture> texHandle;
    private AsyncOperationHandle<Texture2D> tex2DHandle;
    private AsyncOperationHandle<Sprite> spriteHandle;
    private AsyncOperationHandle<AudioClip> audioClipHandle;


    /// <summary>
    /// コンストラクタ
    /// </summary>
    public AddressablesManager()
    {
        //コールバックの登録
        ResourceManager.ExceptionHandler = ErrorHandler;
    }

    /// <summary>
    /// 指定されたアドレスのアセットをロードし、原点に生成
    /// 親を指定した場合は、ローカル座標系の原点に生成
    /// </summary>
    /// <param name="address"></param>
    public async System.Threading.Tasks.Task<GameObject> InstantiateAsset(string address, Transform parent = null)
    {
        Vector3 pos = new Vector3();
        if (parent != null)
        {
            pos = parent.transform.position;
        }
        else
        {
            pos = Vector3.zero;
        }
        Quaternion rotation = new Quaternion();
        rotation = Quaternion.identity;
        handle = Addressables.InstantiateAsync(address, new InstantiationParameters(pos, rotation, parent));
        await handle.Task;
        return handle.Result;
    }

    /// <summary>
    /// 指定されたアドレスのアセットを、指定された座標、回転で生成
    /// </summary>
    /// <param name="address">アドレス</param>
    /// <param name="pos">座標</param>
    /// <param name="rotation">回転</param>
    /// <param name="parent">親のTransform</param>
    public async System.Threading.Tasks.Task<GameObject> InstantiateAsset(string address, Vector3 pos, Quaternion rotation, Transform parent = null)
    {
        if(parent != null)
        {
            pos = parent.transform.position + pos;
        }
        handle = Addressables.InstantiateAsync(address, new InstantiationParameters(pos, rotation, parent));
        await handle.Task;
        return handle.Result;
    }

    /// <summary>
    /// 指定されたアドレスのアセットを、指定されたTransformで生成
    /// </summary>
    /// <param name="address"></param>
    /// <param name="transform"></param>
    public async System.Threading.Tasks.Task<GameObject> InstantiateAsset(string address, Transform transform, Transform parent = null)
    {
        handle = Addressables.InstantiateAsync(address, new InstantiationParameters(transform.position, transform.rotation, parent));
        await handle.Task;
        return handle.Result;
    }

    public async System.Threading.Tasks.Task<GameObject> LoadAsset(string address)
    {
        handle = Addressables.LoadAssetAsync<GameObject>(address);
        await handle.Task;
        return handle.Result;
    }


    public async System.Threading.Tasks.Task<Texture> LoadTextureAsset(string address)
    {
        texHandle = Addressables.LoadAssetAsync<Texture>(address);

        await texHandle.Task;

        return texHandle.Result;
    }

    public void ReleaseTextureAssets(Texture texture)
    {
        Addressables.Release(texture);
    }

    public async System.Threading.Tasks.Task<Texture2D> LoadTexture2DAsset(string address)
    {
        tex2DHandle = Addressables.LoadAssetAsync<Texture2D>(address);

        await tex2DHandle.Task;

        return tex2DHandle.Result;
    }

    public void ReleaseTexture2DAssets(Texture2D texture)
    {
        Addressables.Release(texture);
    }


    public async System.Threading.Tasks.Task<Sprite> LoadSpriteAsset(string address)
    {
        spriteHandle = Addressables.LoadAssetAsync<Sprite>(address);

        await spriteHandle.Task;

        return spriteHandle.Result;
    }

    public void ReleaseSpriteAssets(Sprite texture)
    {
        Addressables.Release(texture);
    }

    public async System.Threading.Tasks.Task<AudioClip> LoadAudioClipAsset(string address)
    {
        audioClipHandle = Addressables.LoadAssetAsync<AudioClip>(address);

        await audioClipHandle.Task;

        return audioClipHandle.Result;
    }

    public void ReleaseAudioClipAssets(AudioClip texture)
    {
        Addressables.Release(texture);
    }


    /// <summary>
    /// オブジェクトの参照カウンタを-1してリリースする。
    /// Addressables.InstantiateAsyncを使ってロードしたオブジェクトの寿命は所属するシーンに属するが、
    /// それ以外の任意のタイミングでオブジェクトをリリースしたい場合にこの関数を使用する。
    /// </summary>
    /// <param name="instance">削除したいオブジェクト</param>
    public void ReleaseInstance(GameObject instance)
    {
        Addressables.ReleaseInstance(instance);
    }

    /// <summary>
    /// エラー補足時に呼ばせるためのコールバック関数
    /// </summary>
    /// <param name="handle">ハンドル</param>
    /// <param name="exception">補足された例外</param>
    private void ErrorHandler(AsyncOperationHandle handle, Exception exception)
    {
        string message = exception.Message;
        //アドレスが間違っているわけではなかった
        if (message.Contains("'UnityEngine.AddressableAssets.InvalidKeyException'") == false)
        {
            Debug.LogError(message);
            return;
        }
        
        Match match = REGEX.Match(message);
        //アドレスと型がエラーメッセージに含まれていない
        if (match.Success == false)
        {
            Debug.LogError(message);
            return;
        }

        GroupCollection groups = match.Groups;
        string key = groups[1].Value;
        string type = groups[2].Value;
        Debug.LogError("Asset Loading Error : " + $"{key} が見つかりませんでした。（{type} 型）");
    }
}