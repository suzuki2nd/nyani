using UnityEngine;

public class AddressablesSample : MonoBehaviour
{
    private GameObject Obj;
    private GameObject Obj2;
    private AddressablesManager AddressablesManager;

    private async void Start()
    {
        //インスペクタ上でAddressablesのチェックボックスの左に表示されている文字列。任意の文字列を指定可能
        string address = "Cube";
        Vector3 pos = new Vector3(-20.0f, 1.0f, -17.0f);
        Quaternion rotation = Quaternion.identity;
        AddressablesManager = new AddressablesManager();
        Obj = await AddressablesManager.InstantiateAsset(address, pos, rotation);

        Vector3 pos2 = new Vector3(-18.0f, 1.0f, -17.0f);
        Obj2 = await AddressablesManager.InstantiateAsset(address, pos2, rotation);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddressablesManager.ReleaseInstance(Obj);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            AddressablesManager.ReleaseInstance(Obj2);
        }
    }
}