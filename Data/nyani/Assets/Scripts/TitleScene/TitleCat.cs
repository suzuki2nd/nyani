using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleCat : MonoBehaviour
{
    int cnt;
    int texIndex;
    Material mat;
    [SerializeField] Texture[] textures;

    private void Start()
    {
        cnt = 0;
        texIndex = 0;

        mat = this.GetComponent<Renderer>().material;
    }

    private void OnTriggerEnter(Collider other)
    {
        cnt++;

        if (cnt == 1)
        {
            texIndex = texIndex == 3 ? 0 : texIndex + 1;
            cnt = 0;
        }

        CharacterHelper helper = CharacterHelper.Instance;
        //mat.SetTexture("_MainTex", helper.GetMainTex(texIndex));
        mat.SetTexture("_MainTex", textures[texIndex]);
    }
}
