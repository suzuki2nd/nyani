using UnityEngine;

[ExecuteInEditMode]
public class CameraFilter : MonoBehaviour
{
    [SerializeField] private Material filter;
    [SerializeField] private Color color;
    float val;
    int isEffect = 0;

    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        val = Mathf.Abs(Mathf.Sin(Time.time) * 0.5f);

        filter.SetColor("_mask", color);
        filter.SetFloat("_val",val);
        filter.SetInt("_isEffect", isEffect);
        Graphics.Blit(src, dest, filter);
    }

    private void Update()
    {
        filter.SetColor("_mask", color);
    }

    public void SetColor(Color col)
    {
        color = col;
    }

    public Color GetColor()
    {
        return color;
    }

    public void EnableEffect()
    {
        isEffect = 1;
    }

    public void DisableEffect()
    {
        isEffect = 0;
    }
}