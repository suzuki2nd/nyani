using UnityEngine;
using UnityEngine.UI;

public class PressButton : MonoBehaviour
{
    [SerializeField] private Texture mainTex;
    [SerializeField] private Texture subTex;
    [SerializeField] private Shader shader;
    private Material material;

    void Start()
    {
        material = new Material(shader);
        this.GetComponent<Image>().material = material;

        material.SetTexture("_MainTex", mainTex);
        material.SetTexture("_SubTex", subTex);
        material.SetFloat("_OutLineSpread", 0.0025f);
        material.SetColor("_ColorX", new Color(0.9607844f, 0.7137255f, 0.654902f, 1.0f));

        material.SetInt("_v", 1);
    }
}