using UnityEngine;

public class ExitButton : ImageButtonBase
{
    private float defaultSizeDelta;
    [SerializeField] GameObject scene;
    [SerializeField] bool exit;

    private new void Start()
    {
        base.Start();

        defaultSizeDelta = 150.0f;
        //selectSizeDelta = 140.0f;
    }


    /// <summary>
    /// ゲームパッドのボタンが押された際に呼び出される処理
    /// </summary>
    public override void OnGamePadButtonDown()
    {
        base.OnGamePadButtonDown();
        
        if (exit == true)
        {
            Application.Quit();
            cursor.GetComponent<CursorBase>().DisebleMove();
        }
        else
        {
            scene.GetComponent<TitleScene>().Continue();
        }
    }


    new private void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(160.0f, 160.0f);
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    new private void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = Vector2.one * defaultSizeDelta;
    }

    public void OnClick()
    {
        if (exit == true)
        {
            Application.Quit();
            cursor.GetComponent<CursorBase>().DisebleMove();
        }
        else
        {
            scene.GetComponent<TitleScene>().Continue();
        }
    }

}
