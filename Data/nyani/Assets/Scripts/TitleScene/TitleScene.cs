using UnityEngine;
using System.Collections;

public class TitleScene : SceneBase
{
    [SerializeField] private GameObject[] stages;
    private bool isDive;
    private bool isExit;
    private bool flg;

    private int current;
    private float time;
    private readonly float INTERVAL = 5.0f;

    [SerializeField] GameObject uiCamera;
    [SerializeField] GameObject cat;
    [SerializeField] GameObject ball;
    [SerializeField] GameObject target;
    [SerializeField] GameObject[] exit;
    [SerializeField] GameObject[] mouse;
    [SerializeField] GameObject panel;

    GameObject camera;
    GameObject model;
    GameObject cursor;
    Animator animator;

    float val;
    private new void Start()
    {
        base.Start();

        isDive = false;
        isExit = false;
        flg = false;

        //カーソルの生成
        //操作を行うのは 1P のみなので 0
        cursor = base.CursorGeneration<TitleSceneCursor>(0, Vector2.one * 50.0f, GameObject.FindGameObjectWithTag("Cursor"));
        FadeManagerTest.Instance.InScene(1.0f);

        SoundManager.Instance.SetBgm("TitleBgm", "TitleBgm");

        camera = GameObject.FindGameObjectWithTag("MainCamera").gameObject;

        //カメラが消えるバグ修正
        uiCamera.SetActive(false);
        uiCamera.SetActive(true);

        time = 0.0f;
        current = 0;
        ChangeRoom();

        model = ball.transform.Find("Character1_Reference").gameObject;
        model.GetComponent<Renderer>().material.SetFloat("val", 1.0f);
        animator = cat.GetComponent<Animator>();

        LoadTexture();
        val = 0;
    }

    new void Update()
    {
        base.Update();
        SoundManager.Instance.PlayBGM("TitleBgm");

        time += Time.deltaTime;

        if (flg == true)
        {
            val = Mathf.Clamp(Mathf.Lerp(val - Time.deltaTime, 0.0f, 0.1f), 0.0f, 1.0f);
            camera.GetComponent<CameraFilter>().SetColor(Color.white * val);
        }
        else if (INTERVAL -0.5f  < time)
        {
            val = Mathf.Clamp(Mathf.Lerp(val + Time.deltaTime, 1.0f, 0.1f), 0.0f, 1.0f);
            camera.GetComponent<CameraFilter>().SetColor(Color.white * val);
        }

        if (val <= 0)
        {
            flg = false;
        }     

        if (INTERVAL < time)
        {
            ChangeRoom();

            flg = true;
            time = 0.0f;
        }

        model.transform.rotation = Quaternion.AngleAxis(-5.0f, ball.transform.forward) * model.transform.rotation;

        float ballSpeed = Random.Range(-0.8f, -0.3f);
        ball.transform.RotateAround(target.transform.position, Vector3.up, ballSpeed);

        if (Vector3.Distance(ball.transform.position, cat.transform.position) < 1.5f)
        {
            animator.SetBool("dive", true);
            isDive = true;

            StartCoroutine(Chase());
        }

        if (isDive == false)
        {
            animator.SetBool("dive", false);
            animator.SetFloat("speed", 1.0f);
            cat.transform.RotateAround(target.transform.position, Vector3.up, Random.Range(-0.8f, ballSpeed));
        }

        if (MyInput.GetStartButtonDown(0) && isExit == false)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            foreach (GameObject index in exit)
            {
                index.SetActive(true);
            }

            panel.SetActive(true);

            cursor.GetComponent<CursorBase>().SetCursorSpeed(10.0f);
            cursor.GetComponent<TitleSceneCursor>().Unselectable();

            isExit = true;
        }
        else if (MyInput.GetStartButtonDown(0) && isExit == true)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            Continue();
        }

        if (Input.GetKeyDown(KeyCode.Escape) && isExit == false)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            foreach (GameObject index in mouse)
            {
                index.SetActive(true);
            }

            panel.SetActive(true);

            isExit = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isExit == true)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            Continue();
        }

    }

    void ChangeRoom()
    {
        stages[current].SetActive(false);
        current = current == 2 ? 0 : current + 1;
        stages[current].SetActive(true);
    }

    public void Continue()
    {
        foreach (GameObject index in exit)
        {
            index.SetActive(false);
        }

        foreach (GameObject index in mouse)
        {
            index.SetActive(false);
        }

        panel.SetActive(false);

        cursor.GetComponent<CursorBase>().SetCursorSpeed(0.0f);
        cursor.GetComponent<TitleSceneCursor>().Selectable();

        isExit = false;
    }

    private async void LoadTexture()
    {
        string EXTENSION = ".png";

        AddressablesManager am = new AddressablesManager();

        for (int index = 0; index < (int)CharacterType.MAX - 1; ++index)
        {
            CharacterHelper helper = CharacterHelper.Instance;
            var charaTex = am.LoadTextureAsset(helper.GetName(index) + EXTENSION);
            await charaTex;

            helper.SetMainTex(index, charaTex.Result);

            am.ReleaseTextureAssets(charaTex.Result);
        }
    }

    private IEnumerator Chase()
    {
        yield return new WaitForSeconds(0.8f);

        isDive = false;
    }
}
