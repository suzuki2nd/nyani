using UnityEngine;
using System.Collections;

public class ParticleDestroyser : MonoBehaviour
{
    private float destroyTime;

    void Update()
    {
        StartCoroutine(Destroy());
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(destroyTime);

        Destroy(this.gameObject);
    }

    public void SetDestroyTime(float time)
    {
        destroyTime = time;
    }
}
