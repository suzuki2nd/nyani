using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultButton : ImageButtonBase
{
    [SerializeField] string nextSceneName;
    private float defaultSizeDelta;

    private new void Start()
    {
        base.Start();

        defaultSizeDelta = 150.0f;
    }

    /// <summary>
    /// ゲームパッドのボタンが押された際に呼び出される処理
    /// </summary>
    public override void OnGamePadButtonDown()
    {
        base.OnGamePadButtonDown();

        FadeManagerTest.Instance.LoadScene(nextSceneName , 1.0f);
        
        cursor.GetComponent<CursorBase>().DisebleMove();
    }

    new private void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(170.0f, 170.0f);
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    new private void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);

        RectTransform rect = this.GetComponent<RectTransform>();
        rect.sizeDelta = Vector2.one * defaultSizeDelta;
    }
}
