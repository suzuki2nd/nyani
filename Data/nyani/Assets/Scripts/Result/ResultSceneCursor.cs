using UnityEngine;
using UnityEngine.UI;

public class ResultSceneCursor : CursorBase
{
    private bool isSelect;
    private GameObject target;
    private GameObject sceneAdmin;

    new void Start()
    {
        base.Start();

        this.gameObject.GetComponent<Image>().enabled = false;

        isSelect = false;
        target = null;

        RectTransform rect = this.GetComponent<RectTransform>();
        float offset = 2.0f;

        //画面サイズによって位置が変わってしまうので、対処必要
        rightSideLimit = new Vector2(Screen.currentResolution.width / 4 - rect.sizeDelta.x * offset, -(Screen.currentResolution.height / 4 - rect.sizeDelta.y * 1.5f));

        //画面サイズによって位置が変わってしまうので、対処必要
        leftSideLimit = new Vector2(-(Screen.currentResolution.width / 4 - rect.sizeDelta.x * offset), Screen.currentResolution.height / 4 - rect.sizeDelta.y * 1.5f);

        sceneAdmin = GameObject.FindGameObjectWithTag("SceneAdmin");
    }

    new void Update()
    {
        if (this.gameObject.GetComponent<Image>().enabled == true)
        {
            base.CursorMove();
        }

        GamePadButtonDown();
    }

    /// <summary>
    /// ゲームパッドのボタンが押された際の処理
    /// </summary>
    protected override void GamePadButtonDown()
    {
        if(MyInput.GetAnyKey(gamePadNum))
        {
            base.GamePadButtonDown();
            sceneAdmin.GetComponent<ResultScene>().ActivateButton(this.gameObject.GetComponent<Image>());
        }

        //ゲームパッドBボタンを押したとき
        if (MyInput.GetAButtonDown(gamePadNum))
        {

            //カーソルがアイコンの上にある場合
            if (isSelect == true && target != null)
            {
                target.GetComponent<ImageButtonBase>().OnGamePadButtonDown();
            }
        //    else
        //    {
        //        sceneAdmin.GetComponent<ResultScene>().ActivateButton(this.gameObject.GetComponent<Image>());
        //    }
        }
    }

    /// <summary>
    /// 当たり続けている場合に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        isSelect = true;
        target = collision.gameObject;
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        isSelect = false;
    }
}
