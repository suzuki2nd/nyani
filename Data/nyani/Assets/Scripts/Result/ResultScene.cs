using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScene : SceneBase
{
    private GameObject SceneTransitonPanel;
    private GameObject AButtonImage;
    private AddressablesManager AddressablesManager;
    [SerializeField]
    private List<Vector3> catPosWin;
    [SerializeField]
    private List<Vector3> woolPosWin;
    [SerializeField]
    private List<Vector3> catPosLose;
    [SerializeField]
    private List<Vector3> woolPosLose;

    private Vector3 catPos;
    private Vector3 woolPos;
    private float time;
    private float alphaSin;
    private int random;
    private bool isFirst;
    private bool buttonActive;
    private bool se = false;
    //ステージの識別
    StageType selectStageType = StageCollector.Instance.GetStageType();
    StageHelper stageHelper = StageHelper.Instance;

    //猫生成用
    List<GameObject> cats = new List<GameObject>();
    //勝利アニメーションの割り当て用配列
    private string[] win =
    {
        "Win1_","Win2_","Win3_"
    };

    private string[] motionPos =
{
        "Left","Center","Right"
    };

    /// <summary>
    /// 初期化
    /// </summary>
    async void Start()
    {
        base.Start();

        base.CursorGeneration<ResultSceneCursor>(0, Vector2.one * 50.0f, GameObject.FindGameObjectWithTag("Cursor"));
        //Aボタン表示判別用
        buttonActive = false;
        isFirst = true;

        se = false;

        //Fade.FadeIn();
        FadeManagerTest.Instance.InScene(1.0f);

        //シーン遷移用
        SceneTransitonPanel = GameObject.FindWithTag("TransitionPanel");
        SceneTransitonPanel.SetActive(false);
        //Aボタンイメージ用
        AButtonImage = GameObject.FindWithTag("ResultAButton");
        AButtonImage.SetActive(false);

        ////////////////////////キャラクター設定用////////////////////////////////////     

        AddressablesManager = new AddressablesManager();
        string catAddress = "Cat.prefab";
        string woolAddress = "Result/BallOfWool.prefab";
        CharacterHelper helper = CharacterHelper.Instance;
        CharacterCollector collector = CharacterCollector.Instance;

        float diffPos;
        CharacterType[] types = new CharacterType[CharacterHelper.PLAYER_MAX - 1];
        int typeIndex = 0;
        for (int i = 0; i < CharacterHelper.PLAYER_MAX; ++i)
        {
            CharacterType type = CharacterCollector.Instance.GetCharacter(i);

            if (type != CharacterType.BALL_OF_WOOL)
            {
                types[typeIndex] = type;
                typeIndex++;
            }
        }
        Shuffle(types);

        //////////////////////////////////////////////////////////////////////////////////
        //勝利判定
        bool isFoundOwner = Referee.isFoundOwner;
        bool isCaughtCat = Referee.isCaughtCat;
        //毛玉がオーナーに見つかったか、猫に捕まったか(猫の勝利)

        //debug
        //isFoundOwner = true;

        if (isFoundOwner || isCaughtCat)
        {
            diffPos = 1.0f;
            //表示位置
            switch (stageHelper.GetName((int)selectStageType))
            {
                case "FirstStage":
                    catPos = catPosWin[0];
                    woolPos = woolPosLose[0];
                    break;
                case "SecondStage":
                    catPos = catPosWin[1];
                    woolPos = woolPosLose[1];
                    break;
                case "ThirdStage":
                    catPos = catPosWin[2];
                    woolPos = woolPosLose[2];
                    break;
            }
            for (int i = 0; i < 3; ++i)
            {
                cats.Add(await AddressablesManager.InstantiateAsset(catAddress, catPos, Quaternion.identity));
            }
            int cnt = 0;
            random = Random.Range(0, 3);

            SoundManager sound = SoundManager.Instance;
            sound.SetBgm(win[random] + 1, win[random] + 1);
            sound.SetSE(win[random] + 2, win[random] + 2);

            //猫にテクスチャを貼り付け
            foreach (GameObject cat in cats)
            {
                Material material = cat.transform.transform.Find("polySurface30").GetComponent<Renderer>().material;
                material.SetTexture("_MainTex", helper.GetMainTex((int)types[cnt]));
                cnt++;

                int catIndex = cats.IndexOf(cat);
                cat.transform.AddPosX(diffPos * catIndex);

                //猫をテレビに合わせてポジションを変更
                if (stageHelper.GetName((int)selectStageType) == "SecondStage")
                {
                    diffPos = 0.7f;
                    cat.transform.AddPosZ(-diffPos * catIndex);
                }

                cat.transform.LookAt(GameObject.FindWithTag("MainCamera").transform);

                cat.GetComponent<Animator>().SetBool(win[random] + motionPos[catIndex], true);
            }
            GameObject wool = (await AddressablesManager.InstantiateAsset(woolAddress, woolPos, Quaternion.identity));
        }
        //毛玉の勝利
        else
        {
            diffPos = 1.0f;
            //表示位置
            switch (stageHelper.GetName((int)selectStageType))
            {
                case "FirstStage":
                    catPos = catPosLose[0];
                    woolPos = woolPosWin[0];
                    break;
                case "SecondStage":
                    catPos = catPosLose[1];
                    woolPos = woolPosWin[1];
                    break;
                case "ThirdStage":
                    catPos = catPosLose[2];
                    woolPos = woolPosWin[2];
                    break;
            }

            for (int i = 0; i < 3; ++i)
            {
                cats.Add(await AddressablesManager.InstantiateAsset(catAddress, catPos, Quaternion.identity));
            }
            int cnt = 0;

            foreach (GameObject cat in cats)
            {
                Material material = cat.transform.transform.Find("polySurface30").GetComponent<Renderer>().material;
                material.SetTexture("_MainTex", helper.GetMainTex((int)types[cnt]));
                cnt++;

                int catIndex = cats.IndexOf(cat);
                cat.transform.AddPosX(diffPos * catIndex);
                cat.transform.LookAt(GameObject.FindWithTag("ResultSubCamera").transform);
                cat.GetComponent<Animator>().SetBool("Lose_" + motionPos[catIndex], true);
            }

            GameObject wool = await AddressablesManager.InstantiateAsset(woolAddress, woolPos, Quaternion.identity);
        }

    }
    /// <summary>
    /// 更新
    /// </summary>
    void Update()
    {
        if (se == false)
        {
            if (random == 0)
                StartCoroutine(WinSeFirst());

            if (random == 1)
                StartCoroutine(WinSeSecond());

            if (random == 2)
                StartCoroutine(WinSeThird());
        }


        time += Time.deltaTime;
        if (time > 3.0f && isFirst)
        {
            buttonActive = true;
            AButtonImage.SetActive(true);
            //AButtonの点滅
            alphaSin = Mathf.Sin(Time.time * Mathf.PI) + 1.0f;
          //  AButtonImage.GetComponent<Image>().SetAlpha(alphaSin);
        }
        //if (MyInput.GetAButtonDown(0) && buttonActive)
        //{
        //    buttonActive = false;
        //    isFirst = false;
        //    AButtonImage.SetActive(false);
        //    SceneTransitonPanel.SetActive(true);
        //}
    }
    /// <summary>
    /// キャラクタータイプの位置を変更
    /// </summary>
    /// <param name="num">キャラクタータイプの数</param>
    void Shuffle(CharacterType[] num)
    {
        for (int i = 0; i < num.Length; i++)
        {
            CharacterType temp = num[i];
            int randomIndex = Random.Range(0, num.Length);
            num[i] = num[randomIndex];
            num[randomIndex] = temp;
        }
    }

    private IEnumerator WinSeFirst()
    {
        SoundManager sound = SoundManager.Instance;

        sound.PlayBGM(win[random] + 1);

        yield return new WaitForSeconds(1.5f);

        sound.StopBGM(win[random] + 1);

        if (se == false)
            sound.OnlyPlaySE(win[random] + 2);

        se = true;
    }
    private IEnumerator WinSeSecond()
    {
        SoundManager sound = SoundManager.Instance;

        sound.PlayBGM(win[random] + 1);

        yield return new WaitForSeconds(1.3f);

        sound.StopBGM(win[random] + 1);

        if (se == false)
            sound.OnlyPlaySE(win[random] + 2);

        se = true;
    }

    private IEnumerator WinSeThird()
    {
        SoundManager sound = SoundManager.Instance;

        yield return new WaitForSeconds(1.5f);

        sound.PlayBGM(win[random] + 1);

        sound.OnlyPlaySE(win[random] + 2);

        yield return new WaitForSeconds(0.8f);
        sound.StopBGM(win[random] + 1);
    }

    public void ActivateButton(Image image)
    {
        if (buttonActive)
        {
            image.enabled = true;

            buttonActive = false;
            isFirst = false;
            AButtonImage.SetActive(false);
            SceneTransitonPanel.SetActive(true);
        }
    }
}
