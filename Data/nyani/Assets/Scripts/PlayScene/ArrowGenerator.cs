using UnityEngine;

/// <summary>
/// 毛玉の方向を向くUIの生成、表示・非表示を管理するクラス
/// </summary>
public class ArrowGenerator : MonoBehaviour
{
    [SerializeField] private float GenerateRange = 10.0f;
    private GameObject BallOfWool;
    private GameObject Arrow;
    private AddressablesManager Manager;

    /// <summary>
    /// 初期化
    /// </summary>
    private async void Start()
    {
        BallOfWool = GameObject.FindWithTag("BallOfWool");
        Arrow = null;
        Manager = new AddressablesManager();
        Arrow = await Manager.InstantiateAsset("Arrow.prefab", this.transform);

        Arrow.SetActive(false);
    }

    /// <summary>
    /// 更新
    /// </summary>
    private void Update()
    {
        if (Arrow == null) return;

        if (BallOfWool == null)
        {
            Arrow.transform.localScale = Vector3.zero;
        }

        if (PlayHelper.Instance.IsStart() == true)
        {
            Vector3 toBallOfWool = BallOfWool.transform.position - this.transform.position;
            float distance = toBallOfWool.sqrMagnitude;

            if (distance <= Mathf.Pow(GenerateRange, 2))
            {
                Arrow.SetActive(true);
                float val = Mathf.Clamp(1.0f * (0.005f * distance), 0.1f, 0.2f);
                Arrow.transform.localScale = Vector3.one * val;
            }
            else
            {
                Arrow.gameObject.SetActive(false);
            }
        }
    }
}
