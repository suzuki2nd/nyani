using UnityEngine;

public class GimmickOrange : MonoBehaviour , IGimmick
{
    float time;
    readonly float INTERVAL = 10.0f;
    [SerializeField] private GameObject orenge;
 
    void Start()
    {
        time = 0.0f;
        ActionExecution(this.gameObject);
    }

    void Update()
    {
        time += Time.deltaTime;

        if(INTERVAL < time)
        {
            ActionExecution(this.gameObject);
            time = 0.0f;
        }
    }

    public void ActionExecution(GameObject executor)
    {
        //  int padNum = executor.GetComponent<CharacterBase>().GetGamePadNumber();
        // Vector3 force = this.transform.position - executor.transform.position;

        //  if (MyInput.GetYButtonDown(padNum))
        if (executor == this.gameObject)
        {
            GameObject ora = Instantiate(orenge);
            ora.transform.parent = this.transform;
            ora.transform.localPosition = Vector3.zero;
            ora.transform.localScale = Vector3.one;

            //Debug.Log(ora.transform.childCount);        

            for (int index = 0; index < ora.transform.childCount; ++index)
            {
                GameObject child = ora.transform.GetChild(index).gameObject;
                Rigidbody rb = child.AddComponent<Rigidbody>();
                rb.mass = 100.0f;
                Vector3 force = new Vector3(Random.Range(-1f, 1f), Random.Range(0.5f, 1f), Random.Range(-1f, 1f)).normalized;

                rb.AddForce(force * 1000.0f);
            }
        }
    }

    public void ActionExecutionEnd(GameObject executor)
    {
    }

    public void ActionExecutionStart(GameObject executor)
    {
    }
}
