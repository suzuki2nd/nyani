using UnityEngine;

public class GimmickAccelerate : MonoBehaviour , IGimmick
{
    private void Start()
    {
        SoundManager manager = SoundManager.Instance;
        manager.SetSE("Accelerate", "Accelerate");
    }

    public void ActionExecution(GameObject executor)
    {
        return;
    }

    public void ActionExecutionStart(GameObject executor)
    {
        if (executor.GetComponent<Cat>().enabled == false)
        {
            return;
        }
        Cat cat = executor.GetComponent<Cat>();

        if (cat.IsDive() == true)
        {
            cat.SetDivePower(10.0f);
            cat.GetCatStateBase().AnimationStop();

            SoundManager manager = SoundManager.Instance;
            manager.PlaySE("Accelerate");
        }
    }

    public void ActionExecutionEnd(GameObject executor)
    {
        if (executor.GetComponent<Cat>().enabled == false)
        {
            return;
        }
        Cat cat = executor.GetComponent<Cat>();
        cat.SetDivePower(3.0f);
        cat.GetCatStateBase().AnimationStart();
    }
}
