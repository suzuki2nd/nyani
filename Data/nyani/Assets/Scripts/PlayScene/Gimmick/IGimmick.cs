using UnityEngine;

public interface IGimmick
{
    /// <summary>
    /// アクションの実行
    /// </summary>
    /// <param name="executor"></param>
    void ActionExecution(GameObject executor);

    /// <summary>
    /// アクション実行開始 1 フレーム目の処理
    /// </summary>
    /// <param name="executor"></param>
    void ActionExecutionStart(GameObject executor);

    /// <summary>
    /// アクション終了時最後のフレームでの処理
    /// </summary>
    /// <param name="executor"></param>
    void ActionExecutionEnd(GameObject executor);
}
