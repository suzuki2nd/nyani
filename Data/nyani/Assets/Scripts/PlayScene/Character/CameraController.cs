using UnityEngine;
using System.Collections;
using System;

public class CameraController : MonoBehaviour
{
    [SerializeField] protected GameObject followObject = null;     // 視点となるオブジェクト
    [SerializeField] protected GameObject reset = null;     // 視点となるオブジェクト
    [SerializeField] private LayerMask obstacleLayer;               // 障害物とするレイヤー

    protected bool isForciblyMove;
    private bool isForciblyLook;
    private bool isReset;
    private bool isMove;
    private float vertical;
    private float forciblyLookTime;
    private int gamePadNum;
    protected float adjustY;


    private float sensitivity;
    private Vector3 forciblyMovePos = Vector3.zero;
    private Vector3 forciblyLookPos = Vector3.zero;
    protected Vector3 pos;

    protected CharacterBase character = null;

    virtual protected void Start()
    {
        isForciblyMove = false;
        isForciblyLook = false;
        isReset = false;
        isMove = true;

        sensitivity = 3.0f;
        adjustY = 3.0f;
        forciblyLookTime = 0.0f;

        forciblyMovePos = Vector3.zero;
        forciblyLookPos = Vector3.zero;

        character = followObject.GetComponent<CharacterBase>();
        gamePadNum = character.GetGamePadNumber();
    }

    protected void NormalMove()
    {
        //コントローラーの識別
        float horizontal =MyInput.GetRXAxis(gamePadNum);

        if (gamePadNum == 0)
        {
           // horizontal += Input.GetAxis("Mouse X");
        }
        
        Vector3 lookAt = followObject.transform.position;
        transform.RotateAround(lookAt, Vector3.up, horizontal * sensitivity * Convert.ToInt32(isMove));
        
        pos = this.transform.position;

        pos.y = followObject.transform.position.y + adjustY;
        transform.position = pos;
    }

    protected void RangeAdjustment()
    {
        float dis = (followObject.transform.position - this.transform.position).magnitude;

        if (dis > 5.0f || dis < 4.5f)
        {
            //Lerp時に移動する速度
            float cameraSpeed = 0.01f;
            float off = dis < 4.5f ? 6.0f : 0.0f;
            Vector3 nextPos = followObject.transform.position + -followObject.transform.forward * off;

            transform.position = Vector3.Lerp(this.transform.position, nextPos, cameraSpeed);
        }
    }

    protected void CameraResetMove(Vector3 characterMove)
    {
        if (characterMove.magnitude > 0)
        {
            isReset = false;
        }

        if (isReset == true)
        {
            transform.position = Vector3.Slerp(transform.position, forciblyMovePos, 0.1f);
        }
    }

    protected void FollowToWall()
    {
        RaycastHit hit;

        //テレビの前にある当たり判定のレイヤーがStageの影響でbug

        if (Physics.Linecast(followObject.transform.position, transform.position, out hit, obstacleLayer))
        {
            transform.position = hit.point;
        }
    }

    protected void FocusOnTarget(Vector3 characterMove)
    {
        //強制的に注視させられていないとき
        if (isForciblyLook == false)
        {
            vertical = Mathf.Clamp(vertical + MyInput.GetRYAxis(gamePadNum) * 0.1f * Convert.ToInt32(isMove), -1.0f,1.0f);

            if (isMove == false) vertical = 0;

            Vector3 relativePos = followObject.transform.position - this.transform.position;
            relativePos.y += 1.0f + (-vertical);
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Slerp(this.transform.rotation, rotation, 0.1f);

            if (characterMove.magnitude > 0)
            {
                character.SetForward(this.transform.forward, Quaternion.identity);
            }
        }
        else
        {
            Vector3 pos = forciblyLookPos - this.transform.position;
            Quaternion rotation = Quaternion.LookRotation(pos);

            StartCoroutine(ForciblyLook(rotation));
        }
    }


    /// <summary>
    /// カメラのリセット処理
    /// </summary>
    public void CameraReset(Vector3 characterMove)
    {
        if (MyInput.GetAButtonDown(character.GetGamePadNumber())
        && characterMove.magnitude <= 0)
        {
            isReset = true;

            vertical = 0.0f;
            Quaternion rotationQuat = Quaternion.LookRotation(reset.transform.forward);
            rotationQuat.x = 0.0f;
            rotationQuat.z = 0.0f;
            followObject.GetComponent<CharacterBase>().SetForward(reset.transform.forward, rotationQuat);

            ForciblyMove(reset.transform.position);
        }
    }

    public void ForciblyMove(Vector3 pos)
    {
        forciblyMovePos = pos;
    }

    public void EnableForciblyLook(Vector3 pos , float time)
    {
        isForciblyLook = true;
        forciblyLookPos = pos;

        forciblyLookTime = time;
    }

    public void DisableForciblyLook()
    {
        isForciblyLook = false;
    }

    public void EnableMove()
    {
        isMove = true;
    }

    public void DisableMove()
    {
        isMove = false;
    }

    private IEnumerator ForciblyLook(Quaternion quaternion)
    {
        // 現在の回転情報と、ターゲット方向の回転情報を補完する
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, quaternion, 0.1f);

        yield return new WaitForSeconds(forciblyLookTime);
        //yield return new WaitForSeconds(1.0f);

        DisableForciblyLook();
        EnableMove();

        yield return 0;
    }
}