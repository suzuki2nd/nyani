using UnityEngine;

public class BallOfWool : CharacterBase
{
    private float footTime;

    private Vector2[] ownerCallOffset = new Vector2[CharacterHelper.PLAYER_MAX]
    {
            new Vector2(-130,-80),
            new Vector2(130,-80),
            new Vector2(-130,-60),
            new Vector2(130,-60),
    };

    new private void Start()
    {
        base.Start();

        walkSpeed = 3.0f;

        model = transform.Find("Character1_Reference").gameObject;
        model.GetComponent<Renderer>().material.SetFloat("val", 1.0f);

        ParticleManager manager = ParticleManager.Instance;
        manager.SetParticle("Stars_1.prefab", "Stars_1.prefab");
        manager.SetParticle("FootPrint.prefab", "FootPrint.prefab");
    }

    new private void Update()
    {
        if (PlayHelper.Instance.IsStart() == false) return;

        base.Update();

        Vector3 result = Quaternion.Euler(0, 90, 0) * moveDir;
        model.transform.rotation = Quaternion.AngleAxis(5.0f, result) * model.transform.rotation;

        WallkEffect();
    }

    protected override void Action()
    {
        //ベースクラスのアクションを呼ぶ
        base.Action();

        if (MyInput.GetBButtonDown(gamePadNum) && PlayHelper.Instance.IsStart() == true)
        {
            GameObject ownerEvent = GameObject.FindGameObjectWithTag("OwnerEventController");

            if (ownerEvent.GetComponent<OwnerEventController>().EventExec() == true)
            {
                GameObject particle = ParticleManager.Instance.InstantiateParticle("Stars_1.prefab", this.gameObject.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                particle.transform.parent = this.gameObject.transform;
                particle.transform.localScale = Vector3.one;
            }
        }
    }

    public override void LateStart()
    {
        base.LateStart();

        ViewPortFormatter view = ViewPortFormatter.Instance;

        GameObject call = GameObject.FindGameObjectWithTag("OwnerCall").gameObject;
        RectTransform pos = call.GetComponent<RectTransform>();
        pos.localPosition = view.GetScreenCenter(gamePadNum) + ownerCallOffset[gamePadNum];
    }

    /// <summary>
    /// 自身が見つかった際の処理
    /// </summary>
    public override void FoundSelf()
    {
        //移動できない状態にする
        DisableMove();
    }

    private void WallkEffect()
    {
        //XZ軸での移動を行っている場合
        if (GetXZMove().magnitude > 0.0f)
        {
            //レイキャスト準備
            RaycastHit hit;
            Vector3 origin = this.transform.position;
            origin.y += 0.1f;

            //レイが当たっていない場合
            if (Physics.Raycast(origin, -Vector3.up, out hit, 1.0f) == true)
            {
                footTime += Time.deltaTime;
                float step = 0.2f;

                if (footTime > step)
                {
                    ParticleManager manager = ParticleManager.Instance;
                    footTime = 0.0f;

                    manager.InstantiateParticle("FootSmoke.prefab", this.transform.position, model.transform.rotation);
                }
            }
        }
    }
}
