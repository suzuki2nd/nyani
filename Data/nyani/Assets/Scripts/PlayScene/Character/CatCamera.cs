using UnityEngine;

/// <summary>
/// ネコのカメラクラス
/// </summary>
public class CatCamera : CameraController
{
    private Cat cat = null;
    private AICat aiCat = null;

    override protected void Start()
    {
        base.Start();
        SetCatComponent();
    }

    //ステートパターンで実装

    void LateUpdate()
    {
        //対象が設定されていないとき
        if (followObject == null) return;

        //y軸以外の移動ベクトル取得
        Vector3 characterMove = character.GetXZMove();

        //カメラのリセット受付
        CameraReset(characterMove);

        //プレイシーンがスタートしている場合
        if (PlayHelper.Instance.IsStart() == true)
        {
            //移動を強制させられていない場合
            if (isForciblyMove == false)
            {
                RangeAdjustment();

                //通常の移動処理
                NormalMove();
            }

            //カメラのリセット移動
            CameraResetMove(characterMove);

            //位置をキャラクターの移動に連動させて更新させる
            //transform.position += characterMove * Time.fixedDeltaTime;
        }

        //壁に沿って移動させる
        FollowToWall();
        //注視対象のほうを見る
        FocusOnTarget(characterMove);

        //四足歩行の場合
        if(cat != null)
        {
            adjustY = cat.GetStateName() == "CatStateQuadrupedal" ?
                        Mathf.Lerp(adjustY, 1.0f, 0.1f) : Mathf.Lerp(adjustY, 3.0f, 0.1f);
        }
        else
        {
            adjustY = aiCat.StateName == "CatStateQuadrupedal" ?
                        Mathf.Lerp(adjustY, 1.0f, 0.1f) : Mathf.Lerp(adjustY, 3.0f, 0.1f);
        }

    }

    private void SetCatComponent()
    {
        if(followObject.GetComponent<Cat>().enabled)
        {
            cat = followObject.GetComponent<Cat>();
        }
        else
        {
            aiCat = followObject.GetComponent<AICat>();
        }
    }
}
