using UnityEngine;
using UnityEngine.AI;

//コントローラーで操作するキャラクターのベースクラス
//継承先の 「Start」 「Update」関数の中で「base.Start()」「base.Update()」で呼ぶ
//生成時に controllerNum を設定する
public class CharacterBase : MonoBehaviour
{
    protected int gamePadNum;
    protected float walkSpeed;

    protected Vector3 moveDir;
    protected Vector2 inputVal;
    private Vector3 starPos;

    protected GameObject myCamera;
    protected GameObject model;
    protected bool isMove;

    virtual protected void Start()
    {
        //初期化
        inputVal = Vector2.zero;
        moveDir = Vector3.zero;
        isMove = true;

        PlayHelper helper = PlayHelper.Instance;

        //this.GetComponent<AIBase>().enabled = false;
        //this.GetComponent<NavMeshAgent>().enabled = false;

        if (gamePadNum < helper.GetEntryNum())
        {
            this.GetComponent<AIBase>().enabled = false;
            this.GetComponent<NavMeshAgent>().enabled = false;
        }
        else
        {
            this.GetComponent<CharacterBase>().enabled = false;
            this.GetComponent<AIBase>().GamePadNum = (short)gamePadNum;
        }
    }

    virtual protected void Update()
    {
        //移動可能状態の場合
        //かつプレイシーンがスタートしている場合
      if (isMove == true && PlayHelper.Instance.IsStart() == true)
        {
            //入力取得
            GetInput();

            //移動
            moveDir = Vector3.Normalize(transform.forward * inputVal.y + transform.right * inputVal.x) * walkSpeed;

            //キャラクターコントローラーで移動
            transform.position += moveDir * Time.fixedDeltaTime;
        }

        //アクション実行
        //ネコ：4足歩行
        //毛玉：飼い主呼び出し
        Action();
    }

    /// <summary>
    ///  入力取得
    /// </summary>
    private void GetInput()
    {
        inputVal = Vector2.zero;

        //コントローラーの識別
        float horizontal = MyInput.GetLXAxis(gamePadNum);
        float vertical = MyInput.GetLYAxis(gamePadNum);

        //デバッグ用に1Pのみキーボードで移動
        if (gamePadNum == 0)
        {
            //horizontal = Input.GetAxis("Horizontal");
            //vertical = Input.GetAxis("Vertical");
        }

        //移動に遊びを持たせる
        if (Mathf.Abs(horizontal) + Mathf.Abs(vertical) > 0.5)
        {
            inputVal = new Vector2(horizontal, vertical);
        }
    }

    /// <summary>
    /// 各キャラクター共通で行いたいアクションの実行
    /// </summary>
    protected virtual void Action()
    {
        //実行例...
        //サウンドを鳴らす
        //クールタイムの計算
    }

    /// <summary>
    /// 制御されるゲームパッドの番号を設定留守
    /// </summary>
    /// <param name="num"> ゲームパッドの番号 </param>
    public void SetGamePadNumber(int num)
    {
        gamePadNum = num;

        //一つ上の親のこの中からカメラを取得
        myCamera = transform.parent.Find("Camera").gameObject;

        //カメラのコンポーネント取得
        Camera camera = myCamera.GetComponent<Camera>();

        //ゲームパッドの番号によってカメラの設定を変える
        camera.rect = ViewPortFormatter.Instance.GetViewPortRect(gamePadNum);
    }

    public int GetGamePadNumber()
    {
        return gamePadNum;
    }

    /// <summary>
    /// ゲームパッドの番号を設定した後に実行する初期化
    /// 各キャラクタ共通して行いたい処理
    /// </summary>
    public virtual void LateStart()
    {
        CharacterCollector collector = CharacterCollector.Instance;
        collector.SetCaracter(gamePadNum, this.gameObject);
    }

    public Vector3 GetMove()
    {
        return moveDir;
    }

    /// <summary>
    /// y座標は邪魔になることが多いのでy以外の移動ベクトル取得
    /// </summary>
    /// <returns> y以外の移動ベクトル </returns>
    public Vector3 GetXZMove()
    {
        Vector3 move = moveDir;
        move.y = 0.0f;

        return move;
    }

    public void SetsStartPos(Vector3 pos)
    {
        starPos = pos;
    }
        

    /// <summary>
    /// 自身を飼い主の方向へ近づける
    /// </summary>
    /// <param name="ownerPos">飼い主の座標</param>
    /// <param name="lerpValue">補間係数</param>
    /// <param name="acceptedInputRange">飼い主とどれくらい近づいたら、入力を受け付けるか</param>
    public void AddForceToOwner(Vector3 ownerPos, float lerpValue, float acceptedInputRange)
    {
        ownerPos.y = transform.position.y;

        transform.position = Vector3.Lerp(transform.position, ownerPos, lerpValue);
        float distance = (ownerPos - transform.position).sqrMagnitude;

        if (distance <= Mathf.Pow(acceptedInputRange, 2))
        {
            return;
        }
    }

    /// <summary>
    /// 自身が見つかった際の処理
    /// 各キャラクタ共通して行いたい処理
    /// </summary>
    public virtual void FoundSelf()
    {

    }


    /// <summary>
    /// 正面ベクトル設定する
    /// </summary>
    /// <param name="forward"> 正面ベクトル </param>
    public void SetForward(Vector3 forward , Quaternion quaternion)
    {
        forward.y = 0.0f;
        this.transform.forward = forward;

        if (quaternion != Quaternion.identity)
        {
            model.transform.rotation = quaternion;
        }
    }

    public GameObject GetMyCamera()
    {
        return myCamera;
    }

    public GameObject GetModel()
    {
        return model;
    }

    /// <summary>
    /// 移動可能状態にする
    /// 主にアニメーション終了時に使用する
    /// </summary>
    public void EnableMove()
    {
        isMove = true;
    }

    /// <summary>
    /// 移動できない状態にする
    /// 主にアニメーション開始時に使用する
    /// </summary>
    public void DisableMove()
    {
        isMove = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("OutOfRange"))
        {
            transform.position = starPos;
        }
    }
}