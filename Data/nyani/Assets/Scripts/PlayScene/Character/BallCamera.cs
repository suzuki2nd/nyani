using UnityEngine;

public class BallCamera : CameraController
{
    new void Start()
    {
        base.Start();
    }

    void LateUpdate()
    {
        //対象が設定されていないとき
        if (followObject == null) return;

        Vector3 characterMove = character.GetXZMove();
        CameraReset(characterMove);

        if (characterMove != Vector3.zero)
        {
            //動いている方向に猫をゆっくり回転させる
            Quaternion rotationQuat = Quaternion.LookRotation(characterMove);

            rotationQuat.x = 0.0f;
            rotationQuat.z = 0.0f;

            reset.transform.rotation = Quaternion.Slerp(reset.transform.rotation, rotationQuat, 0.1f);
        }

        if (PlayHelper.Instance.IsStart() == true)
        {
            if (isForciblyMove == false)
            {
                RangeAdjustment();
                NormalMove();
            }

            CameraResetMove(characterMove);

            //transform.position += characterMove * Time.fixedDeltaTime;
        }

        FollowToWall();
        FocusOnTarget(characterMove);
    }
}
