using System.Collections;
using UnityEngine;

public class FootPrint : MonoBehaviour
{
    [SerializeField] private Sprite[] footPrints;
    float step = 0.0f;

    IEnumerator Disappearing()
    {
        for (int i = 0; i < step; i++)
        {
            this.GetComponent<SpriteRenderer>().material.color = new Color(1, 1, 1, 1 - 1.0f * i / step);
            yield return null;
        }

        Destroy(transform.parent.gameObject);
    }

    public void SetFootPrintSprite(string stateName)
    {
        if(stateName == "CatStateBipedal")
        {
            step = 90.0f;
            this.GetComponent<SpriteRenderer>().sprite = footPrints[0];
        }
        else
        {
            step = 180.0f;
            this.GetComponent<SpriteRenderer>().sprite = footPrints[1];
        }

        StartCoroutine(Disappearing());
    }
}
