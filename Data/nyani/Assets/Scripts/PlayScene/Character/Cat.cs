using UnityEngine;
using System.Collections;

/// <summary>
/// ネコの動きを管理するクラス
/// </summary>
public class Cat : CharacterBase
{
    private bool isDive;
    private bool isPossible;

    private float footTime;
    private float divePower;

    private CatStateBase catStateBase;
    public string StateName { get; set; }
    private GameObject poly;

    override protected void Start()
    {
        base.Start();

        //初期化
        isDive = false;
        isPossible = true;
        footTime = 0.0f;
        divePower = 3.0f;

        //初期ステートは二足歩行
        catStateBase = new CatStateBipedal(this.gameObject);

        //ネコのモデルを取得
        model = transform.Find("Character1_Reference").gameObject;

        ParticleManager manager = ParticleManager.Instance;
        manager.SetParticle("FootPrint.prefab", "FootPrint.prefab");
        manager.SetParticle("FootSmoke.prefab", "FootSmoke.prefab");
        manager.SetParticle("DiveSmoke.prefab", "DiveSmoke.prefab");
        manager.SetParticle("Boom_10.prefab", "Boom_10.prefab");

        SoundManager sound = SoundManager.Instance;
        sound.SetSE("Walk", "Walk");
    }

    /// <summary>
    /// ゲームパッドの番号を設定した後に呼ばれる初期化処理
    /// </summary>
    public override void LateStart()
    {
        base.LateStart();

        CharacterHelper helper = CharacterHelper.Instance;
        CharacterCollector collector = CharacterCollector.Instance;

        //マテリアルを取得
        poly = transform.Find("polySurface30").gameObject;
        Material material = poly.GetComponent<Renderer>().material;

        //自分のゲームパッドの番号がどのキャラクタタイプを使用しているか取得
        CharacterType type = collector.GetCharacter(gamePadNum);

        //テクスチャの設定
        //キャラクタタイプに応じたテクスチャを取得
        material.SetTexture("_MainTex", helper.GetMainTex((int)type));
    }

    override protected void Update()
    {
        //プレイシーンがスタートしている場合
        if (!catStateBase.StateName.Equals("CatStateFound") && PlayHelper.Instance.IsStart() == true)
        {
            //キャラクタベースのupdateを呼ぶ
            base.Update();
            StateName = catStateBase.StateName;
            if (inputVal != Vector2.zero)
            {
                //動いている方向に猫をゆっくり回転させる
                Quaternion rotationQuat = Quaternion.LookRotation(moveDir);

                rotationQuat.x = 0.0f;
                rotationQuat.z = 0.0f;

                model.transform.rotation = Quaternion.Slerp(model.transform.rotation, rotationQuat, 0.1f);
            }

            //ネコのステートに応じて歩く速度を取得する
            walkSpeed = catStateBase.WalkSpeed;

            //ダイブを行う
            if (isDive == true)
            {
                //今向いている方向に進む
                Vector3 dir = model.transform.forward * divePower;
                //dir += Vector3.up * 2.0f;
                transform.position += dir * Time.fixedDeltaTime;

                //当たり判定を取得
                BoxCollider boxCollider = model.GetComponent<BoxCollider>();
                //中心とサイズを変更
                boxCollider.center = Vector3.Lerp(boxCollider.center, new Vector3(0.0f, 0.05f, 0.05f), 0.1f);
                boxCollider.size = Vector3.Lerp(boxCollider.size, new Vector3(0.1f, 0.1f, 0.2f), 0.1f);
            }
            else
            {
                //ネコのステートに応じたupdateを呼ぶ
                catStateBase.Update(moveDir);
                //歩行エフェクト生成
                WallkEffect();
            }
        }
        else
        {
            moveDir = Vector3.zero;
            //ネコのステートに応じたupdateを呼ぶ
            catStateBase.Update(moveDir);
        }

        if (Referee.isTimeUp == true)
        {
            catStateBase = new CatStateFound(this.gameObject);
        }
    }

    protected override void Action()
    {
        //ベースクラスのアクションを呼ぶ
        base.Action();

        //Bボタンが押されたとき
        if (MyInput.GetBButtonDown(gamePadNum) && catStateBase.StateName != "CatStateFound")
        {
            //ステートを四足歩行に変更
            catStateBase = new CaStateQuadrupedal(this.gameObject);
        }

        //レイキャスト準備
        RaycastHit hit;
        Vector3 origin = this.transform.position;
        origin.y += 0.1f;

        //レイが当たっていない場合
        if (Physics.Raycast(origin, Vector3.up, out hit, 0.5f) == false)
        {
            //Bボタンが離されたとき
            if (MyInput.GetBButtonUp(gamePadNum) && catStateBase.StateName != "CatStateFound")
            {
                //ステートを二足歩行に変更
                catStateBase = new CatStateBipedal(this.gameObject);
            }
        }
        else
        {
            //現在四足歩行でない場合
            if (catStateBase.StateName != "CatStateQuadrupedal" && isDive == false)
            {
                //二足歩行に移行しようとして、上部に障害物がある場合
                //ステートを四足歩行に変更
                catStateBase = new CaStateQuadrupedal(this.gameObject);
            }
        }

        //RBボタンが押されたとき
        if (MyInput.GetRBButtonDown(gamePadNum) && isDive == false && catStateBase.StateName != "CatStateQuadrupedal")
        {
            ParticleManager manager = ParticleManager.Instance;

            //ダイブエフェクト生成
            manager.InstantiateParticle("DiveSmoke.prefab", this.transform.position, model.transform.rotation);
            catStateBase.Action();
        }
    }

    /// <summary>
    /// 歩行エフェクト生成
    /// </summary>
    private void WallkEffect()
    {
        //XZ軸での移動を行っている場合
        if (GetXZMove().magnitude > 0.0f)
        {
            //レイキャスト準備
            RaycastHit hit;
            Vector3 origin = this.transform.position;
            origin.y += 0.1f;

            //レイが当たっていない場合
            if (Physics.Raycast(origin, -Vector3.up, out hit, 1.0f) == true)
            {
                footTime += Time.deltaTime;
                float step = catStateBase.StateName == "CatStateBipedal" ? 0.4f : 0.8f;

                if (footTime > step)
                {
                    ParticleManager manager = ParticleManager.Instance;
                    SoundManager sound = SoundManager.Instance;

                    GameObject foot = Instantiate(manager.GetParticle("FootPrint.prefab"), model.transform.position, model.transform.rotation);
                    foot.transform.Find("FootPrint").GetComponent<FootPrint>().SetFootPrintSprite(catStateBase.StateName);

                    footTime = 0.0f;

                    manager.InstantiateParticle("FootSmoke.prefab", this.transform.position, model.transform.rotation);

                    sound.PlaySE("Walk");
                }
            }
        }
    }

    /// <summary>
    /// ダイブ終了時に実行されるメソッド
    /// </summary>
    public void DiveEnd()
    {
        ParticleManager manager = ParticleManager.Instance;
        if(this.GetComponent<Cat>().enabled == false)
        {
            return;
        }
        //ダイブエフェクト生成
        manager.InstantiateParticle("DiveSmoke.prefab", this.transform.position, model.transform.rotation);

        //ネコステートのアクションを終了させる
        catStateBase.ActionEnd();

        //ダイブ状態ではなくする
        isDive = false;

        //移動できるようにする
        EnableMove();
    }

    /// <summary>
    /// ダイブ実行
    /// </summary>
    public void Dive()
    {
        //ダイブ状態にする
        isDive = true;

        //移動できない状態にする
        DisableMove();
    }

    /// <summary>
    /// 
    /// </summary>
    public void EnablePossible()
    {
        isPossible = true;
    }

    /// <summary>
    /// 
    /// </summary>
    public void DisablePossible()
    {
        isPossible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool IsPossible()
    {
        return isPossible;
    }

    public bool IsDive()
    {
        return isDive;
    }

    public CatStateBase GetCatStateBase()
    {
        return catStateBase;
    }

    public void SetDivePower(float power)
    {
        divePower = power;
    }

    /// <summary>
    /// 自身が見つかった時に呼ばれる
    /// </summary>
    public override void FoundSelf()
    {
        //ダイブ状態ではなくする
        isDive = false;
        //移動できない状態にする
        DisableMove();

        myCamera.transform.position = model.transform.Find("Win").gameObject.transform.position;
        //ネコのステートを見つかった状態にする
        catStateBase = new CatStateFound(this.gameObject);
    }

    /// <summary>
    /// 現在のステートの名前を取得する
    /// </summary>
    /// <returns> ネコのステートの名前 </returns>
    public string GetStateName()
    {
        return catStateBase.StateName;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Gimmick"))
        {
            other.gameObject.GetComponent<IGimmick>().ActionExecutionStart(this.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Gimmick"))
        {
            other.gameObject.GetComponent<IGimmick>().ActionExecution(this.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("Gimmick"))
        {
            other.gameObject.GetComponent<IGimmick>().ActionExecutionEnd(this.gameObject);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        //ダイブ状態かつ、
        //衝突した相手がボールの場合
        //捕獲
        if (isDive == true && collision.gameObject.tag == "BallOfWool" &&
            Referee.isCaughtCat == false && GetComponent<AICat>().enabled == false)
        {
            Destroy(collision.gameObject);
            PlayHelper.Instance.EndGame();
            StartCoroutine(CatchWool());
        }
    }

    private IEnumerator CatchWool()
    {
        Referee.isCaughtCat = true;

        myCamera.transform.position = model.transform.Find("Win").gameObject.transform.position;
        myCamera.GetComponent<CameraController>().DisableMove();

        model.transform.LookAt(myCamera.transform.position);

        yield return new WaitForSeconds(2.0f);

        GameObject particle  = ParticleManager.Instance.InstantiateParticle("Boom_10.prefab", this.transform.position , Quaternion.identity) ;
        particle.transform.parent = this.gameObject.transform;
        catStateBase = new CatStateWin(this.gameObject);

        yield return 0;
    }
}
