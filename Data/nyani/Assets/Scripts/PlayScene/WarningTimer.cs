using UnityEngine;
using UnityEngine.UI;
public class WarningTimer : MonoBehaviour
{
    [SerializeField] private float CountStartTime = 3.0f;
    private float ElapsedTime;

    /// <summary>
    /// 初期化
    /// </summary>
    private void Start()
    {
        ElapsedTime = 0.0f;
    }

    /// <summary>
    /// 更新
    /// </summary>
    private async void Update()
    {
        transform.localPosition = Vector3.zero;
        ElapsedTime += Time.deltaTime;
        float currentTime = CountStartTime - ElapsedTime;
        GetComponent<Text>().text = currentTime.ToString("F0");
        if(currentTime <= 1.0f)
        {
            AssetManager.Instance.ReleaseGameObject(this.gameObject);
            await AssetManager.Instance.InstantiateGameObject("Owner.prefab");
        }
    }
}