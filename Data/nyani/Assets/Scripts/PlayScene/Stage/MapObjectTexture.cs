using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectTexture : MonoBehaviour
{
    [SerializeField] private Texture mainTex;

    void Start()
    {
        Material material = this.GetComponent<Renderer>().material;
        material.SetTexture("_MainTex", mainTex);
    }
}
