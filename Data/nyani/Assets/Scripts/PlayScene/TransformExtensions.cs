using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Transformクラスのプロパティを変更しやすくする拡張メソッドをまとめたクラス
public static class TransformExtensions
{
    /// <summary>
    /// X座標を増加させる
    /// </summary>
    /// <param name="transform">移動させたいtransform</param>
    /// <param name="x">Xの増加量</param>
    public static void AddPosX(this Transform transform, float x)
    {
        Vector3 position = transform.position;
        position.x += x;
        transform.position = position;
    }

    /// <summary>
    /// Y座標を増加させる
    /// </summary>
    /// <param name="transform">移動させたいtransform</param>
    /// <param name="y">Yの増加量</param>
    public static void AddPosY(this Transform transform, float y)
    {
        Vector3 position = transform.position;
        position.y += y;
        transform.position = position;
    }

    /// <summary>
    /// Z座標を増加させる
    /// </summary>
    /// <param name="transform">移動させたいtransform</param>
    /// <param name="z">Zの増加量</param>
    public static void AddPosZ(this Transform transform, float z)
    {
        Vector3 position = transform.position;
        position.z += z;
        transform.position = position;
    }

    /// <summary>
    /// transformのpositionのプロパティを変更
    /// </summary>
    /// <param name="transform">変更したいtransform</param>
    /// <param name="pos">設定したい座標</param>
    public static void SetPos(this Transform transform, Vector3 pos)
    {
        Vector3 position = transform.position;
        position = pos;
        transform.position = position;
    }

    /// <summary>
    /// Transformのpositionプロパティを加算
    /// </summary>
    /// <param name="transform">加算したいtransform</param>
    /// <param name="pos">増加させたいposition</param>
    public static void AddPos(this Transform transform, Vector3 pos)
    {
        Vector3 position = transform.position;
        position += pos;
        transform.position = position;
    }

    /// <summary>
    /// 指定された軸に沿って任意の角度回転させる
    /// </summary>
    /// <param name="transform">回転させたいオブジェクトのtransform</param>
    /// <param name="axis">回転させたい軸</param>
    /// <param name="angle">回転させたい量</param>
    public static void RotationAngleAxis(this Transform transform,　Vector3 axis, float angle)
    {
        Quaternion rotation = transform.rotation;
        Quaternion rotationQuat = Quaternion.AxisAngle(axis, angle);
        rotation *= rotationQuat;
    }
}