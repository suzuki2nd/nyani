using UnityEngine;
using UnityEngine.UI;

public class Warning : MonoBehaviour
{
    private float Angle;
    private float DeltaSign;
    private GameObject CountDownCanvas;
    public bool IsEndOfBlink;

    private float FrameCount;

    /// <summary>
    /// 初期化
    /// </summary>
    private void Start()
    {
        DeltaSign = 0.0f;
        Angle = 0.0f;
        FrameCount = 0.0f;
        IsEndOfBlink = false;

        CountDownCanvas = GameObject.FindWithTag("CountDownCanvas");
    }

    /// <summary>
    /// 更新
    /// </summary>
    private async void Update()
    {
        FrameCount++;
        float deltaAngle = 0.1f;
        Angle += deltaAngle;
        Image image = GetComponent<Image>();
        DeltaSign = Mathf.Sin(Angle);
        image.SetAlpha(DeltaSign);
        const short BlinkingTime = 120;
        if (FrameCount <= BlinkingTime)
        {
            return;
        }
        AssetManager.Instance.ReleaseGameObject(this.gameObject);
        Vector3 adjsutPos = new Vector3(1500.0f, -0.5f, 0.0f);
        await AssetManager.Instance.InstantiateGameObject("CutInWord.prefab", adjsutPos, Quaternion.identity, CountDownCanvas.transform);
        IsEndOfBlink = true;
    }
}