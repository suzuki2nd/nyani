using UnityEngine;

/// <summary>
/// 矢印のUIを毛玉の方向へ向けるクラス
/// </summary>
public class Arrow : MonoBehaviour
{
    private GameObject BallOfWool;

    /// <summary>
    /// 初期化
    /// </summary>
    void Start()
    {
        BallOfWool = GameObject.FindWithTag("BallOfWool");
    }

    /// <summary>
    /// 更新
    /// </summary>
    void Update()
    {
        if (PlayHelper.Instance.IsStart() == true)
        {
            GameObject cat = transform.parent.gameObject;
            Vector3 toBallOfWool = (BallOfWool.transform.position - cat.transform.position);
            transform.position = cat.transform.position + (toBallOfWool.normalized * 2.0f);
            transform.LookAt(BallOfWool.transform);
        }
    }
}