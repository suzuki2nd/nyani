

public class Referee
{
    public static void Start()
    {
        isFoundOwner = false;
        isCatFoundToOwner = false;
        isCaughtCat = false;
        isTimeUp = false;
    }

    //下記は最終的に毛玉に書く
    //オーナーに見つかった時の判定(リザルトシーンでキャラクターのオブジェクト位置の指定に使用)
    public static bool isFoundOwner{get; set;}
    public static bool isCatFoundToOwner { get; set; }
    //猫に捕まった時の判定(リザルトシーンでキャラクターのオブジェクト位置の指定に使用)
    public static bool isCaughtCat { get; set;}

    public static bool isTimeUp { get; set; }
}
