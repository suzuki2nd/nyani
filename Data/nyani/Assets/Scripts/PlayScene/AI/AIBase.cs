using UnityEngine;
using UnityEngine.AI;

//コンポーネントの必須化
[RequireComponent(typeof(NavMeshAgent))]
public abstract class AIBase : MonoBehaviour
{
    [SerializeField] protected float WanderRange = 30.0f;
    protected bool IsMove;
    protected float MaxDistance;
    protected short AreaMask;
    protected Vector3 PrevPos;
    private float StopTime;
    public short GamePadNum;
    public NavMeshAgent NavMeshAgent { private set; get; }
    public GameObject Model { get; private set; }
    public bool IsFound { get; set; }

    /// <summary>
    /// 初期化
    /// </summary>
    protected void Start()
    {
        IsFound = false;
        NavMeshAgent = GetComponent<NavMeshAgent>();
        NavMeshAgent.enabled = true;
        NavMeshAgent.stoppingDistance = 0.0f;
        NavMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.GoodQualityObstacleAvoidance;
        MaxDistance = 20.0f;
        AreaMask = 1;
        Model = transform.Find("Character1_Reference").gameObject;
        //NavmeshAgentとRigidBodyが競合してしまうため、RigidBody側からpositionを変更できないように制限
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    /// <summary>
    /// 更新
    /// </summary>
    protected void Update()
    {
        //PrevPos = transform.position;
        if (PlayHelper.Instance.IsStart() == false)
        {
            return;
        }
        Debug.DrawLine(PrevPos, NavMeshAgent.destination);
        if (NavMeshAgent.pathPending)
        {
            return;
        }
        const float RECKON_STOP_RANGE = 0.01f;
        float deltaDistance = (PrevPos - transform.position).sqrMagnitude;
        StopTime += deltaDistance <= Mathf.Pow(RECKON_STOP_RANGE, 2) ? Time.deltaTime : 0.0f;
        const float REPATH_TIME_LIMIT = 1.0f;
        if (StopTime >= REPATH_TIME_LIMIT)
        {
            Debug.Log("reset path");
            NavMeshAgent.ResetPath();
            SetWanderDestination();
            StopTime = 0.0f;
        }
        //if ((NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance) == false)
        //{
        //    return;
        //}
    }

    /// <summary>
    /// 徘徊させる
    /// </summary>
    protected void Wander()
    {
        if (NavMeshAgent.hasPath || NavMeshAgent.velocity.sqrMagnitude != 0.0f)
        {
            return;
        }
        SetWanderDestination();
        NavMeshAgent.autoBraking = true;
    }

    /// <summary>
    /// 目的地を設定
    /// </summary>
    protected void SetWanderDestination()
    {
        Vector3 randomPos = new Vector3(Random.Range(-WanderRange, WanderRange), 0, Random.Range(-WanderRange, WanderRange));
        if (NavMesh.SamplePosition(randomPos, out NavMeshHit navMeshHit, MaxDistance, AreaMask))
        {
            NavMeshAgent.SetDestination(navMeshHit.position);
        }
    }

    /// <summary>
    /// 任意の確率で真偽値を返す
    /// </summary>
    /// <param name="percentOfTrue">真になる確率</param>
    /// <returns></returns>
    protected bool IsProbabilityTrue(float percentOfTrue)
    {
        float random = Random.value * 100.0f;
        return random <= percentOfTrue ? true : false;
    }

}