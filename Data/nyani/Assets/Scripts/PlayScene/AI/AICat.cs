using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// AIによって猫の行動を制御するクラス
/// </summary>
public class AICat : AIBase
{
	[SerializeField] private float StateChangeDistance = 15.0f;
	[SerializeField] private float FieldOfViewRange = 80.0f;
	[SerializeField] private float MinDiveIntervalTime = 1.0f;
	[SerializeField] private float MaxDiveIntervalTime = 3.0f;
	[SerializeField] private float DiveDistance = 3.0f;
	[SerializeField] private float QuadrupedalSpeed = 0.5f;
	[SerializeField] private float BipedalSpeed = 1.0f;
	[SerializeField] private float ToQuadrupedalTime = 10.1f;
	[SerializeField] private float AvoidanceRadius = 0.3f;
	[SerializeField] private float ProbabilityQuadrupedal = 50.0f;

	private enum State
	{
		WANDER,
		CHASE
	}

	private State CurrentState;
	private CatStateBase CatStateBase;
	public string StateName { get; set; }
	private GameObject BallOfWool;
	private Vector3 ToBallOfWool;

	public bool IsDive { get; set; }

	private bool IsCountElapsedTimeFromDive;
	private float ElapsedTimeFromDive;
	private bool IsCountElapsedTimeFromWarning;
	private float ElapsedTimeFromWarning;
	private bool IsAlreadyChangePostureForOwner;
	private float FootTime;
	public bool IsGimmick { get; set; }

	private NavMeshBaker navMeshBaker;
	private BoxCollider BoxCollider;
	private Vector3 BeforeBoxColliderSize;
	private Vector3 BeforeBoxColliderCenter;

	/// <summary>
	/// 初期化
	/// </summary>
	private new void Start()
	{
		base.Start();
		BallOfWool = GameObject.FindWithTag("BallOfWool");
		CatStateBase = new CatStateBipedal(this.gameObject);
		IsDive = false;
		IsMove = true;
		IsCountElapsedTimeFromDive = true;
		IsAlreadyChangePostureForOwner = false;
		IsCountElapsedTimeFromWarning = false;
		IsGimmick = false;
		navMeshBaker = GameObject.FindWithTag("Stage").GetComponent<NavMeshBaker>();
		InitializeNavMeshAgent();
		Bipedal();
		BoxCollider = Model.GetComponent<BoxCollider>();
		BeforeBoxColliderCenter = BoxCollider.center;
		BeforeBoxColliderSize = BoxCollider.size;
		StateName = CatStateBase.StateName;
		NavMeshAgent.radius = AvoidanceRadius;

		ParticleManager.Instance.SetParticle("FootPrint.prefab", "FootPrint.prefab");
		ParticleManager.Instance.SetParticle("FootSmoke.prefab", "FootSmoke.prefab");
		ParticleManager.Instance.SetParticle("DiveSmoke.prefab", "DiveSmoke.prefab");
		ParticleManager.Instance.SetParticle("Boom_10.prefab", "Boom_10.prefab");
		SoundManager.Instance.SetSE("Walk", "Walk");
	}

	/// <summary>
	/// 更新
	/// </summary>
	private new void Update()
	{
		StateName = CatStateBase.StateName;
		if (PlayHelper.Instance.IsStart() == false || NavMeshAgent.enabled == false)
		{
			return;
		}
		if (IsMove == false)
		{
			NavMeshAgent.ResetPath();
			return;
		}
		GetComponent<Animator>().SetBool("dive", IsDive);
		if (IsGimmick)
		{
			return;
		}
		if (NavMeshAgent.hasPath)
		{
			FootTime += Time.deltaTime;
			float step = CatStateBase.StateName == "CatStateBipedal" ? 0.4f : 2.0f;

			if (FootTime > step)
			{
				GameObject foot = Instantiate(ParticleManager.Instance.GetParticle("FootPrint.prefab"), Model.transform.position, Model.transform.rotation);
				foot.transform.Find("FootPrint").GetComponent<FootPrint>().SetFootPrintSprite(CatStateBase.StateName);
				ParticleManager.Instance.InstantiateParticle("FootSmoke.prefab", this.transform.position, Model.transform.rotation);
				SoundManager.Instance.PlaySE("Walk");
				FootTime = 0.0f;
			}
		}
		base.Update();
		CatStateBase.Update(NavMeshAgent.velocity);

		ToBallOfWool = BallOfWool.transform.position - this.transform.position;

		if (GameObject.FindWithTag("WarningTimer"))
		{
			IsCountElapsedTimeFromWarning = true;
		}
		ElapsedTimeFromWarning += IsCountElapsedTimeFromWarning ? Time.deltaTime : 0.0f;
		float toQuadrupedalTime = Random.Range(0.0f, ToQuadrupedalTime);
		ChangePostureForOwner(toQuadrupedalTime);

		ElapsedTimeFromDive += IsCountElapsedTimeFromDive ? Time.deltaTime : 0.0f;

		const float DIVE_START_DISTANCE = 5.0f;
		float diveIntervalTime = Random.Range(MinDiveIntervalTime, MaxDiveIntervalTime);
		if (ElapsedTimeFromDive >= diveIntervalTime && ToBallOfWool.sqrMagnitude <= Mathf.Pow(DIVE_START_DISTANCE, 2))
		{
			Dive();
		}
		ChangeState();
		PrevPos = transform.position;
	}

	/// <summary>
	/// 猫の状態を変化させる
	/// </summary>
	private void ChangeState()
	{
        if (ToBallOfWool.sqrMagnitude <= Mathf.Pow(StateChangeDistance, 2))
        {
            CurrentState = State.CHASE;
            Chase();
        }
        else
        {
            CurrentState = State.WANDER;
            Wander();
        }
	}

	/// <summary>
	/// 追いかける
	/// </summary>
	private void Chase()
	{
		if (Vector3.Angle(ToBallOfWool.normalized, transform.forward) >= FieldOfViewRange)
		{
			return;
		}

		NavMeshAgent.autoBraking = false;
		NavMeshAgent.destination = BallOfWool.transform.position;
	}

	/// <summary>
	/// 4足歩行状態にする
	/// </summary>
	public void Quadrupedal()
	{
		ChangePosture("CatStateQuadrupedal");
	}

	/// <summary>
	/// 2足歩行状態にする
	/// </summary>
	public void Bipedal()
	{
		ChangePosture("CatStateBipedal");
	}

	/// <summary>
	/// 2足歩行状態から４足方向状態、または４足歩行状態から２足歩行状態へと体勢を変える
	/// </summary>
	/// <param name="nextPosture">変更したい体勢</param>
	private void ChangePosture(in string nextPosture)
	{
		if (CatStateBase.StateName == "CatStateFound" ||
			CatStateBase.StateName == nextPosture)
		{
			return;
		}
		if (nextPosture == "CatStateQuadrupedal")
		{
			NavMeshAgent.agentTypeID = StageCollector.Instance.GetStageType() == StageType.SECOND ?
										navMeshBaker.NavMeshAgentTypes["QuadrupedalCatSecondStage"] :
										navMeshBaker.NavMeshAgentTypes["QuadrupedalCat"];
			CatStateBase = new CaStateQuadrupedal(this.gameObject);
			NavMeshAgent.speed = QuadrupedalSpeed;
		}
		else if (nextPosture == "CatStateBipedal")
		{
			NavMeshAgent.agentTypeID = StageCollector.Instance.GetStageType() == StageType.SECOND ?
										navMeshBaker.NavMeshAgentTypes["BipedalCatSecondStage"] :
										navMeshBaker.NavMeshAgentTypes["BipedalCat"];
			CatStateBase = new CatStateBipedal(this.gameObject);
			NavMeshAgent.speed = BipedalSpeed;
		}
	}

	/// <summary>
	/// 飼い主が出現した時のために、体勢を変更
	/// </summary>
	/// <param name="toQuadrupedalTime">4足歩行になるまでの時間</param>
	private void ChangePostureForOwner(in float toQuadrupedalTime)
	{
		if (ElapsedTimeFromWarning <= toQuadrupedalTime && IsAlreadyChangePostureForOwner == false)
		{
			return;
		}
		if(IsProbabilityTrue(ProbabilityQuadrupedal) == false)
        {
			ElapsedTimeFromWarning = 0.0f;
			IsAlreadyChangePostureForOwner = true;
			return;
        }
		if (IsAlreadyChangePostureForOwner == false)
		{
			IsAlreadyChangePostureForOwner = true;
			ChangePosture("CatStateQuadrupedal");
		}
    }

	/// <summary>
	/// 飼い主が去った瞬間
	/// </summary>
	public void OnLeaveOwner()
    {
		IsAlreadyChangePostureForOwner = false;
		IsCountElapsedTimeFromWarning = false;
		ElapsedTimeFromWarning = 0.0f;
        if (CatStateBase.StateName != "CatStateFound")
        {
            Bipedal();
        }
    }

	/// <summary>
	/// ダイブさせる
	/// </summary>
	public void Dive()
	{
        if (GetComponent<AICat>().enabled == false ||
			CatStateBase.StateName == "CaStateQuadrupedal" ||
			CatStateBase.StateName == "CatStateFound" ||
			IsDive)
        {
            return;
        }
		ElapsedTimeFromDive = 0.0f;
		IsCountElapsedTimeFromDive = false;
		IsDive = true;
		StartCoroutine(DiveTranslate(transform.position, 0.1f));
		StartCoroutine(DiveTransformCollider());
		ParticleManager.Instance.InstantiateParticle("DiveSmoke.prefab", this.transform.position, Model.transform.rotation);
	}

	/// <summary>
	/// ダイブ時に移動させる
	/// </summary>
	/// <param name="currentPos">ダイブ時の座標</param>
	/// <param name="lerpValue">補間係数</param>
	/// <returns></returns>
	private IEnumerator DiveTranslate(Vector3 currentPos, float lerpValue)
	{
		Vector3 toPos = currentPos + transform.forward * DiveDistance;
		while ((transform.position - toPos).sqrMagnitude == 0.0f)
		{
			transform.position = Vector3.Lerp(currentPos, toPos, lerpValue);
			yield return null;
		}
		StopCoroutine(DiveTranslate(currentPos, lerpValue));
	}

	private IEnumerator DiveTransformCollider()
    {
		const float LerpValue = 0.1f;
		Vector3 transformedCenter = new Vector3(0.0f, 0.05f, 0.05f);
		Vector3 transformedSize = new Vector3(0.1f, 0.1f, 1.0f);
		while (BoxCollider.center != transformedCenter && BoxCollider.size != transformedSize)
		{
			BoxCollider.center = Vector3.Lerp(BeforeBoxColliderCenter, transformedCenter, LerpValue);
			BoxCollider.size = Vector3.Lerp(BeforeBoxColliderSize, transformedSize, LerpValue);
			yield return null;
		}
		StopCoroutine(DiveTransformCollider());
    }

	/// <summary>
	/// ダイブの終了時にAnimatorから実行される
	/// </summary>
	public void DiveEnd()
	{
		if (GetComponent<AICat>().enabled == false)
		{
			return;
		}
		IsDive = false;
		IsMove = true;
		IsCountElapsedTimeFromDive = true;
		StartCoroutine(DiveEndTransformCollider());
		ParticleManager.Instance.InstantiateParticle("DiveSmoke.prefab", this.transform.position, Model.transform.rotation);
	}

	/// <summary>
	/// ダイブ終了時にコライダーの大きさを変更する
	/// </summary>
	/// <returns></returns>
	private IEnumerator DiveEndTransformCollider()
    {
		const float LerpValue = 0.1f;
		while (BoxCollider.center != BeforeBoxColliderCenter && BoxCollider.size != BeforeBoxColliderSize)
        {
			BoxCollider.center = Vector3.Lerp(BoxCollider.center, BeforeBoxColliderCenter, LerpValue);
			BoxCollider.size = Vector3.Lerp(BoxCollider.size, BeforeBoxColliderSize, LerpValue);
			yield return null;
        }
		StopCoroutine(DiveEndTransformCollider());
    }

	/// <summary>
	/// 飼い主の方向へ引き寄せられる力を加える
	/// </summary>
	/// <param name="ownerPos">飼い主の座標</param>
	/// <param name="lerpValue">補間係数</param>
	/// <param name="acceptedInputRange">入力を受け付ける範囲</param>
	public void AddForceToOwner(in Vector3 ownerPos, in float lerpValue, in float acceptedInputRange)
	{
		if (CatStateBase.StateName == "CatStateQuadrupedal")
		{
			return;
		}
		transform.position = Vector3.Lerp(transform.position, ownerPos, lerpValue);
	}

	/// <summary>
	/// 衝突した瞬間
	/// </summary>
	/// <param name="collision">衝突した相手の情報</param>
	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag == "OutOfRange")
        {
			IsGimmick = false;
			transform.position = new Vector3(0.0f, -3.5f, 0.0f);
        }
		if (IsDive && collision.gameObject.tag == "BallOfWool")
		{
			if (CatStateBase.StateName == "CatStateQuadrupedal")
			{
				return;
			}
			Debug.Log("CatchWool");
			PlayHelper.Instance.EndGame();
			StartCoroutine(CatchWool());
		}
	}

	/// <summary>
	/// 毛玉を捕まえた瞬間
	/// </summary>
	/// <returns></returns>
	private IEnumerator CatchWool()
	{
		Referee.isCaughtCat = true;
	 	GameObject myCamera = transform.parent.Find("Camera").gameObject;
		myCamera.transform.position = Model.transform.Find("Win").gameObject.transform.position;

		Model.transform.LookAt(myCamera.transform.position);
		CatStateBase = new CatStateWin(this.gameObject);

		yield return new WaitForSeconds(1.0f);
		NavMeshAgent.enabled = false;
		yield return new WaitForSeconds(1.0f);

		GameObject particle = ParticleManager.Instance.InstantiateParticle("Boom_10.prefab", this.transform.position, Quaternion.identity);
		particle.transform.parent = this.gameObject.transform;
		//particle.transform.localScale = Vector3.one;
		yield return null;
	}

	/// <summary>
	/// 飼い主から見つかった時に実行する処理
	/// </summary>
	public void FoundSelf()
	{
		if(IsFound)
        {
			return;
        }
		IsFound = true;
		GameObject myCamera = transform.parent.Find("Camera").gameObject;
		myCamera.transform.position = Model.transform.Find("Win").gameObject.transform.position;
		NavMeshAgent.ResetPath();
		IsDive = false;
		IsMove = false;
		CatStateBase = new CatStateFound(this.gameObject);
		NavMeshAgent.enabled = false;
		Debug.Log("found by owner : " + GamePadNum);
	}

	/// <summary>
	/// アニメーションを通常の速さ(1.0f)で再生する
	/// </summary>
	public void AnimationStart()
	{
		GetComponent<Animator>().speed = 1.0f;
	}

	/// <summary>
	/// アニメーションを停止させる
	/// </summary>
	public void AnimationStop()
	{
		GetComponent<Animator>().speed = 0.0f;
	}

	/// <summary>
	/// ステージに応じたNavMeshAgentの初期化処理
	/// </summary>
	private void InitializeNavMeshAgent()
    {
		StageType currentStageType = StageCollector.Instance.GetStageType();
		if(currentStageType == StageType.FIRST || currentStageType == StageType.THIRD)
        {
			NavMeshAgent.agentTypeID = navMeshBaker.NavMeshAgentTypes["BipedalCat"];
			navMeshBaker.BakeByAgentTypeID(navMeshBaker.NavMeshAgentTypes["BipedalCat"]);
			navMeshBaker.BakeByAgentTypeID(navMeshBaker.NavMeshAgentTypes["QuadrupedalCat"]);
		}
		else
        {
			NavMeshAgent.agentTypeID = navMeshBaker.NavMeshAgentTypes["BipedalCatSecondStage"];
			navMeshBaker.BakeByAgentTypeID(navMeshBaker.NavMeshAgentTypes["BipedalCatSecondStage"]);
			navMeshBaker.BakeByAgentTypeID(navMeshBaker.NavMeshAgentTypes["QuadrupedalCatSecondStage"]);
		}
		CatStateBase = new CatStateBipedal(this.gameObject);
		NavMeshAgent.speed = BipedalSpeed;
	}
}