using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

/// <summary>
/// 毛玉を操作するAIを管理するクラス
/// </summary>
public class AIBallOfWool : AIBase
{
    [SerializeField] private float DitectionRange = 10.0f;
    [SerializeField] private float Speed = 3.0f;
    [SerializeField] private float MaxSamplingDistance = 3.0f;
    [SerializeField] private float OwnerCallDistance = 15.0f;
    [SerializeField] private float MaxOwnerCallIntervalTime = 2.0f;
    [SerializeField] private float ProbabilityStop = 50.0f;
    private CatType[] CatsType = new CatType[(int)CatType.MAX - 1] 
    {
        CatType.FIRST, CatType.SECOND, CatType.THIRD
    };
    
    //firstCatから順番に格納
    private GameObject[] Cats = new GameObject[(int)CatType.MAX - 1];
    private Vector3[] VectorsEachCatToBallOfWool = new Vector3[(short)CatType.MAX - 1];
    private List<GameObject> CatsInDitectionRange;
    private float ElapsedTimeAfterOwnerCall;
    private bool IsCalledOwner;

    private enum State
    {
        WANDER,
        ESCAPE
    }
    private State CurrentState = State.WANDER;
    private State PrevState = State.WANDER;

    /// <summary>
    /// 初期化
    /// </summary>
    private new void Start()
    {
        base.Start();
        IsMove = true;
        NavMeshAgent.radius = 0.06f;
        CharacterHelper helper = CharacterHelper.Instance;
        for (int index = 0; index < (int)CatType.MAX - 1; ++index)
        {
            Cats[index] = GameObject.FindWithTag(helper.GetName(index));
        }
        CatsInDitectionRange = new List<GameObject>();
        ElapsedTimeAfterOwnerCall = 0.0f;
        IsCalledOwner = false;
        NavMeshAgent.speed = Speed;
    }

    /// <summary>
    /// 更新
    /// </summary>
    private new void Update()
    {
        if (PlayHelper.Instance.IsStart() == false)
        {
            return;
        }
        PrevState = CurrentState;

        if (GameObject.FindWithTag("Owner") && IsProbabilityTrue(ProbabilityStop))
        {
            NavMeshAgent.ResetPath();
            IsMove = false;
            return;
        }
        else if(GameObject.FindWithTag("Owner") == false)
        {
            IsMove = true;
            Model.transform.rotation = Quaternion.AngleAxis(5.0f, Quaternion.Euler(0, 90, 0) * this.transform.forward) * Model.transform.rotation;
        }
        if(IsMove == false)
        {
            return;
        }
        base.Update();
        SetVectorsEachCatToBallOfWool();
        DitectionCat();
        ChangeState();
        switch (CurrentState)
        {
            case State.WANDER:
                Wander();
                break;
            case State.ESCAPE:
                Escape();
                break;
            default:
                Wander();
                break;
        }

        foreach (GameObject cat in Cats)
        {
            float distanceToCat = (cat.transform.position - transform.position).sqrMagnitude;
            if (distanceToCat > Mathf.Pow(OwnerCallDistance, 2))
            {
                continue;
            }
            ElapsedTimeAfterOwnerCall = 0.0f;
            OwnerCall();
            IsCalledOwner = true;
        }

        if (IsCalledOwner)
        {
            ElapsedTimeAfterOwnerCall += Time.deltaTime;
        }
        float ownerCallIntervalTime = Random.Range(2, MaxOwnerCallIntervalTime);
        if(ElapsedTimeAfterOwnerCall >= ownerCallIntervalTime)
        {
            OwnerCall();
            ElapsedTimeAfterOwnerCall = 0.0f;
            IsCalledOwner = false;
        }
    }

    /// <summary>
    /// 逃げる
    /// </summary>
    private void Escape()
    {
        if (PrevState == State.WANDER && NavMeshAgent.hasPath)
        {
            NavMeshAgent.ResetPath();
            return;
        }
        Vector3 samplePos = GetSamplePosition(MaxSamplingDistance);
        samplePos.y = transform.position.y;
        NavMeshAgent.SetDestination(samplePos);
    }

    /// <summary>
    /// 猫から逃げるために到達可能な目標地点を取得
    /// </summary>
    /// <param name="maxSamplingDistance">サンプリングする最大距離</param>
    /// <returns></returns>
    Vector3 GetSamplePosition(float maxSamplingDistance)
    {
        if(maxSamplingDistance <= 0)
        {
            //0などを返すと移動が停止したり、ぎこちなくなってしまうため、とりあえず前進させる
            Debug.Log("forward");
            return transform.forward;
        }
        Vector3 clearingDirection = GetEscapeDirectionFromSomeCats().normalized * maxSamplingDistance;
        clearingDirection.y = transform.position.y;
        if (NavMesh.SamplePosition(clearingDirection, out NavMeshHit navMeshHit, MaxDistance, AreaMask))
        {
            return navMeshHit.position;
        }
        else
        {
            return GetSamplePosition(maxSamplingDistance - 1.0f);
        }
    }

    /// <summary>
    /// 現在の状態を変更
    /// </summary>
    private void ChangeState()
    {
        if (CatsInDitectionRange.Count == 0)
        {
            CurrentState = State.WANDER;
        }
        else
        {
            CurrentState = State.ESCAPE;
        }
    }

    /// <summary>
    /// 毛玉からネコまでのベクトルを計算し配列に格納
    /// </summary>
    private void SetVectorsEachCatToBallOfWool()
    {
        for (short i = 0; i < (short)CatType.MAX - 1; ++i)
        {
            VectorsEachCatToBallOfWool[i] = transform.position - Cats[i].transform.position;
        }
    }

    /// <summary>
    /// 毛玉から一定範囲内に存在するネコをリストに格納する
    /// </summary>
    private void DitectionCat()
    {
        for (int i = 0; i < CatsType.Length; ++i)
        {
            if (IsExistCatInDitectionRange(CatsType[i]) == false)
            {
                CatsInDitectionRange.Remove(Cats[i]);
                continue;
            }
            if (CatsInDitectionRange.Contains(Cats[i]))
            {
                CatsInDitectionRange.Remove(Cats[i]);
                CatsInDitectionRange.Add(Cats[i]);
            }
            else
            {
                CatsInDitectionRange.Add(Cats[i]);
            }
        }
    }

    /// <summary>
    /// 毛玉が行動を起こすための検出範囲内に、引数で指定したネコが存在するか
    /// </summary>
    /// <param name="catType">猫を識別するためのenum</param>
    /// <returns></returns>
    private bool IsExistCatInDitectionRange(CatType catType)
    {
        if(VectorsEachCatToBallOfWool[(int)catType].sqrMagnitude <= Mathf.Pow(DitectionRange, 2))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// 複数の猫から逃げるための移動方向ベクトルを取得
    /// </summary>
    /// <returns></returns>
    private Vector3 GetEscapeDirectionFromSomeCats()
    {
        const short FOUND_A_CAT = 1;
        const short FOUND_TWO_CATS = 2;
        const short FOUND_THREE_CATS = 3;

        switch(CatsInDitectionRange.Count)
        {
            case FOUND_A_CAT:
                return transform.position - CatsInDitectionRange[0].transform.position;

            case FOUND_TWO_CATS:
                return (transform.position - CatsInDitectionRange[0].transform.position +
                        transform.position - CatsInDitectionRange[1].transform.position);

            case FOUND_THREE_CATS:
                Dictionary<Vector3, float> dots = new Dictionary<Vector3, float>();
                Vector3 escapePoint = Vector3.zero;
                for (short i = 0; i < CatsInDitectionRange.Count - 1; ++i)
                {
                    escapePoint = (-VectorsEachCatToBallOfWool[i]) + (-VectorsEachCatToBallOfWool[i + 1]) * 0.5f;

                    dots.Add(escapePoint, Vector3.Dot(-VectorsEachCatToBallOfWool[i], -VectorsEachCatToBallOfWool[i + 1]));
                }
                int max = CatsInDitectionRange.Count;
                escapePoint = (-VectorsEachCatToBallOfWool[max - 1]) + (-VectorsEachCatToBallOfWool[0]) * 0.5f;
                dots.Add(escapePoint, Vector3.Dot(-VectorsEachCatToBallOfWool[max - 1], (-VectorsEachCatToBallOfWool[0])));
                //包囲されていない
                if (dots.All(i => i.Value > 0))
                {
                    Vector3 direction = Vector3.zero;
                    foreach (GameObject cat in CatsInDitectionRange)
                    {
                        direction += (transform.position - cat.transform.position);
                    }
                    return direction;
                }
                else
                {
                    float minDot = dots.Values.Min();
                    return dots.FirstOrDefault(dot => dot.Value == minDot).Key;
                }

            default:
                //実際はここには入らないはずだが、もし入っても移動は止めないように念のため
                return transform.forward;
        }
    }

    /// <summary>
    /// 飼い主を呼ぶ
    /// </summary>
    private void OwnerCall()
    {
        foreach (GameObject cat in Cats)
        {
            float distanceToCat = (cat.transform.position - transform.position).sqrMagnitude;
            if (distanceToCat > Mathf.Pow(OwnerCallDistance, 2))
            {
                continue;
            }
            GameObject.FindWithTag("OwnerEventController").GetComponent<OwnerEventController>().EventExec();
            ElapsedTimeAfterOwnerCall = 0.0f;
            IsCalledOwner = true;
        }
    }
}