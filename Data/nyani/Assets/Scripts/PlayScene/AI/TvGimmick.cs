using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// テレビの下でネコが４足歩行になり、加速するギミックをAI側から使用するためのクラス
/// </summary>
public class TvGimmick : MonoBehaviour
{
    [SerializeField] private float LerpValue = 0.1f;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("collision");
        GameObject cat = null;
        Debug.Log("other : " + other.transform.parent.gameObject.name);
        if(other.transform.parent.gameObject.name.Contains("Character") &&
           other.transform.parent.gameObject.GetComponent<AICat>() != null)
        {
            cat = other.transform.parent.gameObject;
        }
        if(cat == null)
        {
            return;
        }
        const float MOVE_DISTANCE = 11.0f;
        Debug.Log("cat : " + cat.transform.forward.ToString());
        Vector3 toPos = cat.transform.position + (cat.transform.forward * MOVE_DISTANCE);
        if (cat.GetComponent<AICat>().IsGimmick || cat.GetComponent<AICat>().IsDive == false)
        {
            return;
        }
        StartCoroutine(GimmickAction(cat, toPos));
    }

    /// <summary>
    /// ギミックを作動させる
    /// </summary>
    /// <param name="cat"></param>
    /// <param name="toPos"></param>
    /// <param name="lerpValue"></param>
    /// <returns></returns>
    IEnumerator GimmickAction(GameObject cat, Vector3 toPos)
    {
        cat.GetComponent<NavMeshAgent>().enabled = false;
        cat.GetComponent<AICat>().IsGimmick = true;

        float distance = (cat.transform.position - toPos).sqrMagnitude;
        const float gimmickEndDistance = 0.5f;
        while (distance > Mathf.Pow(gimmickEndDistance, 2) && cat.GetComponent<AICat>().IsGimmick)
        {
            if (cat.GetComponent<AICat>().IsDive)
            {
                cat.GetComponent<AICat>().AnimationStop();
            }
            cat.transform.position = Vector3.Lerp(cat.transform.position, toPos, LerpValue);
            distance = (cat.transform.position - toPos).sqrMagnitude;
            yield return null;
        }
        cat.GetComponent<NavMeshAgent>().enabled = true;
        cat.GetComponent<AICat>().IsGimmick = false;
        cat.GetComponent<AICat>().AnimationStart();
        StopCoroutine(GimmickAction(cat, toPos));
    }
}