using UnityEngine;

/// <summary>
/// 衝突している最中のAI操作のネコを、強制的に4足歩行状態にするためのクラス
/// </summary>
public class ForceQuadrupedal : MonoBehaviour
{
    /// <summary>
    /// 衝突した瞬間
    /// </summary>
    /// <param name="other">衝突したオブジェクト</param>
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("collide");
        Debug.Log("name : " + other.transform.parent.gameObject.name);
        if (other.transform.parent.gameObject.GetComponent<AICat>() == null ||
            other.transform.parent.gameObject.GetComponent<AICat>().enabled == false)
        {
            return;
        }
        GameObject cat = other.transform.parent.gameObject;
        cat.GetComponent<AICat>().Quadrupedal();
    }

    /// <summary>
    /// 衝突しなくなった瞬間
    /// </summary>
    /// <param name="other">衝突したオブジェクト</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent.gameObject.GetComponent<AICat>() == null ||
            other.transform.parent.gameObject.GetComponent<AICat>().enabled == false)
        {
            return;
        }
        GameObject cat = other.transform.parent.gameObject;
        cat.GetComponent<AICat>().Bipedal();
    }
}