using System.Collections;
using UnityEngine;

public class ForceDive : MonoBehaviour
{
    [SerializeField] private float LerpValue = 0.1f;
    BoxCollider BoxCollider;
    private Vector3 BeforeColliderSize;

    private void OnTriggerEnter(Collider other)
    {
        foreach (Transform trans in other.gameObject.transform)
        {
            if (trans.name.Contains("Character") == false ||
                other.transform.parent.gameObject.GetComponent<AICat>() != null)
            {
                continue;
            }
            Debug.Log("name : " + trans.parent.parent.name);
            if (trans.parent.parent.gameObject.GetComponent<AICat>().enabled)
            {
                GameObject cat = trans.parent.parent.gameObject;
                BoxCollider = cat.GetComponent<AICat>().Model.GetComponent<BoxCollider>();
                BeforeColliderSize = BoxCollider.size;
                cat.GetComponent<AICat>().Dive();
                GameObject TV = GameObject.FindWithTag("TV").gameObject;
                Vector3 toTV = cat.transform.position - TV.transform.position;
                float groundHeight = -3.5f;
                toTV.y = groundHeight;
                cat.transform.LookAt(TV.transform.position);
                StartCoroutine(TransformCollider(cat));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (BoxCollider != null)
        {
            //コライダーのサイズもとに戻さないと変形途中のままになる
            BoxCollider.size = BeforeColliderSize;
        }
    }

    /// <summary>
    /// コライダーを変形させる
    /// </summary>
    /// <param name="cat">変形させたいコライダを持つ猫</param>
    /// <returns></returns>
    private IEnumerator TransformCollider(GameObject cat)
    {
        Debug.Log("pos : " + cat.transform.position.ToString());
        Vector3 transformedCenter = new Vector3(0.0f, 0.05f, 0.05f);
        Vector3 transformedSize = new Vector3(0.1f, 0.1f, 0.2f);
        BoxCollider boxCollider = cat.transform.Find("Character1_Reference").GetComponent<BoxCollider>();
        while(boxCollider.center != transformedCenter && boxCollider.size != transformedSize)
        {
            cat.GetComponent<AICat>().NavMeshAgent.updateRotation = false;
            //中心とサイズを変更
            boxCollider.center = Vector3.Lerp(boxCollider.center, transformedCenter, LerpValue);
            boxCollider.size = Vector3.Lerp(boxCollider.size, transformedSize, LerpValue);
            yield return null;
        }
        cat.GetComponent<AICat>().NavMeshAgent.updateRotation = true;
        StopCoroutine(TransformCollider(cat));
    }
}