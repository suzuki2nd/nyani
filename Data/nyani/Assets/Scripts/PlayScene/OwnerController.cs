using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// 飼い主の生成、移動、破棄などを管理するクラス
/// </summary>
public class OwnerController : MonoBehaviour
{
    [Tooltip("飼い主が猫を引き寄せる力を発生させる範囲")]
    [SerializeField] private float AttractRange = 50.0f;
    public float SurvivableTime = 5.0f;
    [Tooltip("0から1の範囲で指定")]
    [SerializeField] private float AttractPower = 0.05f;
    [SerializeField] private float AcceptedInputRange = 20.0f;
    [SerializeField] private float Speed = 1.0f;
    [SerializeField] private LayerMask obstacleLayer;               // 障害物とするレイヤー

    private GameObject[] cats = new GameObject[CharacterHelper.PLAYER_MAX - 1];
    private float[] angles = new float[CharacterHelper.PLAYER_MAX - 1];

    private GameObject BallOfWool;
    private GameObject sceneAdmin;

    private Animator animator;

    private Dictionary<GameObject, Vector3> VectorsEachCatToOwner;
    private List<GameObject> Cats;
    private float ElapsedTime;
    private Vector3 Move;


    private delegate void DelegateCat(GameObject cat);

    /// <summary>
    /// 初期化
    /// </summary>
    private void Start()
    {
        CharacterHelper helper = CharacterHelper.Instance;

        for (int index = 0; index < CharacterHelper.PLAYER_MAX - 1; ++index)
        {
            cats[index] = GameObject.FindWithTag(helper.GetName(index));
        }

        BallOfWool = GameObject.FindWithTag("BallOfWool");
        VectorsEachCatToOwner = new Dictionary<GameObject, Vector3>();
        Cats = new List<GameObject>();
        sceneAdmin = GameObject.FindGameObjectWithTag("SceneAdmin").gameObject;
        sceneAdmin.GetComponent<PlayScene>().SetExistsOwner(true);

        Cats.Add(cats[(int)CharacterType.CAT_FIRST]);
        Cats.Add(cats[(int)CharacterType.CAT_SECOND]);
        Cats.Add(cats[(int)CharacterType.CAT_THIRD]);

        ElapsedTime = 0.0f;
        Move = new Vector3(0.0f, 0.0f, 0.0f);
        SetInstantiatePosition();

        CharacterCollector collector = CharacterCollector.Instance;

        for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
        {
            GameObject camera = collector.GetCaracterObj(index).GetComponent<CharacterBase>().GetMyCamera();
            camera.GetComponent<CameraController>().EnableForciblyLook(this.gameObject.transform.position + Vector3.up * 5.0f , 1.0f);
        }

        animator = transform.Find("Motion").gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// 更新
    /// </summary>
    private void Update()
    {
        StageCollector stageCollector = StageCollector.Instance;
        SoundManager.Instance.PlayOwnerBGM("Owner" + (int)stageCollector.GetStageType());
        
        ElapsedTime += Time.deltaTime;
        if (ElapsedTime >= SurvivableTime)
        {
            AddressablesManager am = new AddressablesManager();
            am.ReleaseInstance(this.gameObject);
        }

        LookRotationToNearestCat();
        AddAttractForce();

        Vector3 rayStart = this.transform.position + Vector3.up * 10.0f;

        Vector3 ownerPos = this.transform.position + Vector3.up;
        //毛玉のアウト判定
        Vector3 dir = BallOfWool.transform.position - ownerPos;
        GameObject ballCamera = BallOfWool.GetComponent<CharacterBase>().GetMyCamera();

        Debug.DrawLine(rayStart, BallOfWool.transform.position, Color.red);

        if (Referee.isFoundOwner == false && Physics.Linecast(rayStart, BallOfWool.transform.position, out RaycastHit ballHit , obstacleLayer) == true)
        {
            Debug.Log(ballHit.collider.gameObject);

            bool isLookingBall = false;
            if (BallOfWool.gameObject.GetComponent<BallOfWool>().enabled)
            {
                isLookingBall = ballHit.collider.gameObject == BallOfWool.gameObject.GetComponent<BallOfWool>().GetModel().gameObject;
            }
            else if (BallOfWool.gameObject.GetComponent<AIBallOfWool>().enabled)
            {
                isLookingBall = ballHit.collider.gameObject == BallOfWool.gameObject.GetComponent<AIBallOfWool>().Model.gameObject;
            }


            Debug.Log(isLookingBall);

            //視界に入っている
            if (isLookingBall == true)
            {
                BallOfWool ballOfWoolScript = BallOfWool.GetComponent<BallOfWool>();
                ballCamera.GetComponent<CameraFilter>().EnableEffect();

                if (ballOfWoolScript.GetXZMove().magnitude > 0.0f)
                {
                    ballOfWoolScript.FoundSelf();
                    sceneAdmin.GetComponent<PlayScene>().FoundBallOfWool(ballOfWoolScript.GetGamePadNumber());

                    animator.SetBool("surprised", true);
                    ballCamera.GetComponent<CameraController>().EnableForciblyLook(this.gameObject.transform.position + Vector3.up * 5.0f, 10.0f);

                    Referee.isFoundOwner = true;
                }
            }
            else
            {
                ballCamera.GetComponent<CameraFilter>().DisableEffect();
            }
        }

        //ネコのアウト判定
        //ネコが視界に入っているか判定
        for (int index = 0; index < CharacterHelper.PLAYER_MAX - 1; ++index)
        {
            Vector3 catDir = cats[index].transform.position - this.transform.position;
            angles[index] = Vector3.Angle(catDir, transform.forward);

            Vector3 rayPos = cats[index].transform.position;
            Debug.DrawLine(rayStart, rayPos , Color.red);

            GameObject camera = cats[index].GetComponent<CharacterBase>().GetMyCamera();
            
            if (Physics.Linecast(this.transform.position, rayPos, out RaycastHit hit , obstacleLayer) == true)
            {

                bool isLooking = false;
                if (cats[index].gameObject.GetComponent<Cat>().enabled)
                {
                    isLooking = hit.collider.gameObject == cats[index].gameObject.GetComponent<Cat>().GetModel().gameObject;
                }
                else if (cats[index].gameObject.GetComponent<AICat>().enabled)
                {
                    isLooking = hit.collider.gameObject == cats[index].gameObject.GetComponent<AICat>().Model.gameObject;   
                }

                //視界に入っている
                if (isLooking == false)
                {
                    camera.GetComponent<CameraFilter>().DisableEffect();
                    //return;
                }

                //ネコオブジェクトから Cat を取得、保持
                Cat catScript = cats[index].GetComponent<Cat>();
                AICat aiCatScript = cats[index].GetComponent<AICat>();

                camera.GetComponent<CameraFilter>().EnableEffect();
                if (catScript.enabled && catScript.GetStateName() == "CatStateBipedal" && catScript.IsPossible() == true)
                {
                    Debug.Log(index + "ネコ発見");
                    catScript.FoundSelf();
                    //プレイシーンにネコを見つけた通知をする
                    sceneAdmin.GetComponent<PlayScene>().FoundCat(catScript.GetGamePadNumber());
                }
                else if (aiCatScript.enabled && aiCatScript.StateName == "CatStateBipedal")
                {
                    Debug.Log("AIネコ発見");
                    aiCatScript.FoundSelf();
                    //プレイシーンにネコを見つけた通知をする
                    sceneAdmin.GetComponent<PlayScene>().FoundCat(aiCatScript.GamePadNum);
                }
            }
        }

        transform.AddPos((Move * 5.0f) * Time.deltaTime);
    }

    /// <summary>
    /// 生成する座標を設定
    /// </summary>
    private void SetInstantiatePosition()
    {
        CalcVectorsEachCatToOwner();
        Dictionary<GameObject, float> distancesToWoolOfBall = new Dictionary<GameObject, float>();
        foreach (GameObject cat in Cats)
        {
            distancesToWoolOfBall.Add(cat, Vector3.SqrMagnitude(cat.transform.position - BallOfWool.transform.position));
        }
        float minDistance = distancesToWoolOfBall.Values.Min();
        GameObject minElement = distancesToWoolOfBall.FirstOrDefault(distance => distance.Value == minDistance).Key;

        //生成する位置を、毛玉から一番近い猫から少し遠ざける
        transform.position = minElement.transform.position;
        float maxRange = 1.0f;
        float minRange = -maxRange;
        float randomX = Random.Range(minRange, maxRange);
        float randomZ = Random.Range(minRange, maxRange);
        transform.AddPosX(randomX);
        transform.AddPosZ(randomZ);
    }

    /// <summary>
    /// 飼い主を毛玉の方向を向くように回転させる
    /// </summary>
    private void LookRotationToNearestCat()
    {
        Vector3 toBallOfWool = (BallOfWool.transform.position - this.transform.position).normalized;
        Move = -toBallOfWool;

        Dictionary<Cat, float> distancesEachCatToOwner = new Dictionary<Cat, float>();
        foreach (var cat in VectorsEachCatToOwner)
        {
            if (cat.Key == null)
            {
                continue;
            }
            distancesEachCatToOwner.Add(cat.Key.GetComponent<Cat>(), cat.Value.sqrMagnitude);
        }
        float minDistance = distancesEachCatToOwner.Values.Min();
        Cat minElement = distancesEachCatToOwner.FirstOrDefault(distance => distance.Value == minDistance).Key;

        Vector3 toNearestCat = minElement.transform.position - this.transform.position;
        Quaternion look = Quaternion.LookRotation(toNearestCat);
        look.x = 0.0f;
        look.z = 0.0f;
        transform.localRotation = look;
    }

    /// <summary>
    /// 一定範囲内の猫に、飼い主に引き寄せられる力を加える
    /// </summary>
    private void AddAttractForce()
    {
        CalcVectorsEachCatToOwner();
        foreach (var vec in VectorsEachCatToOwner)
        {
            if (vec.Value.sqrMagnitude >= Mathf.Pow(AttractRange, 2))
            {
                continue;
            }
            Assert.IsTrue(AttractPower <= 1.0f && AttractPower >= 0.0f);
            if (vec.Key == null)
            {
                continue;
            }

            Vector3 toBottom = new Vector3(0.0f, -2.5f, 0.0f);
            Vector3 toPos = transform.position + toBottom;
            
            AttractPower = 0.0015f;

            if(vec.Key.GetComponent<AICat>().enabled &&
               vec.Key.GetComponent<AICat>().StateName == "CatStateFound" || 
               vec.Key.GetComponent<Cat>().GetStateName() == "CatStateFound")
            {
                continue;
            }
            vec.Key.GetComponent<Cat>().AddForceToOwner(toPos, AttractPower, AcceptedInputRange);
        }
    }

    /// <summary>
    /// 各猫から飼い主へのベクトルを計算してDictionaryに登録
    /// </summary>
    void CalcVectorsEachCatToOwner()
    {
        DelegateCat InsertOrAssign = (GameObject cat) =>
        {
            if (cat == null)
            {
                return;
            }
            if (VectorsEachCatToOwner.ContainsKey(cat) == false)
            {
                VectorsEachCatToOwner.Add(cat, this.transform.position - cat.transform.position);
            }
            else if (VectorsEachCatToOwner.ContainsKey(cat))
            {
                VectorsEachCatToOwner[cat] = this.transform.position - cat.transform.position;
            }
        };

        for (int index = 0; index < CharacterHelper.PLAYER_MAX - 1; ++index)
        {
            InsertOrAssign(cats[index]);
        }
    }

    /// <summary>
    /// 衝突した瞬間
    /// </summary>
    /// <param name="collision">衝突相手</param>
    private void OnCollisionEnter(Collision collision)
    {
        string tag = collision.gameObject.tag;
        if (tag == "FirstCat" || tag == "SecondCat" || tag == "ThirdCat")
        {
            if (collision.gameObject == null)
            {
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void LeaveOwner()
    {
        Debug.Log("count : " + Cats.Count);
        foreach (GameObject cat in Cats)
        {
            if (cat.GetComponent<AICat>().enabled)
            {
                cat.GetComponent<AICat>().OnLeaveOwner();
                Debug.Log("bipedal");
            }
        }
    }

    private void OnDestroy()
    {
        CharacterCollector collector = CharacterCollector.Instance;

        for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
        {
            if(collector.GetCaracterObj(index) == null)
            {
                return;
            }
            GameObject camera = collector.GetCaracterObj(index).GetComponent<CharacterBase>()?.GetMyCamera();
            camera.GetComponent<CameraFilter>().DisableEffect();
        }

        sceneAdmin.GetComponent<PlayScene>().SetExistsOwner(false);
        LeaveOwner();
    }
}   