using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


/// <summary>
/// 飼い主が出現するために必要なイベントを管理するクラス
/// </summary>
public class OwnerEventController : MonoBehaviour
{
    private UnityEvent UnityEvent;
    private GameObject CutInBack;
    private GameObject OwnerCall;
    private GameObject CountDownCanvas;
    private GameObject TopFrame;
    private GameObject BottomFrame;
    private float Angle;

    /// <summary>
    /// 初期化
    /// </summary>
    private void Start()
    {
        OwnerCall = GameObject.FindWithTag("OwnerCall");
        CountDownCanvas = GameObject.FindWithTag("CountDownCanvas");

        ParticleManager.Instance.SetParticle("Stars_7.prefab", "Stars_7.prefab");


        if (UnityEvent == null)
        {
            UnityEvent = new UnityEvent();
        }

        Angle = 0.0f;

        UnityEvent.AddListener(OwnerCall.GetComponent<OwnerCall>().StartAnimation);

        UnityEvent.AddListener(async () =>
        {
            await AddressablesManager.Instance.InstantiateAsset("Warning.prefab", CountDownCanvas.transform/*, () => Debug.Log("test")*/);
            Vector3 adjustPos = new Vector3(0.0f, 100.0f, 0.0f);
            CutInBack = await AddressablesManager.Instance.InstantiateAsset("CutInBack.prefab", CountDownCanvas.transform);
            TopFrame = await AddressablesManager.Instance.InstantiateAsset("CutInFrameTop.prefab", adjustPos, Quaternion.identity, CountDownCanvas.transform);
            BottomFrame = await AddressablesManager.Instance.InstantiateAsset("CutInFrameBottom.prefab", -adjustPos, Quaternion.identity, CountDownCanvas.transform);
        });
    }

    /// <summary>
    /// 更新
    /// </summary>
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            EventExec();
        }
        float deltaAngle = 0.1f;
        Angle += deltaAngle;

        if(TopFrame != null && BottomFrame != null)
        {
            const float SPEED = 0.5f;
            TopFrame.transform.AddPosX(SPEED);
            BottomFrame.transform.AddPosX(-SPEED);
            Image topImage = TopFrame.GetComponent<Image>();
            Image bottomImage = BottomFrame.GetComponent<Image>();
            float deltaSin = Mathf.Sin(Angle);
            topImage.SetAlpha(deltaSin);
            bottomImage.SetAlpha(deltaSin);
        }
        if(CutInBack != null)
        {
            CutInBack.transform.SetSiblingIndex(2);
        }
        if (GameObject.FindWithTag("Owner") != null)
        {
            if(TopFrame != null)
            {
                AssetManager.Instance.ReleaseGameObject(TopFrame.gameObject);
            }
            if(BottomFrame != null)
            {
                AssetManager.Instance.ReleaseGameObject(BottomFrame.gameObject);
            }            

            if(CutInBack != null)
            {
                AssetManager.Instance.ReleaseGameObject(CutInBack.gameObject);
            }
        }

        SoundManager.Instance.SetSE("Warning", "Warning");
    }

    /// <summary>
    /// 飼い主出現のためのイベント実行
    /// </summary>
    /// <returns>true・・・実行成功 / false・・・実行失敗</returns>
    public bool EventExec()
    {
        if (OwnerCall.GetComponent<OwnerCall>().coolTimeFlag == false)
        {
            SoundManager.Instance.PlaySE("Warning");
            UnityEvent.Invoke();

            return true;
        }
        return false;
    }

    /// <summary>
    /// 飼い主接近中のカットインアニメーション終了時
    /// </summary>
    private async void EndOfCutInAnimation() => 
        await AssetManager.Instance.InstantiateGameObject("WarningTimer.prefab", CountDownCanvas.transform);
}