using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayScene : SceneBase
{
    private bool existsOwner;
    private int catCnt;

    [SerializeField] private GameObject catObj;
    [SerializeField] private GameObject ballObj;

    private GameObject[] characters;
    [SerializeField] private GameObject[] stages;
    [SerializeField] private GameObject[] startPos;

    [SerializeField] private Sprite[] readyGO;
    [SerializeField] private Sprite[] characterSprite;
    [SerializeField] private Sprite[] numberSprite;
    [SerializeField] private Sprite gameOverSprite;
    [SerializeField] private Sprite gameEndSprite;
    [SerializeField] private Material countDown;
    [SerializeField] private Shader numberMat;

    //改善予定
    private Vector2[] gameoverOffset = new Vector2[CharacterHelper.PLAYER_MAX]
    {
            new Vector2(25,-25),
            new Vector2(-25,-25),
            new Vector2(25,0),
            new Vector2(-25,0),
    };

    private Vector2[] iconOffset = new Vector2[CharacterHelper.PLAYER_MAX]
    {
            new Vector2(25,70),
            new Vector2(-25,70),
            new Vector2(25,95),
            new Vector2(-25,95),
    };


    new void Start()
    {
        base.Start();
        Referee.Start();
        PlayHelper.Instance.Start();

        LoadTexture();

        FadeManagerTest.Instance.InScene(1.0f, true);
        CharacterCollector collector = CharacterCollector.Instance;
        CharacterHelper helper = CharacterHelper.Instance;
        StageCollector stageCollector = StageCollector.Instance;
        SoundManager sound = SoundManager.Instance;

        string stageBgm = "Stage" + (int)stageCollector.GetStageType();
        sound.SetBgm(stageBgm, stageBgm);

        string ownerBgm = "Owner" + (int)stageCollector.GetStageType();
        sound.SetBgm(ownerBgm, ownerBgm);

        Instantiate(stages[(int)stageCollector.GetStageType()]);

        existsOwner = false;

        int cnt = 0;
        catCnt = 0;

        characters = new GameObject[CharacterHelper.PLAYER_MAX];

        ParticleManager manager = ParticleManager.Instance;
        manager.SetParticle("Boom_3.prefab", "Boom_3.prefab");

        //キャラクタの生成
        for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
        {
            CharacterType type = collector.GetCharacter(index);

            //GameObject character = type != CharacterType.BALL_OF_WOOL ? am.InstantiateAsset("PlayScene/Cat.prefab", Vector3.zero, Quaternion.identity).Result : am.InstantiateAsset("PlayScene/BollOfWool.prefab", Vector3.zero, Quaternion.identity).Result;
            GameObject character = type != CharacterType.BALL_OF_WOOL ? Instantiate(catObj) : Instantiate(ballObj);
            character.transform.position = startPos[(int)stageCollector.GetStageType()].transform.GetChild(index).transform.position;
            //
            character.transform.LookAt(Vector3.zero);

            GameObject ch = character.transform.Find("Character").gameObject;
            characters[index] = ch;

            //選択されたキャラクタに対応するゲームパッドの番号をセットする
            //FindByGamePad：引数のキャラクタータイプが何番のゲームパッドを使用しているか返す
            ch.GetComponent<CharacterBase>().SetGamePadNumber(collector.FindByGamePad(type));
            ch.GetComponent<CharacterBase>().SetsStartPos(startPos[(int)stageCollector.GetStageType()].transform.GetChild(index).transform.position);

            //画面上部にアイコンを置く            
            ViewPortFormatter view = ViewPortFormatter.Instance;
            Transform parent = GameObject.FindGameObjectWithTag("Icon").transform;
            UIGenerator.GenerateImage(characterSprite[(int)type], parent, view.GetScreenCenter(index) + iconOffset[index], Vector3.one, Vector3.one * 50.0f);

            GameObject number = UIGenerator.GenerateImage(numberSprite[index], parent, view.GetScreenCenter(index) + iconOffset[index] + new Vector2(0, 0), Vector3.one, Vector3.one * 10.0f);

            Material material = new Material(numberMat);
            material.SetColor("_addColor", CharacterHelper.Instance.THEME_COLOR);
            material.SetColor("_ColorX", CharacterHelper.Instance.THEME_COLOR);
            number.GetComponent<Image>().material = material;

            if (type != CharacterType.BALL_OF_WOOL)
            {
                ch.tag = helper.GetName(cnt);
                cnt++;
            }
            else
            {
                ch.tag = helper.GetName((int)CharacterType.BALL_OF_WOOL);
            }
        }
    }

    new void Update()
    {
        base.Update();

        StageCollector stageCollector = StageCollector.Instance;
        SoundManager.Instance.PlayBGM("Stage" + (int)stageCollector.GetStageType());

        if (catCnt == CharacterHelper.PLAYER_MAX - 1)
        {
            Referee.isCatFoundToOwner = true;
            PlayHelper.Instance.EndGame();
        }

        if (existsOwner == false)
        {
            //mixRate -= Time.deltaTime;
            mixRate = Mathf.Clamp(mixRate - Time.deltaTime, 0.0f, 1.0f);
            Text text = GameObject.FindGameObjectWithTag("Icon").gameObject.transform.Find("CountDownText").gameObject.GetComponent<Text>();
            text.fontSize = 50;
        }
        else
        {
            mixRate = Mathf.Clamp(mixRate + Time.deltaTime, 0.0f, 1.0f);

            Text text = GameObject.FindGameObjectWithTag("Icon").gameObject.transform.Find("CountDownText").gameObject.GetComponent<Text>();
            //text.fontSize = (int)(Mathf.Lerp(text.fontSize, 75 , 0.1f) * Mathf.Sign(Time.time));
            text.fontSize = (int)(50 + (10 * Mathf.Sin(Time.time * 5.0f) + 10));
            text.text = "うごくな！！";
        }

        if (PlayHelper.Instance.IsEndGame() == true)
        {
            StartCoroutine(EndPlayScene());
        }
    }

    private void LateStart()
    {
        StartCoroutine(ReadyGo(2));

        foreach (GameObject chara in characters)
        {
            chara.GetComponent<CharacterBase>().LateStart();
        }
    }

    public void SetExistsOwner(bool exists)
    {
        existsOwner = exists;
    }

    /// <summary>
    /// ネコが見つかった時に、ネコから呼ばれる関数
    /// ここで見つかった
    /// </summary>
    /// <param name="num"></param>
    public void FoundCat(int num)
    {
        StartCoroutine(GameoverCharacter(3.0f, num));
        catCnt++;
    }

    public void FoundBallOfWool(int num)
    {
        Referee.isFoundOwner = true;

        //シーン切替
        StartCoroutine(EndPlayScene());
    }

    public IEnumerator EndPlayScene()
    {
        if (PlayHelper.Instance.IsStart() == true)
        {
            //Transform parent = GameObject.FindGameObjectWithTag("CountDownCanvas").transform;
            //UIGenerator.GenerateImage(gameEndSprite, parent, Vector3.zero, Vector3.one, Vector3.one * 150.0f);
        }

        Text text = GameObject.FindGameObjectWithTag("Icon").gameObject.transform.Find("CountDownText").gameObject.GetComponent<Text>();
        text.fontSize = 30;
        text.text = "おわり！！";
        PlayHelper.Instance.EndPlayScene();

        yield return new WaitForSeconds(3.0f);

        FadeManagerTest.Instance.LoadScene("ResultScene", 1.0f);

        yield return 0;
    }


    private IEnumerator GameoverCharacter(float countTime, int num)
    {
        yield return new WaitForSeconds(countTime);

        //ゲームオーバーの画像表示
        ViewPortFormatter view = ViewPortFormatter.Instance;
        Transform parent = GameObject.FindGameObjectWithTag("Icon").transform;
        UIGenerator.GenerateImage(gameOverSprite, parent, view.GetScreenCenter(num) + gameoverOffset[num], Vector3.one);

        yield return 0;
    }

    //別スクリプトにする

    /// <summary>
    /// ReadyGOアニメーションコルーチン
    /// </summary>
    /// <param name="countTime">アニメーションにかける時間</param>
    /// <returns></returns>
    private IEnumerator ReadyGo(float countTime)
    {
        yield return new WaitForSeconds(1.0f);

        float time = 0;
        int turn = 0;
        int interval = 30;

        Transform parent = GameObject.FindGameObjectWithTag("CountDownCanvas").transform;
        GameObject readyGo = UIGenerator.GenerateImage(gameOverSprite, parent, new Vector3(0.0f, 125.0f, 0.0f), Vector3.one, Vector2.one * 100.0f);

        readyGo.AddComponent<ReadyGo>();
        readyGo.GetComponent<Image>().material = countDown;

        while (time <= 1)
        {
            turn = ReadyGoRotate(readyGo, time, turn, interval);

            time += Time.deltaTime;

            yield return 0;
        }

        turn = 0;

        ParticleManager manager = ParticleManager.Instance;
        manager.InstantiateParticle("Boom_3.prefab");


        PlayHelper.Instance.StartPlayScene();

        while (time <= 2)
        {
            turn = ReadyGoRotate(readyGo, time, turn, interval);

            time += Time.deltaTime;

            yield return 0;
        }

        Destroy(readyGo);
    }

    private int ReadyGoRotate(GameObject readyGo, float time, int turn, int interval)
    {
        readyGo.GetComponent<Image>().sprite = readyGO[(int)time];

        RectTransform rect = readyGo.GetComponent<RectTransform>();

        if (turn < 360 + interval)
        {
            rect.localRotation = Quaternion.Euler(0, 0, turn);
            rect.localScale = Vector3.one * (turn * 0.005f);

            turn += interval;
        }

        return turn;
    }

    private async void LoadTexture()
    {
        string EXTENSION = ".png";

        AddressablesManager am = new AddressablesManager();

        for (int index = 0; index < (int)CharacterType.MAX - 1; ++index)
        {
            CharacterHelper helper = CharacterHelper.Instance;
            var charaTex = am.LoadTextureAsset(helper.GetName(index) + EXTENSION);
            await charaTex;

            helper.SetMainTex(index, charaTex.Result);

            am.ReleaseTextureAssets(charaTex.Result);
        }

        LateStart();
    }
}