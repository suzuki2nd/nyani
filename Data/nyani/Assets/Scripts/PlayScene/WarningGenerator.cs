using UnityEngine;

public class WarningGenerator : MonoBehaviour
{
    private AddressablesManager Manager;
    private GameObject CountDownCanvas;

    /// <summary>
    /// 初期化
    /// </summary>
    private void Start()
    {
        Manager = new AddressablesManager();
        CountDownCanvas = GameObject.FindWithTag("CountDownCanvas");
    }

    /// <summary>
    /// 飼い主の出現を警告するためのUIを生成
    /// </summary>
    public async void Instantiate()
    {
        GameObject WarningImage = await Manager.InstantiateAsset("Warning.prefab", CountDownCanvas.transform);
    }
}