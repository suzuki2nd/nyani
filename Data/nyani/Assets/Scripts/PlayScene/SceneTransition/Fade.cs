using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fade : MonoBehaviour
{ 
    //フェード用のキャンバスとイメージ
    private static Canvas fadeCanvas;
    private static Image fadeImage;
    //フェード用Imageの透明度
    private static float alpha;

    //フェードしたい時間(秒)
    private static float fadeTime;

    //フェードインアウトのフラグ
    public static bool isFadeIn;
    public static bool isFadeOut;

    //シーン遷移したい番号
    private static int nextSceneIndex;
    //次のシーンに移動するかどうか
    private static bool nextSceneBool;


    /// <summary>
    /// 初期化
    /// </summary>
    static void Init()
    {
        isFadeIn = false;
        isFadeOut = false;
        fadeTime = 1.0f;
        nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

        //フェード用のCanvas生成
        GameObject fadeCanvasObject = new GameObject("CanvasFade");
        fadeCanvas = fadeCanvasObject.AddComponent<Canvas>();
        fadeCanvasObject.AddComponent<GraphicRaycaster>();
        fadeCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        fadeCanvasObject.AddComponent<Fade>();

        //最前面になるようにソートオーダー設定
        fadeCanvas.sortingOrder = 50;

        //フェード用のImage生成
        fadeImage = new GameObject("ImageFade").AddComponent<Image>();
        fadeImage.transform.SetParent(fadeCanvas.transform, false);
        fadeImage.rectTransform.anchoredPosition = Vector3.zero;

        // Imageサイズ設定
        fadeImage.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
    }

    /// <summary>
    /// 更新
    /// </summary>
    void Update()
    {
        //フェードイン/アウト処理
        if (isFadeIn)
        {
            //経過時間から透明度計算
            alpha -= Time.deltaTime / fadeTime;
            //フェードイン終了判定
            if(alpha <= 0.0f)
            {
                alpha = 0.0f;
                fadeCanvas.enabled = false;
                isFadeIn = false;
            }
            //フェード用Imageの色・透明度設定
            fadeImage.color = new Color(0.0f, 0.0f, 0.0f, alpha);
        }
        else if (isFadeOut)
        {
            //経過時間から透明度計算
            alpha += Time.deltaTime / fadeTime;
            //フェードイン終了判定
            if (alpha >= 1.0f)
            {
                alpha = 1.0f;
                isFadeOut = false;

                if (nextSceneBool)
                {
                    SceneManager.LoadScene(nextSceneIndex);
                }
            }
            //フェード用Imageの色・透明度設定
            fadeImage.color = new Color(0.0f, 0.0f, 0.0f, alpha);
        }
    }

    /// <summary>
    /// フェードインするための関数
    /// </summary>
    public static void FadeIn()
    {
        if(fadeImage == null)
        {
            //CanvasとImageの生成
            Init();
        }
        fadeImage.color = Color.black;
        isFadeIn = true;
    }
    /// <summary>
    /// フェードアウトするための関数
    /// </summary>
    /// <param name="sceneTransition">シーン遷移するかの判定</param>
    public static void FadeOut(bool sceneTransition)
    {
        if(fadeImage == null)
        {
            //CanvasとImageの生成
            Init();
        }
        fadeImage.color = Color.clear;
        fadeCanvas.enabled = true;
        isFadeOut = true;
        nextSceneBool = sceneTransition;
    }
}
