using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// パネルの表示(カウント込み)と画面遷移
/// </summary>
public class SceneTransitionFade : MonoBehaviour
{
    [SerializeField]
    private Text countDownText;
    [SerializeField]
    private GameObject backGroundPanel;
    private float transitionCount = 3.0f;
    //private bool countDownFlag;

    /// <summary>
    /// 初期化
    /// </summary>
    //private void Start()
    //{
    //    backGroundPanel.SetActive(false);
    //    countDownFlag = false;
    //}

    /// <summary>
    /// 更新
    /// </summary>
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Return))
        //{
        //    backGroundPanel.SetActive(true);
        //    countDownFlag = true;
        //}
        //if (countDownFlag)
        //{
            countDown();
        //}
    }

    private void countDown()
    {
        this.countDownText.text = string.Format("{0:0}", this.transitionCount);
        this.transitionCount -= Time.deltaTime;
        if(transitionCount <= 0)
        {
            this.transitionCount = 0;
            //Fade.FadeOut(true);
        }
    }
}

