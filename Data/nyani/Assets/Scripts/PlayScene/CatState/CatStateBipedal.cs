using UnityEngine;

public class CatStateBipedal : CatStateBase
{
    public CatStateBipedal(GameObject catObj) : base(catObj)
    {
        animator.SetBool("quadrupedalToBipedal", true);
        animator.SetBool("bipedalToQuadrupedal", false);

        walkSpeed = 2.0f;
        stateName = "CatStateBipedal";
    }

    public override void Update(Vector3 moveDir)
    {
        base.Update(moveDir);

        boxCollider.center = Vector3.Lerp(boxCollider.center, new Vector3(0.0f, 0.1f, 0.0f), 0.1f);
        boxCollider.size = Vector3.Lerp(boxCollider.size, new Vector3(0.1f, 0.2f, 0.15f), 0.1f);
    }

    public override void Action()
    {
        base.Action();
        
        animator.SetBool("dive", true);
    }

    public override void ActionEnd()
    {
        base.ActionEnd();

        animator.SetBool("dive", false);
    }
}
