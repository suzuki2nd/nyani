using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatStateWin : CatStateBase
{
    public CatStateWin(GameObject catObj) : base(catObj)
    {
        walkSpeed = 0.0f;
        stateName = "CatStateWin";

        animator.SetFloat("speed", 0.0f);
        animator.SetBool("dive", false);
        animator.SetBool("Win" + Random.Range(1, 4), true);
    }

    public override void Update(Vector3 moveDir)
    {
        base.Update(moveDir);
        animator.SetFloat("speed", 0.0f);
        animator.SetBool("dive", false);
    }

    public override void Action()
    {
        base.Action();
    }
}
