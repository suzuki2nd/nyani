using UnityEngine;

public class CatStateFound : CatStateBase
{
    public CatStateFound(GameObject catObj) : base(catObj)
    {
        walkSpeed = 0.0f;
        stateName = "CatStateFound";

        animator.SetBool("Lose" + Random.Range(1, 4), true);
    }

    public override void Update(Vector3 moveDir)
    {
        base.Update(moveDir);
        animator.SetFloat("speed", 0.0f);
        animator.SetBool("dive", false);
    }

    public override void Action()
    {
        base.Action();
    }
}
