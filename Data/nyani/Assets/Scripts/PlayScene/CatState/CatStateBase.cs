using UnityEngine;

public class CatStateBase
{
    protected float walkSpeed;
    protected string stateName;

    protected Animator animator;
    protected GameObject catObj;
    protected BoxCollider boxCollider;

    public float WalkSpeed
    {
        get
        {
            return walkSpeed;
        }

        set
        {
            walkSpeed = value;
        }
    }

    public string StateName
    {
        get
        {
            return stateName;
        }

        set
        {
            stateName = value;
        }
    }

    public CatStateBase(GameObject cat)
    {
        catObj = cat;
        animator = cat.GetComponent<Animator>();
        boxCollider = cat.transform.Find("Character1_Reference").GetComponent<BoxCollider>();
    }

    public virtual void Update(Vector3 moveDir)
    {
        Vector3 move = moveDir;
        move.y = 0.0f;

        //現在のスピードに応じて
        //歩行モーションを制御する
        animator.SetFloat("speed", move.magnitude);
    }

    public virtual void Action()
    {
    }

    public virtual void ActionEnd()
    {

    }

    public int GetAnimationSpeed()
    {
        return (int)animator.speed;
    }

    public void AnimationStop()
    {
        animator.speed = 0;
    }

    public void AnimationStart()
    {
        animator.speed = 1;
    }

}
