using UnityEngine;

public class CaStateQuadrupedal : CatStateBase
{
    public CaStateQuadrupedal(GameObject catObj) : base(catObj)
    {
        animator.SetBool("quadrupedalToBipedal", false);
        animator.SetBool("bipedalToQuadrupedal", true);

        animator.SetBool("dive", false);

        walkSpeed = 1.0f;
        catObj.GetComponent<Cat>().DisablePossible();
        stateName = "CatStateQuadrupedal";
    }

    public override void Update(Vector3 moveDir)
    {
        base.Update(moveDir);

        boxCollider.center = Vector3.Lerp(boxCollider.center, new Vector3(0.0f, 0.05f, 0.05f), 0.1f);
        boxCollider.size = Vector3.Lerp(boxCollider.size, new Vector3(0.1f, 0.1f, 0.2f), 0.1f);
    }

    public override void Action()
    {
        base.Action();
    }
}
