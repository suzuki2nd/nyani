using UnityEngine;
using UnityEngine.UI;

public class OwnerCall : MonoBehaviour
{
    [SerializeField] public float countTime;
    private GameObject child;
    Image ownerCallImage;
    //ボタンが押されたとき用クールタイムフラグ
    public bool coolTimeFlag;
    //半透明化するための変数
    private float fillAmountMin;
    private float fillAmountMax;


    /// <summary>
    /// 初期化
    /// </summary>
    void Start()
    {
        this.ownerCallImage = this.GetComponent<Image>();
        
        this.coolTimeFlag = false;
        this.fillAmountMin = 0f;
        this.fillAmountMax = 1.0f;
        this.ownerCallImage.fillAmount = this.fillAmountMax;

        child = transform.Find("child").gameObject;
        child.SetActive(false);
    }
    /// <summary>
    /// 更新
    /// </summary>
    void Update()
    {
        if (coolTimeFlag)
        {
            CoolTime();
        }
    }

    /// <summary>
    /// クールタイムの実行処理
    /// </summary>
    private void CoolTime()
    {
        this.ownerCallImage.fillAmount += 1.0f / this.countTime * Time.deltaTime;

        if(this.ownerCallImage.fillAmount == this.fillAmountMax)
        {
            child.SetActive(false);
            this.coolTimeFlag = false;
        }
    }

    /// <summary>
    /// 飼い主呼び出しボタンのアニメーションをスタート
    /// </summary>
    public void StartAnimation()
    {
        if(coolTimeFlag)
        {
            return;
        }

        this.coolTimeFlag = true;
        this.ownerCallImage.fillAmount = this.fillAmountMin;
        child.SetActive(true);
    }
}
