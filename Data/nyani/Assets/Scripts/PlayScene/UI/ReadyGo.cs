using UnityEngine;
using UnityEngine.UI;

public class ReadyGo : MonoBehaviour
{
    private float offset;
    private float val;

    void Start()
    {
        offset = 2.0f;
        val = 0.0f;
    }

    void Update()
    {
        val = Time.time * offset;

        Material material = this.GetComponent<Image>().material;
        material.SetFloat("_ScrollX", val);
        material.SetFloat("_ScrollY", -val);
    }
}
