using UnityEngine;
using UnityEngine.UI;

public class TimerFrame : MonoBehaviour
{
    [SerializeField] private GameObject countdown;
    [SerializeField] private Sprite[] frames;
    [SerializeField] private Font[] fonts;

    void Start()
    {
        int num = (int)StageCollector.Instance.GetStageType();
        this.gameObject.GetComponent<Image>().sprite = frames[num];

        if(num == 1)
        {
            RectTransform rect = this.gameObject.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(250.0f, 150.0f);

            countdown.GetComponent<Text>().color = Color.black;
            countdown.GetComponent<Outline>().effectColor = Color.white;
        }

        countdown.GetComponent<Text>().font = fonts[num];  
    }
}
