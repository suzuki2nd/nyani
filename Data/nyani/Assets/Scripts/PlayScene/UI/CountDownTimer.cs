using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CountDownTimer : MonoBehaviour
{
    //カウントダウン表示用テキスト
    public Text countDownText;
    [SerializeField] private int minitue = 1;
    [SerializeField] private float seconds = 30.0f;
    //前回のUpdate時の秒数
    private float totalTime;
    private bool counting;
    //GameObjectの生成用
    private AddressablesManager AddressablesManager;

    /// <summary>
    /// 初期化
    /// </summary>
    void Start()
    {
        countDownText = GetComponent<Text>();
        this.totalTime = minitue * 60 + seconds;
        //制限時間を止める用
        this.counting = true;
        //呼び出したいPrefab       
        AddressablesManager = new AddressablesManager();
    }
    /// <summary>
    ///　更新
    /// </summary>
    void Update()
    {
        PlayHelper helper = PlayHelper.Instance;

        if (counting && helper.IsStart() == true)
        {
            countDownTimer();
        }
    }

    async public void countDownTimer()
    {
        if (this.totalTime <= 0)
        {
            //制限時間が終了したなら下記に記述
            counting = false;

            Referee.isTimeUp = true;
            PlayHelper.Instance.EndGame();
            //Fade.FadeOut(true);
        }

        totalTime = minitue * 60 + seconds;
        //制限時間(秒)を減らす
        totalTime -= Time.deltaTime;

        minitue = (int)totalTime / 60;
        seconds = totalTime % 60;
        //制限時間のUI表示
        this.countDownText.text = minitue.ToString("00") + ":" + ((int)seconds).ToString("00");
    }
}
