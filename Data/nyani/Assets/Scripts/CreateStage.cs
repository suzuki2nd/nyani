using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateStage : MonoBehaviour
{
    [SerializeField]
    private GameObject mainCamera;
    [SerializeField]
    private List<Vector3> cameraPositions;
    [SerializeField]
    private List<Vector3> cameraRotations;
    [SerializeField]
    private Vector3 stagePos;
    /// <summary>
    /// ステージの生成(addressables)
    /// </summary>
    private async void Start()
    {
        AddressablesManager addressablesManager = new AddressablesManager();
        StageType selectStageType = StageCollector.Instance.GetStageType();
        StageHelper stageHelper = StageHelper.Instance;
        string address;

        address = stageHelper.GetName((int)selectStageType) + ".prefab";
        //address = "FirstStage.prefab";
        //address = "SecondStage.prefab";
        //address = "ThirdStage.prefab";
        switch (address)
        {
            case "FirstStage.prefab":
                mainCamera.transform.SetPos(cameraPositions[0]);
                mainCamera.transform.rotation = Quaternion.Euler(cameraRotations[0]);
                await addressablesManager.InstantiateAsset(address,stagePos,Quaternion.identity);
                break;
            case "SecondStage.prefab":
                mainCamera.transform.SetPos(cameraPositions[1]);
                mainCamera.transform.rotation = Quaternion.Euler(cameraRotations[1]);
                await addressablesManager.InstantiateAsset(address, stagePos, Quaternion.identity);
                break;
            case "ThirdStage.prefab":
                mainCamera.transform.SetPos(cameraPositions[2]);
                mainCamera.transform.rotation = Quaternion.Euler(cameraRotations[2]);
                await addressablesManager.InstantiateAsset(address, stagePos, Quaternion.Euler(0.0f, 90.0f, 0.0f));
                break;
        }
        
        
    }
}
