using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// キャラクタを選択する際の画像型ボタン
/// </summary>
public class CharacterImageButton : ImageButtonBase
{
    private GameObject sceneAdmin;
    private Material material;

    [SerializeField] private int imageNumber;
    [SerializeField] private Texture mainTex;
    [SerializeField] private Texture subTex;
    [SerializeField] private Shader shader;

    new void Start()
    {
        base.Start();

        sceneAdmin = GameObject.FindGameObjectWithTag("SceneAdmin").gameObject;

        material = new Material(shader);
        this.GetComponent<Image>().material = material;

        material.SetTexture("_MainTex", mainTex);
        material.SetTexture("_SubTex", subTex);
        material.SetFloat("_OutLineSpread", 0.02f);
        material.SetColor("_ColorX", CharacterHelper.Instance.THEME_COLOR) ;
        //material.SetColor("_ColorX", new Color(0.254902f, 0.4117647f, 0.882353f, 1.0f));

        material.SetInt("_v", 0);
    }

    void Update()
    {
        CharacterCollector collector = CharacterCollector.Instance;
        CharacterHelper helper = CharacterHelper.Instance;

        //Debug.Log(helper.GetType(this.gameObject.tag));
        int num = collector.FindByPad(helper.GetType(this.gameObject.tag));


        selectable = num >= 0 ? false : true;

        material.SetInt("_v", selectable == true ? 0 : 1) ;
    }

    public override void OnGamePadButtonDown(int num)
    {
        if (selectable == true)
        {
            base.OnGamePadButtonDown(num);

            CharacterCollector collector = CharacterCollector.Instance;
            CharacterHelper helper = CharacterHelper.Instance;


            CharacterType type = helper.GetType(this.gameObject.tag);

            material.SetColor("_ColorX", outlineColor);
            //選んだキャラクターを登録
            collector.SelectCharacter(num, type);

            cursor.GetComponent<CursorBase>().DisebleMove();
        }
    }

    /// <summary>
    /// 当たり続けている場合に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    override protected void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
    }
}
