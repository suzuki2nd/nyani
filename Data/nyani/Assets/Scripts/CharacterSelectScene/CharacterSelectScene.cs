using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// キャラクタ選択シーンクラス
/// </summary>
public class CharacterSelectScene : SceneBase
{
    [SerializeField] private GameObject camerasObj;
    [SerializeField] private GameObject chanvasObj;
    [SerializeField] private GameObject previewObj;
    [SerializeField] private GameObject woolPreviewObj;
    [SerializeField] private GameObject backSceneObj;
    [SerializeField] private Shader shader;

    CharacterType[] previewTypes = new CharacterType[CharacterHelper.PLAYER_MAX - 1];
    private GameObject[] catPreviws = new GameObject[CharacterHelper.PLAYER_MAX - 1];

    [SerializeField] private Sprite[] sprites;

    Vector2[] pos =
    {
        new Vector2(-180.0f,120.0f),
        new Vector2(180.0f,130.0f),
        new Vector2(90.0f,45.0f),
        new Vector2(270.0f,45.0f),
    };

    private GameObject[] cursors = new GameObject[CharacterHelper.PLAYER_MAX];
    private GameObject[] playerNumbers = new GameObject[CharacterHelper.PLAYER_MAX];
    private bool isFirst = true;

    new void Start()
    {
        base.Start();
        FadeManagerTest.Instance.InScene(1.0f);

        CharacterCollector.Instance.Start();
        CharacterHelper.Instance.Start();

        SoundManager sound = SoundManager.Instance;
        sound.SetBgm("SelectBgm", "SelectBgm");

        //キャラクターアイコンとプレビューの生成
        //ゲームパッドカーソルの生成
        //四人分生成するためにループ
        PreviewGeneration();
    }

    new void Update()
    {
        base.Update();

        StageCollector.Instance.Start();

        StageCollector stageCollector = StageCollector.Instance;
        SoundManager.Instance.PlayBGM("SelectBgm");

        CharacterCollector collector = CharacterCollector.Instance;
        //選択されたキャラクターの数をセット
        //collector.SelectCharacterNum(GetCheck());

        int cnt = 0;

        for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
        {
            CharacterType type = collector.GetCharacter(index);

            if (type != CharacterType.BALL_OF_WOOL && cnt < 3)
            {
                previewTypes[cnt] = type;
                cnt++;
            }

            //Debug.Log(type);
        }

        for (int index = 0; index < CharacterHelper.PLAYER_MAX - 1; ++index)
        {
            catPreviws[index].GetComponent<CharacterPreview>().SetCharacterType(CharacterType.NONE);
        }

        int previewCnt = 0;


        for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
        {
            //if (previewTypes[index] != CharacterType.NONE)
            CharacterType type = collector.GetCharacter(index);
            SetPlayerNumber(index, type, false);

            if (type == CharacterType.BALL_OF_WOOL)
            {
                SetPlayerNumber(index, type, true);
            }

            if (type != CharacterType.NONE && type != CharacterType.BALL_OF_WOOL && previewCnt < 3)
            {
                //catPreviws[previewCnt].GetComponent<CharacterPreview>().SetCharacterType(previewTypes[index]);

                catPreviws[previewCnt].GetComponent<CharacterPreview>().SetCharacterType(type);
                previewCnt++;

                SetPlayerNumber(index, type, true);

                //Debug.Log(type);
            }
        }




        //すべてのチェックオブジェクトが表示されている（チェック済み）の場合
        if (IsCheckedAll() == true)
        {
            if (PlayHelper.Instance.GetEntryNum() == 4)
            {
                //選ばれたキャラクターの数が正しい場合
                if (collector.IsValidCharacter() == true)
                {
                    FadeManagerTest.Instance.LoadScene("StageSelectScene", 1.0f);
                }
            }
            //4人プレイ以外
            else
            {
                //AIで選択するキャラクタをランダム決定
                int num = PlayHelper.Instance.GetEntryNum();
                //コントローラーで選ばれたキャラクターの重複チェック
                CharacterType[] seectTypes = new CharacterType[num];

                for (int index = 0; index < num; ++index)
                {
                    seectTypes[index] = collector.GetCharacter(index);
                }

                CharacterType[] distinctTypes = seectTypes.Distinct().ToArray();

                //重複がある場合は中止
                if (distinctTypes.Length != num) return;

                SelectCharacter();
            }
        }
    }

    private void SelectCharacter()
    {
        if (isFirst == true)
        {
            isFirst = false;
            int num = PlayHelper.Instance.GetEntryNum();

            CharacterCollector collector = CharacterCollector.Instance;
            CharacterHelper helper = CharacterHelper.Instance;

            //コントローラーの数分、選択されるキャラクターを格納する変数
            CharacterType[] selectCharacterTypes = new CharacterType[num];
            List<CharacterType> notSelectedTypes = new List<CharacterType>();

            //全キャラクタータイプをリストに追加
            for (int index = 0; index < (int)CharacterType.MAX; ++index)
            {
                notSelectedTypes.Add(helper.GetType(index));
            }

            for (int index = 0; index < num; ++index)
            {
                selectCharacterTypes[index] = collector.GetCharacter(index);
            }

            foreach (CharacterType selectCharacterType in selectCharacterTypes)
            {
                notSelectedTypes.RemoveAll(num => num == selectCharacterType);
            }

            for (int index = 0; index < CharacterHelper.PLAYER_MAX - num; ++index)
            {
                CharacterType t = GetRandom(notSelectedTypes);
                collector.SelectCharacter(num + index, t);
                notSelectedTypes.Remove(t);
            }

            CharacterType[] chara = new CharacterType[CharacterHelper.PLAYER_MAX];


            for (int index = 0; index < chara.Length; ++index)
            {
                chara[index] = collector.GetCharacter(index);
            }

            bool ball = false;

            //選択された中に 毛玉が含まれているか確認
            foreach (CharacterType type in chara)
            {
                //毛玉が含まれている
                if (type == CharacterType.BALL_OF_WOOL)
                {
                    ball = true;
                }
            }

            if (ball == false)
            {
                chara[CharacterHelper.PLAYER_MAX - 1] = CharacterType.BALL_OF_WOOL;
                collector.SelectCharacter(CharacterHelper.PLAYER_MAX - 1, CharacterType.BALL_OF_WOOL);
            }

            foreach (GameObject index in cursors)
            {
                index.GetComponent<CharacterSelectCursor>().EnableTransrate();
                index.GetComponent<CursorBase>().DisebleMove();
                index.GetComponent<CharacterSelectCursor>().SelectMotion();
            }

            FadeManagerTest.Instance.LoadScene("StageSelectScene", 2.0f);
        }
    }

    private T GetRandom<T>(List<T> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    /// <summary>
    /// プレビューの生成
    /// </summary>
    private void PreviewGeneration()
    {
        //画面サイズによって位置が変わってしまうので、対処必要
        Vector2[] previewPos = new Vector2[CharacterHelper.PLAYER_MAX]
        {
            new Vector2(-200,150),
            new Vector2(200,150),
            new Vector2(-200,-75),
            new Vector2(200,-75)
        };

        ViewPortFormatter viewPort = ViewPortFormatter.Instance;

        float offset = 225.0f;

        GameObject cameras = Instantiate(camerasObj, transform);


        Camera uiCamera = cameras.transform.Find("UICamera").gameObject.GetComponent<Camera>();

        //プレビューオブジェクト生成
        GameObject canvas = Instantiate(chanvasObj, transform);

        Canvas c = canvas.GetComponent<Canvas>();
        c.worldCamera = uiCamera;

        //戻るボタン生成
        GameObject back = Instantiate(backSceneObj, transform);
        back.transform.SetParent(c.transform, false);

        RectTransform backRect = back.GetComponent<RectTransform>();
        backRect.localPosition = viewPort.GetScreenCenter(0) + new Vector2(-130, 45);

        //戻るシーン先の設定
        back.GetComponent<SceneBackButton>().SetSceneName("EntryScene");

        GameObject preview = canvas.transform.Find("Preview").gameObject;
        RectTransform preRect = preview.GetComponent<RectTransform>();


        //UI座標からスクリーン座標に変換
        Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(c.worldCamera, preRect.position);

        //ワールド座標
        Vector3 result = Vector3.zero;

        //スクリーン座標→ワールド座標に変換
        RectTransformUtility.ScreenPointToWorldPointInRectangle(preRect, screenPos, c.worldCamera, out result);


        //result = new Vector3(result.x + offset * index, result.y, result.z);
        result = new Vector3(result.x + offset, result.y, result.z);
        uiCamera.transform.position = result;

        //位置を調整
        result.y = -1.0f;
        result.z += 10.0f;


        float catOffset = -4.0f;

        Vector3[] catPositions = new Vector3[CharacterHelper.PLAYER_MAX]
        {
                new Vector3(221.0f,0.0f,110.0f),
                new Vector3(229.0f,1.4f,110.0f),
                new Vector3(227.0f,-0.5f,110.0f),
                new Vector3(231.0f,-0.5f,110.0f),
        };

        CharacterHelper helper = CharacterHelper.Instance;

        int cnt = 0;

        for (int index = 0; index <CharacterHelper.PLAYER_MAX ; ++index)
        {
            //プレビューを表示するネコ生成
            GameObject previewCat;

            //カーソルの生成
            GameObject cursor = base.CursorGeneration<CharacterSelectCursor>(index, Vector2.one * 50.0f, canvas, shader);

            cursors[index] = cursor;

            if (index == 0)
            {
                previewCat = Instantiate(woolPreviewObj, transform);
            }
            else
            {
                previewCat = Instantiate(previewObj, transform);


                //カーソルとネコを関連付けてはいけない
                cursor.GetComponent<CharacterSelectCursor>().SetPreviewCat(previewCat);
                catPreviws[cnt] = previewCat;

                cnt++;
            }

            result.x += index * catOffset;
            catOffset += 2.0f;

            //初期位置設定
            previewCat.transform.position = catPositions[index];

            Transform parent = GameObject.FindGameObjectWithTag("Icon").transform;

            playerNumbers[index] = UIGenerator.GenerateImage(null, parent, Vector2.zero, Vector3.one, Vector3.one * 50.0f);
            playerNumbers[index].SetActive(false);

            this.GetComponent<ActiveChanger>().SetActiveTarget(index, playerNumbers[index]);
        }
    }

    public void SetPlayerNumber(int num, CharacterType characterType, bool active)
    {
        this.GetComponent<ActiveChanger>().ActiveChange(active, num);

        RectTransform rectTransform = playerNumbers[num].GetComponent<RectTransform>();

        if (active == true)
        {
            playerNumbers[num].GetComponent<Image>().sprite = sprites[num];

            CharacterCollector collector = CharacterCollector.Instance;
            int previewCnt = 0;


            for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
            {
                CharacterType type = collector.GetCharacter(index);

                if (type != CharacterType.NONE && type != CharacterType.BALL_OF_WOOL && previewCnt < 3)
                {
                    previewCnt++;

                    if (type == characterType)
                    {
                        break;
                    }
                }
            }

            if (characterType != CharacterType.BALL_OF_WOOL)
            {
                rectTransform.localPosition = pos[previewCnt];
            }
            else
            {
                rectTransform.localPosition = pos[0];
            }
        }
    }

    /// <summary>
    /// チェックされたカウント判定
    /// </summary>
    /// <returns></returns>
    public int GetCheck()
    {
        int cnt = 0;

        foreach (GameObject index in playerNumbers)
        {
            if (index.activeSelf == true)
            {
                cnt++;
            }
        }
        return cnt;
    }


    /// <summary>
    /// すべてチェックされているか判定
    /// </summary>
    /// <returns> すべてチェック済みの場合 true </returns>
    private bool IsCheckedAll()
    {
        bool checkedAll = true;

        int entryNum = PlayHelper.Instance.GetEntryNum();

        for (int index = 0; index < entryNum; ++index)
        {
            if (playerNumbers[index].activeSelf == false)
            {
                checkedAll = false;
            }
        }

        return checkedAll;
    }
}
