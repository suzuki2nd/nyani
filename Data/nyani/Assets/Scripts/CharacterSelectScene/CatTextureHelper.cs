using UnityEngine;

public class CatTextureHelper : MonoBehaviour
{
    [SerializeField] private Texture[] mainTextures = new Texture[(int)CatType.MAX];
    [SerializeField] private Texture[] shadowTextures = new Texture[(int)CatType.MAX];

    void Start()
    {
        CharacterHelper helper = CharacterHelper.Instance;

        for (int index = 0; index < (int)CatType.MAX; ++index)
        {
            helper.SetMainTex(index, mainTextures[index]);
            helper.SetShadowTex(index, shadowTextures[index]);
        }
    }
}
