using UnityEngine;
using UnityEngine.UI;

//対応するゲームパッドが選んだキャラクターのプレビューを表示する
public class CharacterPreview : MonoBehaviour
{
    private bool isSelect;
    private float val;


    private CharacterType characterType;
    private CharacterType prevType;
    private GameObject poly;
    //[SerializeField] private GameObject wool;
    private GameObject particle;
    Material catMaterial;
    //Material woolMaterial;

    private void Start()
    {
        //characterType = CharacterType.CAT_FIRST;
        
        characterType = CharacterType.NONE;
        prevType = CharacterType.NONE;

        poly = transform.Find("polySurface30").gameObject;
        //wool = transform.Find("Character").gameObject;

        particle = transform.Find("Select").gameObject;

        //woolMaterial = wool.transform.Find("Character1_Reference").gameObject.GetComponent<Renderer>().material;
        //woolMaterial.SetFloat("val", -1.0f);

        catMaterial = poly.GetComponent<Renderer>().material;
        catMaterial.SetFloat("val", -1.0f);
    }

    void Update()
    {
        //画像変更
        ChangePreview();
    }

    /// <summary>
    /// プレビュー画像を変更する
    /// </summary>
    private void ChangePreview()
    {
        if (characterType != CharacterType.NONE)
        {
            CharacterHelper helper = CharacterHelper.Instance;
            catMaterial.SetTexture("_MainTex", helper.GetMainTex((int)characterType));
            this.GetComponent<Animator>().SetBool("emoteGenbaneko", true);

            val = Mathf.Clamp(val + 5.0f * Time.deltaTime, 0.0f , 1.5f);
            catMaterial.SetFloat("val", val);

            if(isSelect == false)
            {
                Effect();

                isSelect = true;
            }
        }
        else
        {
            this.GetComponent<Animator>().SetBool("emoteGenbaneko", false);

            isSelect = false;
            val = Mathf.Clamp(val - (5.0f * Time.deltaTime), -1.5f, 1.5f);
            catMaterial.SetFloat("val", val);
        }


        if (prevType != characterType)
        {
            this.GetComponent<Animator>().SetBool("emoteGenbaneko", false);

            val = Mathf.Clamp(val - (5.0f * Time.deltaTime), -1.5f, 1.5f);
            catMaterial.SetFloat("val", val);
        }

            prevType = characterType;
    }

    public void Effect()
    {
        particle.GetComponent<ParticleSystem>().Play();
    }

    public void SetCharacterType(CharacterType type)
    {
        characterType = type;
    }
}
