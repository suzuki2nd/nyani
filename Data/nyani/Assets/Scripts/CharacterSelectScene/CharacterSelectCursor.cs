using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// キャラクタ選択カーソルクラス
/// </summary>
public class CharacterSelectCursor : CursorBase
{
    private bool isSelect;
    private bool transrate;

    private Color cursorColor;

    private GameObject target = null;
    private GameObject previewCat;

    new void Start()
    {
        base.Start();

        //初期化
        isSelect = false;
        transrate = false;

        //カーソルの位置調整
        PositionAdjustment();

        if (PlayHelper.Instance.GetEntryNum() <= gamePadNum)
        {
            this.gameObject.GetComponent<Image>().color = Color.white * 0.0f;
        }
    }

    new void Update()
    {
        base.CursorMove();

        //ゲームパッドのボタンが押されたときの処理
        GamePadButtonDown();
    }

    /// <summary>
    /// カーソルの位置を調整する
    /// </summary>
    private void PositionAdjustment()
    {
        //カーソルの位置を調整
        RectTransform rect;
        rect = this.GetComponent<RectTransform>();
        Vector2 halfSize = new Vector2(rect.sizeDelta.x / 2, rect.sizeDelta.y / 2);

        float width = 800.0f;

        //画面サイズによって位置が変わってしまうので、対処必要
        rightSideLimit = new Vector2(width / 2 - halfSize.x, -(225 - halfSize.y));

        //画面サイズによって位置が変わってしまうので、対処必要
        leftSideLimit = new Vector2(-(width / 2 - halfSize.x), 225 - halfSize.y);

        //初期位置は各画面の右下
        rect.localPosition = rightSideLimit;
    }

    /// <summary>
    /// ゲームパッドのボタンが押された際の処理
    /// </summary>
    protected override void GamePadButtonDown()
    {
        //ゲームパッドBボタンを押したとき
        if (MyInput.GetAButtonDown(gamePadNum))
        {
            base.GamePadButtonDown();

            //カーソルがアイコンの上にある場合
            //かつ、シーン遷移を行わない場合
            if (isSelect == true)
            {
                //最後に触れたボタンが押された処理を行う
                target.GetComponent<ImageButtonBase>().SetOutlineColor(cursorColor);
                target.GetComponent<ImageButtonBase>().OnGamePadButtonDown(gamePadNum);
                
                //DisebleMove();
                //SelectMotion();
            }
        }
    }

    public void SetPreviewCat(GameObject cat)
    {
        previewCat = cat;
    }

    public void EnableTransrate()
    {
        transrate = true;
    }

    public bool IsTransrate()
    {
        return transrate;
    }

    public void SelectMotion()
    {                
        if (previewCat == null) return;

        previewCat.GetComponent<Animator>().SetBool("emoteGenbaneko", true);
        previewCat.GetComponent<CharacterPreview>().Effect();
    }

    protected override void SetColor()
    {
        Material material = new Material(shader);
        
        Color[] colors = { Color.red, Color.blue, Color.yellow, Color.green };
        cursorColor = colors[gamePadNum];

        material.SetColor("nikukyu", cursorColor);
        this.GetComponent<Image>().material = material;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
    }

    /// <summary>
    /// 当たり続けている場合に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Cursor")) return;

        if (transrate == false)
        {
            //セレクトしている状態
            isSelect = true;

            //OnGamePadButtonDown を実行するために保持しておく
            target = collision.gameObject;
        }
    }

    /// <summary>
    /// 離れた瞬間に呼ばれる
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Cursor") || collision.gameObject.tag.Equals("Untagged")) return;
        
        target = null;

        isSelect = false;

        CharacterCollector collector = CharacterCollector.Instance;
        
        //選んだキャラクターを登録
        collector.SelectCharacter(gamePadNum,CharacterType.NONE);
        EnableMove();
    }
}

