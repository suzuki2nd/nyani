using UnityEngine;

public class WoolPreview : MonoBehaviour
{
    private float val;
    private Material material;

    void Start()
    {
        material = this.transform.Find("Character1_Reference").gameObject.GetComponent<Renderer>().material;
        material.SetFloat("val", -1.0f);
    }

    void Update()
    {
        CharacterCollector collecter = CharacterCollector.Instance;

        bool isExist = false;

        for (int index = 0;index < CharacterHelper.PLAYER_MAX; ++index)
        {
            CharacterType type = collecter.GetCharacter(index);

            if(type == CharacterType.BALL_OF_WOOL)
            {
                val = Mathf.Clamp(val + 5.0f * Time.deltaTime, -1.5f, 1.5f);

                material.SetFloat("val", val);

                isExist = true;
            }
        }


        if(isExist == false)
        {
            val = Mathf.Clamp(val - (5.0f * Time.deltaTime), -1.5f, 1.5f);

            material.SetFloat("val", val);
        }
    }
}
