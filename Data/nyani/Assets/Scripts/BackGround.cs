using UnityEngine;
using UnityEngine.UI;

public class BackGround : MonoBehaviour
{
    private float offset;
    private float val;

    void Start()
    {
        offset = 0.03f;
        val = 0.0f;
    }

    void Update()
    {
        val = Time.time * offset;

        Material material = this.GetComponent<Image>().material;
        material.SetFloat("_ScrollX", val);
        material.SetFloat("_ScrollY", -val);
    }
}
