using UnityEngine;

public class ActiveChanger : MonoBehaviour
{
    [SerializeField] private GameObject[] targetObj;

    public void ActiveChange(bool active, int num = 0)
    {
        targetObj[num].SetActive(active);
    }

    public void SetActiveTarget(int num ,GameObject obj)
    {
        targetObj[num] = obj;
    }
}