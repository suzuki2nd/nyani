using UnityEngine;

//カーソルを使用して押すボタンのベースクラス
public class ImageButtonBase : MonoBehaviour
{
    protected bool selectable;
    protected Color outlineColor;
    protected string seName = "ButtonDown";
    protected GameObject cursor;

    protected void Start()
    {
        selectable = true;

        string seName = "ButtonDown";
        SoundManager sound = SoundManager.Instance;
        sound.SetSE(seName, seName);

        cursor = null;
    }

    public virtual void OnGamePadButtonDown()
    {
        PlaySE(seName);
    }

    /// <summary>
    /// ゲームパッドの番号が必要な場合に呼ばれる
    /// </summary>
    /// <param name="num"></param>
    public virtual void OnGamePadButtonDown(int num)
    {
        PlaySE(seName);
    }

    private void PlaySE(string seName)
    {
        SoundManager.Instance.PlaySE(seName);
    }

    virtual protected void OnTriggerEnter2D(Collider2D collision)
    {
        cursor = collision.gameObject;
    }

    virtual protected void OnTriggerExit2D(Collider2D collision)
    {
        cursor = null;
    }
    public void SetOutlineColor(Color color)
    {
        outlineColor = color;
    }

    public void Selectable()
    {
        selectable = true;
    }

    public void Unselectable()
    {
        selectable = false;
    }
}
