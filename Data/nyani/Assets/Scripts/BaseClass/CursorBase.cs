using UnityEngine;
using UnityEngine.UI;
using System;

public class CursorBase : MonoBehaviour
{
    protected int gamePadNum;
    protected float cursorSpeed;
    protected bool isEffect;
    protected bool isMove;
    
    protected GameObject sceneAdmin;
    protected Shader shader;

    //右下
    protected Vector2 rightSideLimit;
    //左上
    protected Vector2 leftSideLimit;

    protected void Start()
    {
        //シーンアドミン取得
        sceneAdmin = GameObject.FindGameObjectWithTag("SceneAdmin").gameObject;

        ParticleManager manager = ParticleManager.Instance;
        manager.SetParticle("Cursor.prefab", "Cursor.prefab");

        isEffect = true;
        isMove = true;

        //カーソルの速度設定
        cursorSpeed = 10.0f;
    }

    protected void Update()
    {
    }

    /// <summary>
    /// カーソルの移動
    /// </summary>
    protected void CursorMove()
    {
        //動けない場合中止
        if (isMove == false) return;

        float horizontal = MyInput.GetLXAxis(gamePadNum) * cursorSpeed;
        float vertical = MyInput.GetLYAxis(gamePadNum) * cursorSpeed;

        //デバッグ用
        if (gamePadNum == 0)
        {
            //horizontal = Input.GetAxis("Horizontal") * cursorSpeed;
            //vertical = Input.GetAxis("Vertical") * cursorSpeed;
        }


        //現在位置取得
        RectTransform pos = this.GetComponent<RectTransform>();

        //各画面サイズから出ないか判定
        float x = Mathf.Clamp(pos.localPosition.x + horizontal, leftSideLimit.x ,rightSideLimit.x);
        float y = Mathf.Clamp(pos.localPosition.y + vertical, rightSideLimit.y, leftSideLimit.y);

        //位置更新
        pos.localPosition = new Vector2(x ,  y);
    }

    private Canvas m_canvas;

    /// <summary>
    /// 各カーソル 共通して行いたい動作を行う
    /// seなど
    /// </summary>
    protected virtual void GamePadButtonDown()
    {
        if (isEffect == false) return;

        ParticleManager manager = ParticleManager.Instance;

        m_canvas = this.transform.parent.gameObject.GetComponent<Canvas>();

        //キャンバス統一させなきゃできない
        RectTransform rect = this.GetComponent<RectTransform>();

        //UI座標からスクリーン座標に変換
        Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(m_canvas.worldCamera, rect.position);

        //ワールド座標
        Vector3 result = Vector3.zero;

        //スクリーン座標→ワールド座標に変換
        RectTransformUtility.ScreenPointToWorldPointInRectangle(rect, screenPos, m_canvas.worldCamera, out result);

        manager.InstantiateParticle("Cursor.prefab", result, Quaternion.identity);
    }


    protected virtual void SetColor()
    {
        this.GetComponent<Image>().material.SetColor("nikukyu", CharacterHelper.Instance.THEME_COLOR);
    }    

    public void SetShader(Shader sh)
    {
        shader = sh;
    }

    public void SetCursorSpeed(float speed)
    {
        cursorSpeed = speed;
    }

    /// <summary>
    /// どの番号のゲームパッドを使用するか設定する
    /// </summary>
    /// <param name="num"> 任意のゲームパッドの番号 </param>
    public void SetGamePadNumber(int num)
    {
        gamePadNum = num;
        SetColor();
    }

    public int GetGamePadNumber()
    {
        return gamePadNum;
    }

    public void EnableMove()
    {
        isMove = true;
    }

    public void DisebleMove()
    {
        isMove = false;
    }

}
