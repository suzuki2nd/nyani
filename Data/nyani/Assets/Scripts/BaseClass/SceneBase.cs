using UnityEngine;

public class SceneBase : MonoBehaviour
{
    protected bool canCountDown;
    [SerializeField] protected GameObject cursorObj;

    public float mixRate = 0;

    protected void Start()
    {
        SceneTimer.Reset();
        //フレームレート固定
        GameSetting.Instance.SetFrameRate();
        SoundManager.Instance.Start();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        this.gameObject.tag = "SceneAdmin";
    }

    protected void Update()
    {        
        SoundManager.Instance.Update(mixRate);
    }

    /// <summary>
    /// カーソルの生成
    /// </summary>
    /// <param name="num"> ゲームパッドの番号 </param>
    protected GameObject CursorGeneration<T>(int num, Vector2 size, GameObject parent = null , Shader shader = null) where T : CursorBase
    {
        //カーソル生成
        GameObject cursor = Instantiate(cursorObj, transform);

        //親を指定
        if (parent != null)
            cursor.transform.SetParent(parent.transform, false);

        RectTransform rect = cursor.GetComponent<RectTransform>();
        rect.sizeDelta = size;
        cursor.GetComponent<BoxCollider2D>().size = size;

        //CursorBase を継承したクラスを追加
        cursor.AddComponent<T>();

        if (shader != null)
        {
            cursor.GetComponent<CursorBase>().SetShader(shader);
        }

        //使用するゲームパッドの番号を指定
        cursor.GetComponent<CursorBase>().SetGamePadNumber(num);

        return cursor;
    }

    protected void OnDestroy()
    {
        //SoundManager.Instance.SetTime();
        //SoundManager.Instance.ReleaseSE();
        //SoundManager.Instance.ReleaseBgm();
    }
}
