using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Imageクラスのプロパティを変更しやすくした拡張メゾット
/// </summary>
public static class ImageExtensions
{
    /// <summary>
    /// アルファ値を設定
    /// </summary>
    /// <param name="image">アルファ値を変更したい画像</param>
    /// <param name="alpha">設定したいアルファ値</param>
    public static void SetAlpha(this Image image, float alpha)
    {
        var color = image.color;
        image.color = new Color(color.r, color.g, color.b, alpha);
    }

    public static Texture2D ToTexture2D(this Texture self)
    {
        var sw = self.width;
        var sh = self.height;
        var format = TextureFormat.RGBA32;
        var result = new Texture2D(sw, sh, format, false);
        var currentRT = RenderTexture.active;
        var rt = new RenderTexture(sw, sh, 32);
        Graphics.Blit(self, rt);
        RenderTexture.active = rt;
        var source = new Rect(0, 0, rt.width, rt.height);
        result.ReadPixels(source, 0, 0);
        result.Apply();
        RenderTexture.active = currentRT;
        return result;
    }
}
