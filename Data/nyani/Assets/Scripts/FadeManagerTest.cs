﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// シーン遷移時のフェードイン・アウトを制御するためのクラス .
/// </summary>
public class FadeManagerTest : MonoBehaviour
{

    #region Singleton

    private static FadeManagerTest instance;
    [SerializeField] private GameObject wipe;

    public static FadeManagerTest Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (FadeManagerTest)FindObjectOfType(typeof(FadeManagerTest));

                if (instance == null)
                {
                    Debug.LogError(typeof(FadeManagerTest) + "is nothing");
                }
            }

            return instance;
        }
    }

    #endregion Singleton

    /// <summary>
    /// デバッグモード .
    /// </summary>
    public bool DebugMode = true;
    /// <summary>フェード中の透明度</summary>
    private float fadeAlpha = 0;
    /// <summary>フェード中かどうか</summary>
    private bool isFading = false;
    /// <summary>フェード色</summary>
    public Color fadeColor = Color.black;


    public void Awake()
    {
        if (this != Instance)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public void OnGUI()
    {
        // Fade .
        if (this.isFading)
        {
            //色と透明度を更新して白テクスチャを描画 .
            this.fadeColor.a = this.fadeAlpha;
            GUI.color = this.fadeColor;
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
        }
    }

    /// <summary>
    /// 画面遷移 .
    /// </summary>
    /// <param name='scene'>シーン名</param>
    /// <param name='interval'>暗転にかかる時間(秒)</param>
    public void LoadScene(string scene, float interval , bool isWipe = false)
    {
        if (isWipe == true)
        {
            wipe.SetActive(true);
            StartCoroutine(WipeTransScene(scene, interval));
        }
        else
        {
            StartCoroutine(TransScene(scene, interval));
        }
    }

    public void InScene(float interval, bool isWipe = false)
    {
        if (isWipe == true)
        {
            wipe.SetActive(true);
            StartCoroutine(WipeFadeIn(interval));
        }
        else
        {
            StartCoroutine(FadeIn(interval));
        }
    }

    /// <summary>
    /// シーン遷移用コルーチン .
    /// </summary>
    /// <param name='scene'>シーン名</param>
    /// <param name='interval'>暗転にかかる時間(秒)</param>
    private IEnumerator WipeTransScene(string scene, float interval)
    {
        //だんだん暗く .
        this.isFading = true;
        float time = 0;
        float val = 2.0f;

        Material material = wipe.GetComponent<Image>().material;

        while (time <= interval)
        {
            val -= Time.deltaTime * 2;
            material.SetFloat("_Radius", val);

            time += Time.deltaTime;
            yield return 0;
        }

        //数秒の暗転処理を入れて
        //切り替わってる感を出す
        yield return new WaitForSeconds(1.0f);

        //シーン切替
        SceneManager.LoadScene(scene);
    }

    private IEnumerator WipeFadeIn(float interval)
    {
        //だんだん明るく .
        this.isFading = true;
        float time = 0;
        float val = 0.0f;

        Material material = wipe.GetComponent<Image>().material;
        material.SetFloat("_Radius", 0.0f);

        yield return new WaitForSeconds(0.5f);

        while (time <= interval)
        {
            val += Time.deltaTime;
            material.SetFloat("_Radius", val);

            time += Time.deltaTime;
            yield return 0;
        }

        this.isFading = false;
        wipe.SetActive(false);
    }


    /// <summary>
    /// シーン遷移用コルーチン .
    /// </summary>
    /// <param name='scene'>シーン名</param>
    /// <param name='interval'>暗転にかかる時間(秒)</param>
    private IEnumerator TransScene(string scene, float interval)
    {
        //だんだん暗く .
        this.isFading = true;
        float time = 0;
        while (time <= interval)
        {
            this.fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        //シーン切替 .
        SceneManager.LoadScene(scene);
    }
    private IEnumerator FadeIn(float interval)
    {
        //だんだん明るく .
        this.isFading = true;
        float time = 0;

        while (time <= interval)
        {
            this.fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
            time += Time.deltaTime;
            yield return 0;
        }

        this.isFading = false;
    }
}

