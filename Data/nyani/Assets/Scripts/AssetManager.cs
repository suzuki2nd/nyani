using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;

/// <summary>
/// アセットのロードや生成など、アセットに関する処理を管理するクラス
/// </summary>
public sealed class AssetManager : Singleton<AssetManager>
{
    private static readonly Regex REGEX = new Regex("Key=(.*), Type=(.*)");
    private AsyncOperationHandle<GameObject> handle;
    private AsyncOperationHandle<Texture> texHandle;

    private Dictionary<string, GameObject> LoadedGameObjects;
    private Dictionary<string, Texture> LoadedTextures;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public AssetManager()
    {
        LoadedGameObjects = new Dictionary<string, GameObject>();
        LoadedTextures = new Dictionary<string, Texture>();
        ResourceManager.ExceptionHandler = ErrorHandler;
    }

    /// <summary>
    /// GameObjectをロードする。
    /// </summary>
    /// <param name="address">ロードしたいアドレス</param>
    /// <returns>ロードしたGameObject</returns>
    public GameObject LoadGameObject(in string address, params Action[] continueMethod)
    {
        if (LoadedGameObjects.ContainsKey(address))
        {
            return LoadedGameObjects[address];
        }
        //GameObject obj = AddressablesManager.Instance.LoadAssets(address).Result;
        //LoadedGameObjects.Add(address, obj);
        //ContinueAction(continueMethod);
        //return obj;
        return null;
    }

    /// <summary>
    /// 指定したGameObjectを生成する。
    /// </summary>
    /// <param name="address">生成したいGameObjecのアドレス</param>
    /// <param name="position">生成時に指定したい座標</param>
    /// <param name="rotation">生成時に指定したい回転</param>
    /// <param name="parent">親</param>
    /// <returns>生成したGameObject</returns>
    public async Task<GameObject> InstantiateGameObject(string address, Vector3? position, Quaternion? rotation, Transform parent, params Action[] continueMethod)
    {
        Vector3 pos = position ?? Vector3.zero;
        Quaternion rot = rotation ?? Quaternion.identity;
        if(parent != null)
        {
            position = parent.position + position;
            rotation = parent.rotation * rotation;
        }
        //if (LoadedGameObjects.ContainsKey(address))
        //{
        //    return UnityEngine.Object.Instantiate(LoadedGameObjects[address], parent);
        //}
        handle = Addressables.InstantiateAsync(address, new InstantiationParameters(pos, rot, parent));
        await handle.Task;
        //LoadedGameObjects.Add(address, handle.Result);
        ContinueAction(continueMethod);
        return handle.Result;
    }

    /// <summary>
    /// 指定したGameObjectを生成する。
    /// </summary>
    /// <param name="address">生成したいGameObjecのアドレス</param>
    /// <param name="position">生成時に指定したい座標</param>
    /// <param name="rotation">生成時に指定したい回転</param>
    /// <returns>生成したGameObject</returns>
    public async Task<GameObject> InstantiateGameObject(string address, Vector3? position, Quaternion? rotation, params Action[] continueMethod)
    {
        return await InstantiateGameObject(address, position, rotation, null, continueMethod);
    }

    /// <summary>
    /// 指定したGameObjectを生成する。
    /// </summary>
    /// <param name="address">生成したいGameObjecのアドレス</param>
    /// <param name="position">生成時に指定したい座標</param>
    /// <returns>生成したGameObject</returns>
    public async Task<GameObject> InstantiateGameObject(string address, Vector3? position, params Action[] continueMethod)
    {
        return await InstantiateGameObject(address, position, null, continueMethod);
    }

    /// <summary>
    /// 指定したGameObjectを生成する。
    /// </summary>
    /// <param name="address">生成したいGameObjecのアドレス</param>
    /// <returns>生成したGameObject</returns>
    public async Task<GameObject> InstantiateGameObject(string address, params Action[] continueMethod)
    {
        return await InstantiateGameObject(address, null, null, null, continueMethod);
    }

    /// <summary>
    /// 指定したGameObjectを生成する
    /// </summary>
    /// <param name="address">生成したいGameObjectのアドレス</param>
    /// <param name="transform">生成時に指定するTransform</param>
    /// <param name="parent"></param>
    /// <returns>生成したGameObject</returns>
    public async Task<GameObject> InstantiateGameObject(string address, Transform transform, Transform parent = null, params Action[] continueMethod)
    {
        //if (LoadedGameObjects.ContainsKey(address))
        //{
        //    return UnityEngine.Object.Instantiate(LoadedGameObjects[address], transform, parent);
        //}
        handle = Addressables.InstantiateAsync(address, new InstantiationParameters(transform.position, transform.rotation, parent));
        await handle.Task;
        //LoadedGameObjects.Add(address, handle.Result);
        ContinueAction(continueMethod);
        return handle.Result;
    }

    /// <summary>
    /// 指定したGameObjectを親のTransformを使用して生成する。
    /// </summary>
    /// <remarks>
    /// 親を指定しない場合、ワールド座標系の原点に、無回転で生成する。
    /// </remarks>
    /// <param name="address"></param>
    /// <param name="parent"></param>
    /// <returns>生成したGameObject</returns>
    public async Task<GameObject> InstantiateGameObject(string address, Transform parent = null, params Action[] continueMethod)
    {
        Vector3 pos = parent != null ? parent.position : Vector3.zero;
        Quaternion rotation = parent != null ? parent.rotation : Quaternion.identity;
        //if (LoadedGameObjects.ContainsKey(address))
        //{
        //    return UnityEngine.Object.Instantiate(LoadedGameObjects[address], pos, rotation, parent);
        //}

        handle = Addressables.InstantiateAsync(address, new InstantiationParameters(pos, rotation, parent));
        await handle.Task;
        //LoadedGameObjects.Add(address, handle.Result);
        ContinueAction(continueMethod);
        return handle.Result;
    }

    /// <summary>
    /// 指定したアドレスのテクスチャをロード
    /// </summary>
    /// <param name="address">ロードしたいテクスチャのアドレス</param>
    /// <returns>ロードしたテクスチャ</returns>
    public async Task<Texture> LoadTexture(string address)
    {
        texHandle = Addressables.LoadAssetAsync<Texture>(address);

        await texHandle.Task;
        if(LoadedGameObjects.ContainsKey(address))
        {
            return LoadedTextures[address];
        }
        return texHandle.Result;
    }

    /// <summary>
    /// 呼び出し直前のタスクに続けて実行したいタスクがあった場合に、順番に処理する
    /// </summary>
    /// <param name="continueActions">後続して実行したい任意の数の関数</param>
    private async void ContinueAction(params Action[] continueActions)
    {
        if (continueActions.Length <= 0)
        {
            return;
        }
        for (int i = 0; i < continueActions.Length; ++i)
        {
            await handle.Task.ContinueWith(task => continueActions[i].Invoke());
        }
    }

    /// <summary>
    /// オブジェクトの参照カウンタを-1してリリースする。
    /// </summary>
    /// <remarks>
    /// Addressables.InstantiateAsyncを使ってロードしたオブジェクトの寿命は所属するシーンに属するが、
    /// それ以外の任意のタイミングでオブジェクトをリリースしたい場合にこの関数を使用する。
    /// </remarks>
    /// <param name="instance"></param>
    public void ReleaseGameObject(GameObject instance)
    {
        //if (instance == null)
        //{
        //    return;
        //}
        //LoadedGameObjects.Remove(LoadedGameObjects.FirstOrDefault(i => i.Value == instance).Key);
        Addressables.ReleaseInstance(instance);
    }

    /// <summary>
    /// テクスチャをリリースする
    /// </summary>
    /// <param name="texture">リリースしたいテクスチャ</param>
    public void ReleaseTexture(Texture texture)
    {
        Addressables.Release(texture);
        LoadedTextures.Remove(LoadedTextures.FirstOrDefault(i => i.Value == texture).Key);
    }

    /// <summary>
    /// ロード済みのアセットをすべて開放
    /// </summary>
    private void ReleaseAllInstance()
    {
        foreach(var i in LoadedTextures)
        {
            ReleaseTexture(i.Value);
            LoadedTextures.Remove(i.Key);
        }
        foreach(var i in LoadedGameObjects)
        {
            ReleaseGameObject(i.Value);
            LoadedGameObjects.Remove(i.Key);
        }
    }

    /// <summary>
    /// 次のシーンをロードする。
    /// Addressables.LoadAssetAsyncは自分で開放しないとメモリーリークになるため、
    /// この関数で開放処理をしてから安全に次のシーンをロードする。
    /// </summary>
    /// <param name="sceneName">ロードするシーンの名前</param>
    /// <param name="mode">シーンをどのようにロードするか。基本はsingleで良い。</param>
    public void SafeLoadScene(in string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
    {
        ReleaseAllInstance();
        SceneManager.LoadScene(sceneName, mode);
    }

    /// <summary>
    /// エラー補足時に呼ばせるためのコールバック関数
    /// エラーメッセージを見やすくするための機能
    /// </summary>
    /// <param name="handle">ハンドル</param>
    /// <param name="exception">補足された例外</param>
    private void ErrorHandler(AsyncOperationHandle handle, Exception exception)
    {
        string message = exception.Message;
        //アドレスが間違っているわけではなかった
        if (message.Contains("'UnityEngine.AddressableAssets.InvalidKeyException'") == false)
        {
            Debug.LogError(message);
            return;
        }

        Match match = REGEX.Match(message);
        //アドレスと型がエラーメッセージに含まれていない
        if (match.Success == false)
        {
            Debug.LogError(message);
            return;
        }

        GroupCollection groups = match.Groups;
        string key = groups[1].Value;
        string type = groups[2].Value;
        Debug.LogError("Asset Loading Error : " + $"{key} が見つかりませんでした。（{type} 型）");
    }
}