
public class Singleton<T> where T : class, new()
{
    protected Singleton()
    {
    }

    private static readonly T instance = new T();

    public static T Instance
    {
        get
        {
            return instance;
        }
    }
}