using System.Collections.Generic;

//ステージの種類
public enum StageType
{
    FIRST = 0,
    SECOND,
    THIRD,
    MAX,
    NONE
}

public class StageHelper : Singleton<StageHelper>
{
    private readonly StageType[] stageTypes = new StageType[(int)StageType.MAX]
    {
        StageType.FIRST,StageType.SECOND,StageType.THIRD,
    };

    private readonly string[] stageNames = new string[(int)StageType.MAX]
    {
        "FirstStage","SecondStage","ThirdStage",
    };

    private Dictionary<string, StageType> stageTypeDictionary = new Dictionary<string, StageType>
    {
        { "FirstStage",StageType.FIRST },
        { "SecondStage",StageType.SECOND },
        { "ThirdStage",StageType.THIRD },
    };

    public StageType GetType(int num)
    {
        return stageTypes[num];
    }

    public StageType GetType(string name)
    {
        return stageTypeDictionary[name];
    }

    public string GetName(int num)
    {
        return stageNames[num];
    }
}
