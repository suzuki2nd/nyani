using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : Singleton<ParticleManager>
{
    Dictionary<string, PatricleInfo> particles = new Dictionary<string, PatricleInfo>();

    class PatricleInfo
    {
        public string resourceName;
        public GameObject particle;

        public PatricleInfo(string resourceName)
        {
            this.resourceName = resourceName;
        }
    }

    public void SetParticle(string key, string resourceName)
    {
        //重複している場合
        if (particles.ContainsKey(key))
        {
            return;
        }

        particles.Add(key, new PatricleInfo(resourceName));
        Loadparticle(key);
    }

    private async void Loadparticle(string key)
    {
        AddressablesManager am = new AddressablesManager();
        particles[key].particle = await am.LoadAsset(key);
    }

    public GameObject GetParticle(string name)
    {
        return particles[name].particle;
    }

    public GameObject InstantiateParticle(string name ,Vector3 position , Quaternion quaternion , float destroyTime = 2.0f)
    {
        GameObject particle = MonoBehaviour.Instantiate(GetParticle(name), position, quaternion);
        particle.AddComponent<ParticleDestroyser>();
        particle.GetComponent<ParticleDestroyser>().SetDestroyTime(destroyTime);

        return particle;
    }

    public GameObject InstantiateParticle(string name , float destroyTime = 2.0f)
    {
        GameObject particle = MonoBehaviour.Instantiate(GetParticle(name));
        particle.AddComponent<ParticleDestroyser>();
        particle.GetComponent<ParticleDestroyser>().SetDestroyTime(destroyTime);

        return particle;
    }
}
