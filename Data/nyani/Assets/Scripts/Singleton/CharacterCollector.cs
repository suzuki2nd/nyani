using UnityEngine;
using System.Linq;

public class CharacterCollector : Singleton<CharacterCollector>
{
    private CharacterType[] selectCharacterTypes = new CharacterType[CharacterHelper.PLAYER_MAX]
     {
        CharacterType.CAT_FIRST,CharacterType.CAT_SECOND,CharacterType.CAT_THIRD,CharacterType.BALL_OF_WOOL,
     };

    //private CharacterType[] selectCharacterTypes = new CharacterType[CharacterHelper.PLAYER_MAX]
    //{
    //    CharacterType.NONE,CharacterType.NONE,CharacterType.NONE,CharacterType.NONE,
    //};

    private GameObject[] chars = new GameObject[CharacterHelper.PLAYER_MAX];
    private int selectCharacterNum;

    public void Start()
    {
        CharacterHelper helper = CharacterHelper.Instance;

        for (int index = 0; index < selectCharacterTypes.Length; ++index)
        {
            //selectCharacterTypes[index] = helper.GetType(index);
            selectCharacterTypes[index] = CharacterType.NONE;
        }
    }

    public void SetCaracter(int num, GameObject chara)
    {
        chars[num] = chara;
    }

    public GameObject GetCaracterObj(int num)
    {
        return chars[num];
    }
    /// <summary>
    /// 選択されたキャラクタタイプを登録する
    /// </summary>
    /// <param name="num"> 任意の数字（ゲームパッド番号） </param>
    /// <param name="type"> 選んだキャラクタタイプ </param>
    public void SelectCharacter(int num, CharacterType type)
    {
        selectCharacterTypes[num] = type;
    }


    /// <summary>
    /// 選択されたキャラクター数をセット
    /// </summary>
    /// <param name="num">コントローラの数</param>
    public void SelectCharacterNum(int num)
    {
        selectCharacterNum = num;
    }
    /// <summary>
    /// 選択されたキャラクター数をゲット
    /// </summary>
    /// <returns>選択されたキャラクター(コントローラー数)</returns>
    public int GetCharacterNum()
    {
        return selectCharacterNum;
    }

    /// <summary>
    /// 引数番目のキャラクターがどのキャラクタータイプを使用するか返す
    /// </summary>
    /// <param name="num"> 任意の数字（ゲームパッド番号） </param>
    /// <returns> キャラクタータイプ </returns>
    public CharacterType GetCharacter(int num)
    {
        return selectCharacterTypes[num];
    }

    public bool IsValidCharacter()
    {
        //Distinct()：selectCharacterTypes の中身で一意のものだけ抜きだす
        //抜き出したものが プレイヤーの数分あれば true
        CharacterType[] types = selectCharacterTypes.Distinct().ToArray();
        bool selectedBall = false;

        //選択された中に 毛玉が含まれているか確認
        foreach (CharacterType type in types)
        {
            //毛玉が含まれている
            if (type == CharacterType.BALL_OF_WOOL)
            {
                selectedBall = true;
            }
        }

        //毛玉が含まれているかつ、
        //抜き出したものが プレイヤーの数分あれば true
        return selectedBall == true && types.Length == PlayHelper.Instance.GetEntryNum();
    }

    /// <summary>
    /// 引数のキャラクタータイプがどのゲームパッドを使用しているか取得する
    /// </summary>
    /// <param name="type"> 任意のキャラクタータイプ </param>
    /// <returns> 引数のキャラクタータイプを使用してるゲームパッドの番号 </returns>
    public int FindByGamePad(CharacterType type)
    {
        for (int index = 0; index < (int)CharacterType.MAX; index++)
        {
            if (type == selectCharacterTypes[index])
            {
                return index;
            }
        }

        //見つからなかった
        return -1;
    }

    public int FindByPad(CharacterType type)
    {
        for (int index = 0; index < CharacterHelper.PLAYER_MAX; index++)
        {
            if (type == selectCharacterTypes[index])
            {
                return index;
            }
        }

        //見つからなかった
        return -1;
    }
}