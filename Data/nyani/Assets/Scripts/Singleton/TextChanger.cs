using UnityEngine;
using UnityEngine.UI;

public class TextChanger : Singleton<TextChanger>
{
    public void RefleshText(string target , string text)
    {
        GameObject.FindGameObjectWithTag(target).GetComponent<Text>().text = text;
    }
}