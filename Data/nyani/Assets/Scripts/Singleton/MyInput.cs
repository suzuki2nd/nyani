using UnityEngine;

public static class MyInput
{
    static int keyNum = 0;

    public static bool GetAnyKey(int num)
    {
        return GetAButtonDown(num) || GetBButtonDown(num) || GetXButtonDown(num) || GetYButtonDown(num) || GetRSBButtonDown(num) || GetLSBButtonDown(num) || GetLBButtonDown(num) || GetRBButtonDown(num);
    }

    public static float GetLXAxis(int num)
    {
        return Input.GetAxis("GamePadLX_" + num.ToString());
    }

    public static float GetLYAxis(int num)
    {
        return Input.GetAxis("GamePadLY_" + num.ToString());
    }

    public static float GetRXAxis(int num)
    {
        return Input.GetAxis("GamePadRX_" + num.ToString());
    }

    public static float GetRYAxis(int num)
    {
        return Input.GetAxis("GamePadRY_" + num.ToString());
    }

    public static bool GetAButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadA_" + num.ToString()) || (Input.GetKeyDown(KeyCode.Z) && num == keyNum);
    }

    public static bool GetAButtonUp(int num)
    {
        return Input.GetButtonUp("GamePadA_" + num.ToString()) || (Input.GetKeyDown(KeyCode.Z) && num == keyNum);
    }

    public static bool GetBButton(int num)
    {
        return Input.GetButton("GamePadB_" + num.ToString()) || (Input.GetKey(KeyCode.X) && num == keyNum);
    }

    public static bool GetBButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadB_" + num.ToString()) || (Input.GetKeyDown(KeyCode.X) && num == keyNum);
    }

    public static bool GetBButtonUp(int num)
    {
        return Input.GetButtonUp("GamePadB_" + num.ToString()) || (Input.GetKeyUp(KeyCode.X) && num == keyNum);
    }

    public static bool GetXButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadX_" + num.ToString()) || (Input.GetKeyDown(KeyCode.C) && num == keyNum);
    }

    public static bool GetYButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadY_" + num.ToString()) || (Input.GetKeyDown(KeyCode.V) && num == keyNum);
    }

    public static bool GetYButtonUp(int num)
    {
        return Input.GetButtonUp("GamePadY_" + num.ToString()) || (Input.GetKeyUp(KeyCode.V) && num == keyNum);
    }

    public static bool GetLBButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadLB_" + num.ToString()) || (Input.GetKeyDown(KeyCode.B) && num == keyNum);
    }

    public static bool GetRBButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadRB_" + num.ToString()) || (Input.GetKeyDown(KeyCode.N) && num == keyNum);
    }

    public static bool GetRBButtonUp(int num)
    {
        return Input.GetButtonUp("GamePadRB_" + num.ToString()) || (Input.GetKeyDown(KeyCode.N) && num == keyNum);
    }

    public static bool GetLSBButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadLSB_" + num.ToString()) || (Input.GetKeyDown(KeyCode.M) && num == keyNum);
    }

    public static bool GetRSBButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadRSB_" + num.ToString()) || (Input.GetKeyDown(KeyCode.L) && num == keyNum);
    }


    public static bool GetStartButtonDown(int num)
    {
        return Input.GetButtonDown("GamePadStart_" + num.ToString()) || (Input.GetKeyDown(KeyCode.K) && num == keyNum); ;
    }
}
