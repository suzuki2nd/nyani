using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    enum BgmType
    {
        NORMAL = 0,
        OWNER,
        MAX
    };

    private static float time;
    private GameObject se;
    private GameObject bgm;

    private AudioSource seSource;
    private AudioSource[] bgmSource;

    Dictionary<string, AudioClipInfo> seClips = new Dictionary<string, AudioClipInfo>();
    Dictionary<string, AudioClipInfo> bgmClips = new Dictionary<string, AudioClipInfo>();

    class AudioClipInfo
    {
        public string resourceName;
        public AudioClip clip;

        //コンストラクタthis
        public AudioClipInfo(string resourceName)
        {
            this.resourceName = resourceName;
        }
    }

    public void Start()
    {
        bgmSource = new AudioSource[(int)BgmType.MAX];

        se = new GameObject();
        seSource = se.AddComponent<AudioSource>();
        se.name = "se";

        bgm = new GameObject();
        bgmSource[(int)BgmType.NORMAL] = bgm.AddComponent<AudioSource>();
        bgmSource[(int)BgmType.OWNER] = bgm.AddComponent<AudioSource>();
        bgm.name = "bgm";
    }

    public void SetSE(string key, string resourceName)
    {
        if (seClips.ContainsKey(key) == true)
            return;

        seClips.Add(key, new AudioClipInfo(resourceName));
        LoadSE(key);
    }

    private async void LoadSE(string key)
    {
        string EXTENSION = ".mp3";

        AddressablesManager am = new AddressablesManager();
        var bgm = am.LoadAudioClipAsset(key + EXTENSION);
        await bgm;

        seClips[key].clip = bgm.Result;
    }

    public void ReleaseSE()
    {
        AddressablesManager am = new AddressablesManager();
//        if (seClips.Count == 0) return;

        foreach (string key in seClips.Keys)
        {
            am.ReleaseAudioClipAssets(seClips[key].clip);
        }
    }

    public bool PlaySE(string bgmName)
    {
        //その音源が存在するかチェック
        if (seClips.ContainsKey(bgmName) == false)
            return false;

        AudioClipInfo info = seClips[bgmName];
        seSource.PlayOneShot(info.clip);

        return true;
    }

    public bool OnlyPlaySE(string bgmName)
    {
        if (seSource.isPlaying == true) return false;

        //その音源が存在するかチェック
        if (seClips.ContainsKey(bgmName) == false)
            return false;

        AudioClipInfo info = seClips[bgmName];
        seSource.PlayOneShot(info.clip);

        return true;
    }


    public bool StopSE(string seName)
    {
        //その音源が存在するかチェック
        if (seClips.ContainsKey(seName) == false)
            return false;

        seSource.Stop();
        seSource.clip = null;

        return true;
    }


    public void SetBgm(string key, string resourceName)
    {
        if (bgmClips.ContainsKey(key) == true)
            return;

        bgmClips.Add(key, new AudioClipInfo(resourceName));
        LoadBgm(key);
    }

    private async void LoadBgm(string key)
    {
        string EXTENSION = ".mp3";

        AddressablesManager am = new AddressablesManager();
        var bgm = am.LoadAudioClipAsset(key + EXTENSION);
        await bgm;

        bgmClips[key].clip = bgm.Result;
    }

    public void ReleaseBgm()
    {
        AddressablesManager am = new AddressablesManager();

    //    if (bgmClips.Count == 0) return;

        foreach (string key in bgmClips.Keys)
        {
            am.ReleaseAudioClipAssets(bgmClips[key].clip);
        }
    }


    public bool PlayBGM(string bgmName)
    {
        if (bgmSource[(int)BgmType.NORMAL].isPlaying == true) return false;

        //その音源が存在するかチェック
        if (bgmClips.ContainsKey(bgmName) == false)
            return false;

        AudioClipInfo info = bgmClips[bgmName];

        //bgmSource[(int)BgmType.NORMAL].time = 10.0f;

        bgmSource[(int)BgmType.NORMAL].playOnAwake = false;
        bgmSource[(int)BgmType.NORMAL].loop = true;
        bgmSource[(int)BgmType.NORMAL].clip = info.clip;
        bgmSource[(int)BgmType.NORMAL].Play();

        return true;
    }

    public bool StopBGM(string bgmName)
    {
        //その音源が存在するかチェック
        if (bgmClips.ContainsKey(bgmName) == false)
            return false;

        bgmSource[(int)BgmType.NORMAL].Stop();
        bgmSource[(int)BgmType.NORMAL].clip = null;

        return true;
    }

    public bool PlayOwnerBGM(string bgmName)
    {
        if (bgmSource[(int)BgmType.OWNER].isPlaying == true) return false;

        //その音源が存在するかチェック
        if (bgmClips.ContainsKey(bgmName) == false)
            return false;

        AudioClipInfo info = bgmClips[bgmName];

        bgmSource[(int)BgmType.OWNER].loop = true;
        bgmSource[(int)BgmType.OWNER].clip = info.clip;
        bgmSource[(int)BgmType.OWNER].Play();

        return true;
    }

    public bool StopOwnerBGM(string bgmName)
    {
        //その音源が存在するかチェック
        if (bgmClips.ContainsKey(bgmName) == false)
            return false;

        bgmSource[(int)BgmType.OWNER].Stop();
        bgmSource[(int)BgmType.OWNER].clip = null;

        return true;
    }

    public void SetTime()
    {
        time = bgmSource[(int)BgmType.NORMAL].time;
    }

    public void Update(float rate)
    {
        if (bgmSource[(int)BgmType.NORMAL] == null || bgmSource[(int)BgmType.OWNER] == null) return;

        if (bgmSource[(int)BgmType.NORMAL].isPlaying == true)
        {
            bgmSource[(int)BgmType.NORMAL].volume = 1.0f - rate;
        }

        if (bgmSource[(int)BgmType.OWNER].isPlaying == true)
        {
            bgmSource[(int)BgmType.OWNER].volume = rate;
        }

        //SetTime();
    }
}
