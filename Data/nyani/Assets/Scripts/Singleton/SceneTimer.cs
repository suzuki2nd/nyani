using UnityEngine;

/// <summary>
/// シーンが始まってからの時間を返します
/// </summary>
public static class SceneTimer
{
    static float startTime;

    public static void Reset()
    {
        startTime = Time.time;
    }

    public static float GetTime()
    {
        return Time.time - startTime;
    }
}
