
public class PlayHelper : Singleton<PlayHelper>
{
    private bool isStart;
    private bool isEndGame;
    private short entryNum= 1;

    public void Start()
    {
        isStart = false;
        isEndGame = false;
    }
    
    public void StartPlayScene()
    {
        isStart = true;
    }

    public void EndPlayScene()
    {
        isStart = false;
    }

    public bool IsStart()
    {
        return isStart;
    }

    public void EndGame()
    {
        isEndGame = true; ;
    }

    public bool IsEndGame()
    {
        return isEndGame;
    }

    public void SetEntryNum(short num)
    {
        entryNum = num;
    }

    public short GetEntryNum()
    {
        return entryNum;
    }
}
