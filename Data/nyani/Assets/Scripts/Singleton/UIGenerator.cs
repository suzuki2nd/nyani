using UnityEngine;
using UnityEngine.UI;

public class UIGenerator 
{
    public static GameObject GenerateImage(Sprite sprite , Transform parent , Vector3 position , Vector3 scale , Vector2 sizeDelta)
    {
        GameObject ui = new GameObject();
        ui.transform.parent = parent.transform;
        ui.AddComponent<Image>();
        ui.GetComponent<Image>().sprite = sprite;

        RectTransform rectTransform = ui.GetComponent<RectTransform>();
        rectTransform.localPosition = position;
        rectTransform.localScale = scale;
        rectTransform.sizeDelta = sizeDelta;

        return ui;
    }

    public static GameObject GenerateImage(Sprite sprite, Transform parent, Vector3 position, Vector3 scale)
    {
        return GenerateImage(sprite, parent, position, scale , Vector2.one * 100.0f);
    }

    public static GameObject GenerateImage(Sprite sprite, Transform parent, Vector3 position)
    {
        return GenerateImage(sprite, parent, position, Vector3.one, Vector2.one * 100.0f);
    }

    public static GameObject GenerateImage(Sprite sprite, Transform parent)
    {
        return GenerateImage(sprite, parent, Vector3.zero, Vector3.one, Vector2.one * 100.0f);
    }
}
