
public class StageCollector : Singleton<StageCollector>
{
    private StageType selectStage = StageType.THIRD;

    public void Start()
    {
        selectStage = StageType.NONE;
    }

    public void SelectStage(StageType stageType)
    {
        selectStage = stageType;
    }

    public StageType GetStageType()
    {
        return selectStage;
    }
}
