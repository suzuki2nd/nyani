using UnityEngine;

public class ViewPortFormatter : Singleton<ViewPortFormatter>
{
    //カメラのビューポートの値をまとめた構造体
    public struct ViewPort
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="x">ビューポート短形：x</param>
        /// <param name="y">ビューポート短形：y</param>
        /// <param name="w">ビューポート短形：w</param>
        /// <param name="h">ビューポート短形：h</param>
        public ViewPort(float x, float y, float w, float h)
        {
            position.x = x;
            position.y = y;
            size.x = w;
            size.y = h;
        }

        public Vector2 position;
        public Vector2 size;
    }

    private ViewPort[] viewPorts = new ViewPort[CharacterHelper.PLAYER_MAX]
    {
            new ViewPort(0.0f,0.5f,0.5f,0.5f),
            new ViewPort(0.5f,0.5f,0.5f,0.5f),
            new ViewPort(0.0f,0.0f,0.5f,0.5f),
            new ViewPort(0.5f,0.0f,0.5f,0.5f)
    };

    //    private ViewPort[] viewPorts = new ViewPort[CharacterHelper.PLAYER_MAX]
    //{
    //        new ViewPort(0,0,1,1),
    //            new ViewPort(0.0f,0.0f,0.0f,0.0f),
    //            new ViewPort(0.0f,0.0f,0.0f,0.0f),
    //            new ViewPort(0.0f,0.0f,0.0f,0.0f)
    //};

    private Vector2[] screenCenter = new Vector2[CharacterHelper.PLAYER_MAX]
    {
        new Vector2(-225.0f,125.0f),
        new Vector2(225,125.0f),
        new Vector2(-225.0f,-125.0f),
        new Vector2(225.0f,-125.0f),
    };

    /// <summary>
    /// ビューポートの位置,サイズを Rect で出力
    /// </summary>
    /// <param name="index"> カメラの番号 </param>
    /// <returns>ビューポートの位置,サイズを Rect 型にしたもの</returns>
    public Rect GetViewPortRect(int index)
    {
        return new Rect(viewPorts[index].position.x, viewPorts[index].position.y, viewPorts[index].size.x, viewPorts[index].size.y);
    }

    public Vector2 GetScreenCenter(int index)
    {
        return screenCenter[index];
    }

    /// <summary>
    /// 4分割された状態でカメラを生成
    /// </summary>
    /// <param name="cameraObj"></param>
    /// <param name="transform"></param>
    public void InstantiateFourDivisionCamera(GameObject cameraObj, Transform transform)
    {
        //カメラの生成
        for (int index = 0; index < CharacterHelper.PLAYER_MAX; ++index)
        {
            //GameObject cam =MonoBehaviour.Instantiate((GameObject)Resources.Load("Camera"), transform);
            GameObject cam = MonoBehaviour.Instantiate(cameraObj, transform);
            Camera camera = cam.GetComponent<Camera>();
            camera.rect = GetViewPortRect(index);
        }
    }
}
