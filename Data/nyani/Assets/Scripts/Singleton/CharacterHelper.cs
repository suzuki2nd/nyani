using System.Collections.Generic;
using UnityEngine;

//キャラクターベースを継承したクラスの種類
public enum CharacterType
{
    CAT_FIRST = 0,
    CAT_SECOND,
    CAT_THIRD,
    CAT_FOURTH,
    BALL_OF_WOOL,
    MAX,
    NONE
}

public enum CatType
{
    FIRST = 0,
    SECOND,
    THIRD,
    FOURTH,
    MAX
}

public class CharacterHelper : Singleton<CharacterHelper>
{
    public const int PLAYER_MAX = 4;
    public readonly Color THEME_COLOR = new Color(0.9607844f, 0.7137255f, 0.654902f, 1.0f);

    Texture[] mainTextures = new Texture[(int)CatType.MAX];
    Texture[] shadowTextures = new Texture[(int)CatType.MAX];
    AddressablesManager am;

    //
    private readonly CharacterType[] characterTypes = new CharacterType[(int)CharacterType.MAX]
    {
        CharacterType.CAT_FIRST,CharacterType.CAT_SECOND,CharacterType.CAT_THIRD , CharacterType.CAT_FOURTH ,CharacterType.BALL_OF_WOOL
    };

    private readonly string[] characterNames = new string[(int)CharacterType.MAX]
    {
        "FirstCat","SecondCat","ThirdCat", "FourthCat" ,"BallOfWool",
    };

    private Dictionary<string, CharacterType> characterTypeDictionary = new Dictionary<string, CharacterType>
    {
        { "FirstCat",CharacterType.CAT_FIRST },
        { "SecondCat",CharacterType.CAT_SECOND },
        { "ThirdCat",CharacterType.CAT_THIRD },
        { "FourthCat",CharacterType.CAT_FOURTH },
        { "BallOfWool",CharacterType.BALL_OF_WOOL },
    };


    public void Start()
    {
        am = new AddressablesManager();
        SetTex();
    }

    /// <summary>
    /// キャラクタのタグに対応するキャラクタタイプを取得する
    /// </summary>
    /// <param name="name"> 任意の名前 </param>
    /// <returns> キャラクタタイプ </returns>
    public CharacterType GetType(string name)
    {
        return characterTypeDictionary[name];
    }
    /// <summary>
    ///　添え字に対応するキャラクタタイプを取得する
    /// </summary>
    /// <param name="num">任意の添え字</param>
    /// <returns>キャラクタタイプ</returns>
    public CharacterType GetType(int num)
    {
        return characterTypes[num];
    }

    /// <summary>
    /// キャラクタタイプに対応する名前を取得する
    /// </summary>
    /// <param name="num"> 任意のキャラクタタイプの int型 </param>
    /// <returns> キャラクタの名前 </returns>
    public string GetName(int num)
    {
        return characterNames[num];
    }

    /// <summary>
    /// メインのテクスチャのセット
    /// </summary>
    /// <param name="num"> 任意のキャラクタタイプの int型 </param>
    /// <param name="tex"> テクスチャ </param>
    public void SetMainTex(int num, Texture tex)
    {
        mainTextures[num] = tex;
    }

    /// <summary>
    /// 影のテクスチャのセット
    /// </summary>
    /// <param name="num"> 任意のキャラクタタイプの int型 </param>
    /// <param name="tex"> テクスチャ </param>
    public void SetShadowTex(int num , Texture tex)
    {
        shadowTextures[num] = tex;
    }

    /// <summary>
    /// メインテクスチャの取得
    /// </summary>
    /// <param name="num"> 任意のキャラクタタイプの int型 </param>
    /// <returns> テクスチャ </returns>
    public Texture GetMainTex(int num)
    {
        return mainTextures[num];
    }

    /// <summary>
    /// 影のテクスチャの取得
    /// </summary>
    /// <param name="num"> 任意のキャラクタタイプの int型 </param>
    /// <returns> テクスチャ </returns>
    public Texture GetShadowTex(int num)
    {
        return shadowTextures[num];
    }

    private async void SetTex()
    {
        string EXTENSION = ".png";

        for (int index = 0; index < (int)CharacterType.MAX - 1; ++index)
        {
            var main = am.LoadTextureAsset(GetName(index) + EXTENSION);
            await main;

            mainTextures[index] = main.Result;
        }

        ReleaseTex();
    }

    public void ReleaseTex()
    {
        for (int index = 0; index < (int)CharacterType.MAX - 1; ++index)
        {
            am.ReleaseTextureAssets(mainTextures[index]);
        }
    }
}
