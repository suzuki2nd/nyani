using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    /// <summary>
    /// オブジェクトのアクティブ・非アクティブに関わらず実行
    /// </summary>
    private void Awake()
    {
        //60fpsに設定
        Application.targetFrameRate = 60;
    }
}

/// <summary>
/// フレームレート固定シングルトンクラス
/// </summary>
public class GameSetting : Singleton<GameSetting>
{
    /// <summary>
    /// フレームレート固定
    /// 一回呼ぶとシーンをまたいでも固定されてるっぽい
    /// </summary>
    /// <param name="fps"> 固定したいfps値 </param>
    public void SetFrameRate(int fps = 60)
    {
        Application.targetFrameRate = fps;
    }
}