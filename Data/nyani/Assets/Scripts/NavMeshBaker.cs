using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshBaker : MonoBehaviour
{
    [SerializeField] private NavMeshSurface BallOfWoolNavMesh = null;
    [SerializeField] private NavMeshSurface BipedalCatNavMesh = null;
    [SerializeField] private NavMeshSurface QuadrupedalCatNavMesh = null;
    
    private NavMeshSurface[] NavMeshSurfaces = new NavMeshSurface[3];

    public Dictionary<string, int> NavMeshAgentTypes { get; private set; }

    /// <summary>
    /// 初期化
    /// </summary>
    void Start()
    {
        NavMeshSurfaces[0] = BallOfWoolNavMesh;
        NavMeshSurfaces[1] = BipedalCatNavMesh;
        NavMeshSurfaces[2] = QuadrupedalCatNavMesh;

        foreach(NavMeshSurface nms in NavMeshSurfaces)
        {
            nms.BuildNavMesh();
            Debug.Log("bake");
        }

        NavMeshAgentTypes = new Dictionary<string, int>();
        StoreNavMeshAgentTypes();
    }

    /// <summary>
    /// 指定されたタイプのAgentのNavMeshをベイクする
    /// </summary>
    /// <param name="agentTypeID">ベイクするAgentのタイプ</param>
    public void BakeByAgentTypeID(int agentTypeID)
    {
        foreach (NavMeshSurface nms in NavMeshSurfaces)
        {
            if (agentTypeID == nms.agentTypeID)
            {
                Debug.Log("bake by agentTypeID");
                nms.BuildNavMesh();
            }
        }
    }

    /// <summary>
    /// Agentのタイプを、タイプ名とIDで紐づけて登録
    /// </summary>
    private void StoreNavMeshAgentTypes()
    {
        for(int i = 0; i < NavMesh.GetSettingsCount(); ++i)
        {
            int id = NavMesh.GetSettingsByIndex(i).agentTypeID;
            string typeName = NavMesh.GetSettingsNameFromID(id);
            NavMeshAgentTypes.Add(typeName, id);
        }
    }
}