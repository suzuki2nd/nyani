using UnityEngine;

//カメラのビューポートの値をまとめた構造体
public struct ViewPort
{
    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="x">ビューポート短形：x</param>
    /// <param name="y">ビューポート短形：y</param>
    /// <param name="w">ビューポート短形：w</param>
    /// <param name="h">ビューポート短形：h</param>
    public ViewPort(float x, float y, float w, float h)
    {
        position.x = x;
        position.y = y;
        size.x = w;
        size.y = h;
    }

    public Vector2 position;
    public Vector2 size;
}

//キャラクターベースを継承したクラスの種類
enum CharacterType
{
    CAT_FIRST = 0,
    CAT_SECOND,
    CAT_THIRD,
    BALL_OF_WOOL,
    MAX
}


//キャラクターコントローラー
[RequireComponent(typeof(CharacterController))]

//コントローラーで操作するキャラクターのベースクラス
//継承先の 「Start」 「Update」関数の中で「base.Start()」で呼ぶ
//生成時に controllerNum を設定する
public class CharacterBase : MonoBehaviour
{
    [SerializeField] private float walkSpeed;
    [SerializeField] private AudioClip[] footstepSounds;
    [SerializeField] private string controllerNum;

    private Vector2 inputVal;
    private Vector3 moveDir;
    private CharacterController characterController;
    private AudioSource audioSource;

    private ViewPort[] viewPorts = new ViewPort[(int)CharacterType.MAX]
    {
        new ViewPort(0.0f,0.5f,0.5f,0.5f),
        new ViewPort(0.5f,0.5f,0.5f,0.5f),
        new ViewPort(0.0f,0.0f,0.5f,0.5f),
        new ViewPort(0.5f,0.0f,0.5f,0.5f)
    };


    protected void Start()
    {
        //初期化
        inputVal = Vector2.zero;
        moveDir = Vector3.zero;

        //コンポーネントからキャラクターコントローラー取得
        characterController = GetComponent<CharacterController>();
        //audioSource = GetComponent<AudioSource>();


        //子オブジェクトの順番で取得。最初が0で二番目が1となる。つまり↓は最初の子オブジェクト
        GameObject child = transform.GetChild(0).gameObject;
        Camera camera = child.GetComponent<Camera>();

        //コントローラーの識別は 1 から始まっているので、配列の添え字にする場合は -1 をする
        int num = int.Parse(controllerNum) - 1;
        camera.rect = new Rect(viewPorts[num].position.x, viewPorts[num].position.y, viewPorts[num].size.x, viewPorts[num].size.y);
    }

    protected void Update()
    {
        GetInput();

        moveDir.x = inputVal.x * walkSpeed;
        moveDir.z = inputVal.y * walkSpeed;

        characterController.Move(moveDir * Time.fixedDeltaTime);
    }

    private void PlayFootStepAudio()
    {
        //if (!m_CharacterController.isGrounded)
        //{
        //    return;
        //}
        //// pick & play a random footstep sound from the array,
        //// excluding sound at index 0
        //int n = Random.Range(1, m_FootstepSounds.Length);
        //m_AudioSource.clip = m_FootstepSounds[n];
        //m_AudioSource.PlayOneShot(m_AudioSource.clip);
        //// move picked sound to index 0 so it's not picked next time
        //m_FootstepSounds[n] = m_FootstepSounds[0];
        //m_FootstepSounds[0] = m_AudioSource.clip;
    }

    /// <summary>
    ///  入力取得
    /// </summary>
    private void GetInput()
    {
        //コントローラーの識別
        float horizontal = Input.GetAxis("GamePadX_" + controllerNum);
        float vertical = Input.GetAxis("GamePadY_" + controllerNum);

        //デバッグ用に1Pのみキーボードで移動
        //horizontal = Input.GetAxis("Horizontal") * (controllerNum == "1" ? 1 : 0);
        //vertical = Input.GetAxis("Vertical") * (controllerNum == "1" ? 1 : 0);

        inputVal = new Vector2(horizontal, vertical);

        //アクションボタン
        if (Input.GetButtonDown("GamePadB_" + controllerNum))
        {
            //アクション実行
            //ネコ：4足歩行
            //毛玉：飼い主呼び出し
            Action();
        }
    }

    /// <summary>
    /// 各キャラクター共通で行いたいアクションの実行
    /// </summary>
    protected virtual void Action()
    {
        //実行例...
        //サウンドを鳴らす
        //クールタイムの計算

        Debug.Log(controllerNum + "_押された");
    }

    /// <summary>
    /// 当たり判定
    /// </summary>
    /// <param name="hit"></param>
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

    }
}
