using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// すでに4人のプレイヤーがいる場合、CAT_FIRSTのタイプが選択されていないことに
/// なっていしまい、よろしくないので要修正
/// </summary>

public class AISwitch : MonoBehaviour
{
    private List<short> virtualPadNums = new List<short>() { 1, 2, 3, 4 };

    /// <summary>
    /// 初期化
    /// </summary>
    private void Start()
    {
        List<CharacterType> notSelectedCharacterTypes = new List<CharacterType>();
        StoreCharacterTypesNotSelected(notSelectedCharacterTypes);
        StoreNumbersAsAvailableVirtualPadNumber(virtualPadNums, notSelectedCharacterTypes);
        foreach (CharacterType type in notSelectedCharacterTypes)
        {
            Debug.Log("not selected : " + type.ToString());
        }
        SwitchCharacterControlComponent(notSelectedCharacterTypes);
    }

    /// <summary>
    /// プレイヤーから選択されていないキャラクターのタイプをリストに格納
    /// </summary>
    /// <param name="notSelectedCharacterTypes">格納先リスト</param>
    private void StoreCharacterTypesNotSelected(in List<CharacterType> notSelectedCharacterTypes)
    {
        CharacterCollector collector = CharacterCollector.Instance;
        int controllerNum = collector.GetCharacterNum();

        CharacterType[] selectCharacterTypes = new CharacterType[controllerNum];

        //全キャラクタータイプをリストに追加
        for (int index = 0; index < (int)CharacterType.MAX; ++index)
        {
            CharacterHelper helper = CharacterHelper.Instance;
            notSelectedCharacterTypes.Add(helper.GetType(index));
        }

        for (int index = 0; index < controllerNum; ++index)
        {
            selectCharacterTypes[index] = collector.GetCharacter(index);
        }

        foreach (CharacterType selectCharacterType in selectCharacterTypes)
        {
            notSelectedCharacterTypes.RemoveAll(num => num == selectCharacterType);
        }
    }

    /// <summary>
    /// キャラクターを操作するコンポーネントの有効・無効を切り替える
    /// </summary>
    /// <param name="notSelectedCharacterTypes">選択されていないキャラクタータイプが格納されたリスト</param>
    private void SwitchCharacterControlComponent(in List<CharacterType> notSelectedCharacterTypes)
    {
        Dictionary<CharacterType, string> tags = new Dictionary<CharacterType, string>();
        tags.Add(CharacterType.CAT_FIRST, "FirstCat");
        tags.Add(CharacterType.CAT_SECOND, "SecondCat");
        tags.Add(CharacterType.CAT_THIRD, "ThirdCat");
        tags.Add(CharacterType.BALL_OF_WOOL, "BallOfWool");

        foreach (CharacterType type in notSelectedCharacterTypes)
        {
            //引数で受け取った配列に余計な要素が入っている場合があるため、一応確認
            if (tags.ContainsKey(type) == false)
            {
                return;
            }
            GameObject obj = GameObject.FindWithTag(tags[type]);
            //キャラコンを廃止したのでそれに合わせて↓の行も変更しておく
            obj.GetComponent<CharacterController>().enabled = false;
            //if (type == CharacterType.CAT_FIRST || type == CharacterType.CAT_SECOND || type == CharacterType.CAT_THIRD)
            //{
            //    obj.GetComponent<Cat>().enabled = false;
            //    obj.GetComponent<AICat>().enabled = true;
            //    obj.GetComponent<AICat>().GamePadNum = virtualPadNums[0];
            //}
            //else if (type == CharacterType.BALL_OF_WOOL)
            //{
            //    obj.GetComponent<BallOfWool>().enabled = false;
            //    obj.GetComponent<AIBallOfWool>().enabled = true;
            //    obj.GetComponent<AIBallOfWool>().GamePadNum = virtualPadNums[0];
            //}
            if (type == CharacterType.CAT_FIRST || type == CharacterType.CAT_SECOND || type == CharacterType.CAT_THIRD)
            {
                obj.GetComponent<AICat>().enabled = false;
            }
            else if (type == CharacterType.BALL_OF_WOOL)
            {
                obj.GetComponent<AIBallOfWool>().enabled = false;
            }
            virtualPadNums.RemoveAt(0);
        }
    }

    //それぞれのゲームパッド番号を調べる
    //1-4までの配列から既存の番号を削除
    //残った番号を各キャラに割り当て
    /// <summary>
    /// 他の処理でゲームパッド番号を使用するものがいくつかあるため、
    /// AIで操作するキャラにも仮想のゲームパッド番号を付加するために、
    /// 使える番号がないか探して格納する
    /// </summary>
    void StoreNumbersAsAvailableVirtualPadNumber(in List<short> virtualPadNums, in List<CharacterType> notSelectedCharacterTypes)
    {
        const int MAX_CHARACTER_COUNT = 4;
        int aiCharacterCount = notSelectedCharacterTypes.Count;
        int currentPlayerCount = MAX_CHARACTER_COUNT - aiCharacterCount;
        for (short i = 0; i < currentPlayerCount; ++i)
        {
            if (notSelectedCharacterTypes.Count > 0)
            {
                virtualPadNums.RemoveAt(0);                
            }
        }
    }
}