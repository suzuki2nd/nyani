using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

/// <summary>
/// 毛玉を操作するAIを管理するクラス
/// </summary>
public class AIBallOfWool : AIBase
{
    [SerializeField] private float DitectionRange = 10.0f;
    [SerializeField] private float WanderSpeed = 3.0f;
    [SerializeField] private float EscapeSpeed = 3.5f;
    //40でステージの端までサンプリングできる
    [SerializeField] private float MaxSamplingDistance = 30.0f;
    [SerializeField] private float OwnerCallDistance = 15.0f;
    [SerializeField] private float MaxOwnerCallIntervalTime = 5.0f;
    private CatType[] CatsType = new CatType[(int)CatType.MAX - 1] 
    {
        CatType.FIRST, CatType.SECOND, CatType.THIRD
    };
    //firstCatから順番に格納
    private GameObject[] Cats = new GameObject[(int)CatType.MAX - 1];
    private Vector3[] VectorsEachCatToBallOfWool = new Vector3[(short)CatType.MAX - 1];
    private List<GameObject> CatsInDitectionRange;
    private float ElapsedTimeAfterOwnerCall;
    private bool IsCalledOwner;
    private bool IsSurviveOwner;

    private enum State
    {
        WANDER,
        ESCAPE
    }
    private State CurrentState = State.WANDER;
    private State PrevState = State.WANDER;

    /// <summary>
    /// 初期化
    /// </summary>
    private new void Start()
    {
        base.Start();
        CharacterHelper helper = CharacterHelper.Instance;
        for (int index = 0; index < (int)CatType.MAX - 1; ++index)
        {
            Cats[index] = GameObject.FindWithTag(helper.GetName(index));
        }
        CatsInDitectionRange = new List<GameObject>();
        ElapsedTimeAfterOwnerCall = 0.0f;
        IsCalledOwner = false;
        IsSurviveOwner = false;
    }

    /// <summary>
    /// 更新
    /// </summary>
    private new void Update()
    {
        PrevState = CurrentState;
        IsSurviveOwner = GameObject.FindWithTag("Owner") ? true : false;
        if(IsSurviveOwner)
        {
            Debug.Log("found owner");
            return; //飼い主出現中は移動処理をしない
        }
        else
        {
            Debug.Log("not found owner");
        }
        base.Update();
        SetVectorsEachCatToBallOfWool();
        DitectionCat();
        ChangeState();
        switch (CurrentState)
        {
            case State.WANDER:
                Wander();
                break;
            case State.ESCAPE:
                Escape();
                break;
            default:
                Wander();
                break;
        }

        foreach (GameObject cat in Cats)
        {
            float distanceToCat = (cat.transform.position - transform.position).sqrMagnitude;
            if (distanceToCat > Mathf.Pow(OwnerCallDistance, 2))
            {
                continue;
            }
            ElapsedTimeAfterOwnerCall = 0.0f;
            //GameObject.FindGameObjectWithTag("OwnerEventController")?.
            // GetComponent<OwnerEventController>()?.EventExec();
            IsCalledOwner = true;
        }

        if (IsCalledOwner)
        {
            ElapsedTimeAfterOwnerCall += Time.deltaTime;
        }
        float ownerCallIntervalTime = Random.Range(2, MaxOwnerCallIntervalTime);
        if(ElapsedTimeAfterOwnerCall >= ownerCallIntervalTime)
        {
            OwnerCall();
            ElapsedTimeAfterOwnerCall = 0.0f;
            IsCalledOwner = false;
        }
    }

    /// <summary>
    /// 徘徊する
    /// </summary>
    protected override void Wander()
    {
        if (NavMeshAgent.hasPath || NavMeshAgent.velocity.sqrMagnitude != 0.0f)
        {
            return;
        }
        NavMeshAgent.speed = WanderSpeed;
        base.SetWanderDestination();
    }

    /// <summary>
    /// 逃げる
    /// </summary>
    private void Escape()
    {
        if (PrevState == State.WANDER && NavMeshAgent.hasPath)
        {
            NavMeshAgent.ResetPath();
        }
        if(NavMeshAgent.hasPath)
        {
            return;
        }
        NavMeshAgent.speed = EscapeSpeed;
        Vector3 samplePos = GetSamplePosition(MaxSamplingDistance);
        NavMeshAgent.SetDestination(samplePos);
        Debug.DrawLine(transform.position, samplePos);
    }

    /// <summary>
    /// 猫から逃げるために到達可能な目標地点を取得
    /// </summary>
    /// <param name="maxSamplingDistance">サンプリングする最大距離</param>
    /// <returns></returns>
    Vector3 GetSamplePosition(float maxSamplingDistance)
    {
        if(maxSamplingDistance <= 0)
        {
            //0などを返すと移動が停止したり、ぎこちなくなってしまうため、とりあえず前進させる
            return transform.forward;
        }
        Vector3 clearingDirection = GetEscapeDirectionFromSomeCats().normalized * maxSamplingDistance;
        if (NavMesh.SamplePosition(clearingDirection, out NavMeshHit navMeshHit, MaxDistance, AreaMask))
        {
            return navMeshHit.position;
        }
        else
        {
            return GetSamplePosition(maxSamplingDistance - 1.0f);
        }
    }

    /// <summary>
    /// 現在の状態を変更
    /// </summary>
    private void ChangeState()
    {
        if (CatsInDitectionRange.Count == 0)
        {
            CurrentState = State.WANDER;
        }
        else
        {
            CurrentState = State.ESCAPE;
        }
    }

    /// <summary>
    /// 毛玉からネコまでのベクトルを計算し配列に格納
    /// </summary>
    private void SetVectorsEachCatToBallOfWool()
    {
        for (short i = 0; i < (short)CatType.MAX - 1; ++i)
        {
            VectorsEachCatToBallOfWool[i] = transform.position - Cats[i].transform.position;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void DitectionCat()
    {
        for (int i = 0; i < CatsType.Length; ++i)
        {
            if (IsExistCatInDitectionRange(CatsType[i]) == false)
            {
                CatsInDitectionRange.Remove(Cats[i]);
                continue;
            }
            if (CatsInDitectionRange.Contains(Cats[i]))
            {
                CatsInDitectionRange.Remove(Cats[i]);
                CatsInDitectionRange.Add(Cats[i]);
            }
            else
            {
                CatsInDitectionRange.Add(Cats[i]);
            }
        }
    }

    /// <summary>
    /// 毛玉が行動を起こすための検出範囲内に、引数で指定したネコが存在するか
    /// </summary>
    /// <param name="catType">猫を識別するためのenum</param>
    /// <returns></returns>
    private bool IsExistCatInDitectionRange(CatType catType)
    {
        if(VectorsEachCatToBallOfWool[(int)catType].sqrMagnitude <= Mathf.Pow(DitectionRange, 2))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// 複数の猫から逃げるための移動方向ベクトルを取得
    /// </summary>
    /// <returns></returns>
    private Vector3 GetEscapeDirectionFromSomeCats()
    {
        const short FOUND_A_CAT = 1;
        const short FOUND_TWO_CATS = 2;
        const short FOUND_THREE_CATS = 3;

        switch(CatsInDitectionRange.Count)
        {
            case FOUND_A_CAT:
                return transform.position - CatsInDitectionRange[0].transform.position;

            case FOUND_TWO_CATS:
                return (transform.position - CatsInDitectionRange[0].transform.position +
                        transform.position - CatsInDitectionRange[1].transform.position);

            case FOUND_THREE_CATS:
                Dictionary<Vector3, float> dots = new Dictionary<Vector3, float>();
                Vector3 escapePoint = Vector3.zero;
                for (short i = 0; i < CatsInDitectionRange.Count - 1; ++i)
                {
                    escapePoint = (-VectorsEachCatToBallOfWool[i]) + (-VectorsEachCatToBallOfWool[i + 1]) * 0.5f;

                    dots.Add(escapePoint, Vector3.Dot(-VectorsEachCatToBallOfWool[i], -VectorsEachCatToBallOfWool[i + 1]));
                }
                int max = CatsInDitectionRange.Count;
                escapePoint = (-VectorsEachCatToBallOfWool[max - 1]) + (-VectorsEachCatToBallOfWool[0]) * 0.5f;
                dots.Add(escapePoint, Vector3.Dot(-VectorsEachCatToBallOfWool[max - 1], (-VectorsEachCatToBallOfWool[0])));
                //包囲されていない
                if (dots.All(i => i.Value > 0))
                {
                    Vector3 direction = Vector3.zero;
                    foreach (GameObject cat in CatsInDitectionRange)
                    {
                        direction += (transform.position - cat.transform.position);
                    }
                    return direction;
                }
                else
                {
                    float minDot = dots.Values.Min();
                    return dots.FirstOrDefault(dot => dot.Value == minDot).Key;
                }

            default:
                //実際はここには入らないはずだが、もし入っても移動は止めないように念のため
                return transform.forward;
        }
    }

    /// <summary>
    /// 飼い主を呼ぶ
    /// </summary>
    private void OwnerCall()
    {
        //foreach (GameObject cat in Cats)
        //{
        //    float distanceToCat = (cat.transform.position - transform.position).sqrMagnitude;
        //    if (distanceToCat > Mathf.Pow(OwnerCallDistance, 2))
        //    {
        //        continue;
        //    }
        //    ElapsedTimeAfterOwnerCall = 0.0f;
        //    IsCalledOwner = true;
        //}
    }
}