using UnityEngine;
using UnityEngine.AI;

//コンポーネントの必須化
[RequireComponent(typeof(NavMeshAgent))]
public abstract class AIBase : MonoBehaviour
{
    [SerializeField] protected float WanderRange = 5.0f;
    protected NavMeshAgent NavMeshAgent;
    protected float MaxDistance;
    protected short AreaMask;
    private Vector3 PrevPos;
    private float StopTime;
    public short GamePadNum;

    /// <summary>
    /// 初期化
    /// </summary>
    protected void Start()
    {
        NavMeshAgent = GetComponent<NavMeshAgent>();
        Vector3 initPos = Vector3.zero;
        const float STAGE_SIZE = 50.0f;
        initPos.x = Random.Range(-STAGE_SIZE, STAGE_SIZE);
        initPos.z = Random.Range(-STAGE_SIZE, STAGE_SIZE);
        NavMeshAgent.SetDestination(initPos);
        NavMeshAgent.avoidancePriority = Random.Range(0, 100);
        NavMeshAgent.stoppingDistance = 1.0f;
        MaxDistance = 20.0f;
        AreaMask = 1;

        //NavmeshAgentとRigidBodyが競合してしまうため、RigidBody側からpositionを変更できないように制限
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
        GetComponent<CharacterController>().enabled = false;
    }

    /// <summary>
    /// 更新
    /// </summary>
    protected void Update()
    {
        Debug.Log("pad num : " + GamePadNum);
        PrevPos = transform.position;
        if (NavMeshAgent.pathPending)
        {
            return;
        }
        if ((NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance) == false)
        {
            return;
        }
        const float RECKON_STOP_RANGE = 3.0f;
        if ((PrevPos - transform.position).sqrMagnitude <= Mathf.Pow(RECKON_STOP_RANGE, 2))
        {
            StopTime += Time.deltaTime;
        }
        const float REPATH_TIME_LIMIT = 2.0f;
        if (StopTime >= REPATH_TIME_LIMIT)
        {
            NavMeshAgent.ResetPath();
            SetWanderDestination();
            StopTime = 0.0f;
        }
    }

    /// <summary>
    /// 徘徊させる
    /// </summary>
    protected abstract void Wander();

    /// <summary>
    /// 目的地を設定
    /// </summary>
    protected void SetWanderDestination()
    {
        Vector3 randomPos = new Vector3(Random.Range(-WanderRange, WanderRange), 0, Random.Range(-WanderRange, WanderRange));
        if (NavMesh.SamplePosition(randomPos, out NavMeshHit navMeshHit, MaxDistance, AreaMask))
        {
            NavMeshAgent.SetDestination(navMeshHit.position);
        }
    }
}