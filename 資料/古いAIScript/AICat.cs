using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

/// <summary>
/// AIによって猫の行動を制御するクラス
/// </summary>
public class AICat : AIBase
{
    private enum State
    {
		WANDER,
		CHASE
	}
	private State CurrentState;
	private CatStateBase CatStateBase;
	private GameObject BallOfWool;
	private Vector3 ToBallOfWool;
	private CharacterController CharacterController;

	private bool IsMove;
	private bool IsDive;

	private bool IsCountElapsedTimeAfterDive;
	private float ElapsedTimeAfterDive;
	private float DispersionTime; //各猫で、動作を実行するタイミングにばらつきを持たせるための秒数
	private float QuadrupedalTime;
	[SerializeField] private float StateChangeDistance = 15.0f;
	[SerializeField] private float ChaseSpeed = 3.0f;
	[SerializeField] private float WanderSpeed = 2.5f;
	[SerializeField] private float FieldOfViewRange = 80.0f;
	[SerializeField] private float MinDiveIntervalTime = 1.0f;
	[SerializeField] private float MaxDiveIntervalTime = 3.0f;

	/// <summary>
	/// 初期化
	/// </summary>
	private new void Start()
	{
        base.Start();
        BallOfWool = GameObject.FindWithTag("BallOfWool");
		CatStateBase = new CatStateBipedal(this.gameObject);
		CharacterController = GetComponent<CharacterController>();
		IsDive = false;
		IsMove = true;
		DispersionTime = 0.0f;
		IsCountElapsedTimeAfterDive = true;
    }

	/// <summary>
	/// 更新
	/// </summary>
	private new void Update()
	{
		GetComponent<Animator>().SetBool("dive", IsDive);
		if (IsMove == false || GetStateName().Equals("CatStateFound"))
		{
			return;
		}
		base.Update();

		ToBallOfWool = BallOfWool.transform.position - this.transform.position;
		Bipedal(); //基本は二足歩行
        if (GameObject.FindWithTag("WarningTimer") || GameObject.FindWithTag("Owner"))
        {
            Quadrupedal();
        }
        else
        {
			Bipedal();
        }
        if (CatStateBase.StateName.Equals("CaStateQuadrupedal"))
		{
			QuadrupedalTime += Time.deltaTime;
		}
		else
		{
			QuadrupedalTime = 0.0f;
		}

		const float DIVE_DISTANCE = 5.0f;
		if (IsCountElapsedTimeAfterDive)
		{
			ElapsedTimeAfterDive += Time.deltaTime;
		}

		float diveIntervalTime = Random.Range(MinDiveIntervalTime, MaxDiveIntervalTime);
		if(ToBallOfWool.sqrMagnitude <= 1.0f ||
			ElapsedTimeAfterDive >= diveIntervalTime && ToBallOfWool.sqrMagnitude <= Mathf.Pow(DIVE_DISTANCE, 2))
        {
			Dive();
		}
		ChangeState();
	}

	/// <summary>
	/// 猫の状態を変化させる
	/// </summary>
	void ChangeState()
    {
		if (ToBallOfWool.sqrMagnitude <= Mathf.Pow(StateChangeDistance, 2))
		{
			CurrentState = State.CHASE;
			Chase();
		}
		else
		{
			CurrentState = State.WANDER;
			Wander();
		}
	}

	/// <summary>
	/// 猫を徘徊する
	/// </summary>
	protected override void Wander()
    {
		if (NavMeshAgent.hasPath || NavMeshAgent.velocity.sqrMagnitude != 0f)
		{
			return;
		}
		NavMeshAgent.speed = WanderSpeed;
		base.SetWanderDestination();
	}

	/// <summary>
	/// 追いかける
	/// </summary>
	private void Chase()
    {
		if (Vector3.Angle(ToBallOfWool.normalized, transform.forward) >= FieldOfViewRange)
		{
			return;
		}

		//if (NavMeshAgent.velocity.sqrMagnitude != 0f)
		//{
		//    return;
		//}
		NavMeshAgent.speed = ChaseSpeed;
		NavMeshAgent.stoppingDistance = 0.0f;
		NavMeshAgent.destination = BallOfWool.transform.position;
	}

	/// <summary>
	/// 4足歩行状態にする
	/// </summary>
	private void Quadrupedal()
    {
		//ここの条件おかしくね？
		if (CatStateBase.StateName.Equals("CaStateQuadrupedal") ||
			CatStateBase.StateName.Equals("CatStateFound") ||
			CatStateBase.StateName.Equals("CatStateBipedal") == false)
        {
			Debug.Log("not Quadrupedal");
			return;
        }
		Debug.Log("Quadrupedal");
		CatStateBase = new CaStateQuadrupedal(this.gameObject);
		const float QUADRUPEDAL_RADIUS = 0.75f;
		NavMeshAgent.radius = QUADRUPEDAL_RADIUS;
		NavMeshAgent.speed = 1.5f;
    }

	/// <summary>
	/// 2足歩行状態にする
	/// </summary>
	private void Bipedal()
    {
		//ここの条件おかしくね？
		if(CatStateBase.StateName.Equals("CatStateBipedal") &&
			CatStateBase.StateName.Equals("CatStateFound") &&
			CatStateBase.StateName.Equals("CaStateQuadrupedal") == false)
        {
			Debug.Log("not Bipedal");
			return;
        }
		Debug.Log("Bipedal");
		const float WARNING_TIMER_COUNT = 3.2f;
		DispersionTime = Random.Range(2.0f, WARNING_TIMER_COUNT);
		if(GameObject.FindWithTag("Owner") is null == false)
        {
			CatStateBase = new CatStateBipedal(this.gameObject);
			const float QUADRUPEDAL_RADIUS = 0.75f;
			CharacterController.radius = QUADRUPEDAL_RADIUS;
		}
		else if (QuadrupedalTime  >= DispersionTime +
			GameObject.FindWithTag("Owner")?.GetComponent<OwnerController>()?.SurvivableTime)
        {
			CatStateBase = new CatStateBipedal(this.gameObject);
			const float QUADRUPEDAL_RADIUS = 0.75f;
			NavMeshAgent.radius = QUADRUPEDAL_RADIUS;
			NavMeshAgent.speed = WanderSpeed;
		}
	}

	/// <summary>
	/// ダイブさせる
	/// </summary>
	private void Dive()
	{
		if (CurrentState != State.CHASE ||
			CatStateBase.StateName.Equals("CaStateQuadrupedal") ||
			CatStateBase.StateName.Equals("CatStateFound") ||
			IsDive)
		{
			return;
		}
		ElapsedTimeAfterDive = 0.0f;
		IsCountElapsedTimeAfterDive = false;
		IsDive = true;
		IsMove = false;
	}

	/// <summary>
	/// ダイブの終了時にAnimatorから実行される
	/// </summary>
	private void DiveEnd()
    {
		IsDive = false;
		IsMove = true;
		IsCountElapsedTimeAfterDive = true;
	}

	/// <summary>
	/// 飼い主の方向へ引き寄せられる力を加える
	/// </summary>
	/// <param name="ownerPos">飼い主の座標</param>
	/// <param name="lerpValue">補間係数</param>
	/// <param name="acceptedInputRange">入力を受け付ける範囲</param>
	public void AddForceToOwner(in Vector3 ownerPos, in float lerpValue, in float acceptedInputRange)
	{
		if(CatStateBase.StateName.Equals("CaStateQuadrupedal"))
        {
			return;
        }
		transform.position = Vector3.Lerp(transform.position, ownerPos, lerpValue);
	}

	/// <summary>
	/// 衝突した瞬間
	/// </summary>
	/// <param name="hit">衝突した相手の情報</param>
	private void OnCollisionEnter(Collision collision)
	{
		if (IsDive && collision.gameObject.tag == "BallOfWool")
		{
			Referee.isCaughtCat = true;
			SceneManager.LoadScene("ResultScene");
		}
	}

	/// <summary>
	/// 現在の状態名を取得
	/// </summary>
	/// <returns>状態を表す文字列</returns>
	public string GetStateName()
    {
		return CatStateBase.StateName;
    }

	/// <summary>
	/// 飼い主から見つかった時に実行する処理
	/// </summary>
	public void FoundSelf()
	{
		IsDive = false;
		//CatStateBase = new CatStateFound(this.gameObject);
	}
}